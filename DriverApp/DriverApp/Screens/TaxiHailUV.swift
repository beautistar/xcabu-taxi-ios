//
//  TaxiHailUV.swift
//  DriverApp
//
//  Created by NEW MAC on 25/07/17.
//  Copyright © 2017 V3Cube. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation

class TaxiHailUV: UIViewController, OnLocationUpdateDelegate, AddressFoundDelegate, GMSMapViewDelegate, MyBtnClickDelegate, UICollectionViewDelegate, UICollectionViewDataSource {

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var gMapContainerView: UIView!
    @IBOutlet weak var destView: UIView!
    @IBOutlet weak var destLbl: MyLabel!
    @IBOutlet weak var destHLbl: MyLabel!
    @IBOutlet weak var destPointView: UIView!
    @IBOutlet weak var progressBarContainerView: UIView!
    @IBOutlet weak var destPinImgview: UIImageView!
    
     //Request PickUp BottomView OutLets
    @IBOutlet weak var cabTypeCollectionView: UICollectionView!
    @IBOutlet weak var noCabTypeLbl: MyLabel!
    @IBOutlet weak var bookNowBtn: MyButton!
    @IBOutlet weak var payImgView: UIImageView!
    @IBOutlet weak var payLbl: MyLabel!
    
    var vVehicleImgPath = CommonUtils.webServer + "webimages/icons/VehicleType/"
    
    var getAddressFrmLocation:GetAddressFromLocation!
    
    var isDataSet = false
    
    var getLocation:GetLocation!
    var gMapView:GMSMapView!
    
    var destLocation:CLLocation!
    var pickUpLocation:CLLocation!
    
    var pickUpAddress = ""
    
    var isFirstLocationUpdate = true
    
    var generalFunc = GeneralFunctions()
    
    var requestPickUpView:UIView!
    
    var isCashPayment = true
    
    var cabTypesArr = [NSDictionary]()
    
    var selectedCabTypeId = ""
    
    var carTypeResponse:NSDictionary!
    
    let linearProgressBar = LinearProgressBarView()
    override func viewWillAppear(_ animated: Bool) {
        
        self.configureRTLView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.contentView.addSubview(self.generalFunc.loadView(nibName: "TaxiHailScreenDesign", uv: self, contentView: contentView))
        
        
        getAddressFrmLocation = GetAddressFromLocation(uv: self, addressFoundDelegate: self)
        
        
        self.addBackBarBtn()
        
        setData()
        NotificationCenter.default.addObserver(self, selector: #selector(self.releaseAllTask), name: NSNotification.Name(rawValue: Utils.releaseAllTaskObserverKey), object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if(isDataSet == false){
            let camera = GMSCameraPosition.camera(withLatitude: -180, longitude: -180, zoom: 15.0)
            gMapView = GMSMapView.map(withFrame: self.gMapContainerView.frame, camera: camera)
            
            gMapView.isMyLocationEnabled = true
            gMapView.delegate = self
            self.gMapContainerView.addSubview(gMapView)
            
            
            linearProgressBar.heightForLinearBar = 5
            linearProgressBar.backgroundColor = UIColor.clear
            linearProgressBar.backgroundProgressBarColor = self.progressBarContainerView.backgroundColor != nil ? self.progressBarContainerView.backgroundColor! : UIColor.clear
            linearProgressBar.progressBarColor = UIColor.UCAColor.AppThemeColor
            
            self.progressBarContainerView.addSubview(linearProgressBar)
            
            linearProgressBar.startAnimation()
            
            getLocation = GetLocation(uv: self, isContinuous: false)
            getLocation.buildLocManager(locationUpdateDelegate: self)
            
            isDataSet = true
            
            
        }
    }
    override func closeCurrentScreen() {
        if(self.requestPickUpView != nil){
            self.requestPickUpView.removeFromSuperview()
            self.destPinImgview.isHidden = true
            self.destLocation = nil
            self.destLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_ADD_DESTINATION_BTN_TXT")
            self.requestPickUpView = nil
            return
        }
        releaseAllTask()
        super.closeCurrentScreen()
    }

    func setData(){
        self.navigationItem.title = self.generalFunc.getLanguageLabel(origValue: "Taxi Hail", key: "LBL_TAXI_HAIL")
        self.title = self.generalFunc.getLanguageLabel(origValue: "Taxi Hail", key: "LBL_TAXI_HAIL")
        
        destLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_ADD_DESTINATION_BTN_TXT")
        destHLbl.text = self.generalFunc.getLanguageLabel(origValue: "Drop at", key: "LBL_DROP_AT")
    }
    
    deinit {
        releaseAllTask()
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        mapView.selectedMarker = nil
        return true
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        
        if(destPinImgview.isHidden == true){
            return
        }
        self.destLocation = nil
        self.destLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_SELECTING_LOCATION_TXT")
        getAddressFrmLocation.setLocation(latitude: getCenterLocation().coordinate.latitude, longitude: getCenterLocation().coordinate.longitude)
        getAddressFrmLocation.setPickUpMode(isPickUpMode: false)
        getAddressFrmLocation.executeProcess(isOpenLoader: false, isAlertShow:false)
    }
    
    
    func getCenterLocation() -> CLLocation{
        return CLLocation(latitude: self.gMapView.camera.target.latitude, longitude: self.gMapView.camera.target.longitude)
    }
    
    
    func releaseAllTask(){
        if(self.linearProgressBar != nil){
            self.linearProgressBar.stopAnimation()
        }
        if(gMapView != nil){
            gMapView!.stopRendering()
            gMapView!.removeFromSuperview()
            gMapView!.clear()
            gMapView!.delegate = nil
            gMapView = nil
        }
        
        
        if(self.getLocation != nil){
            self.getLocation!.locationUpdateDelegate = nil
            self.getLocation!.releaseLocationTask()
            self.getLocation = nil
        }
        
        GeneralFunctions.removeObserver(obj: self)
        
    }
    
    func onLocationUpdate(location: CLLocation) {
        self.pickUpLocation = location
        
        
        var currentZoomLevel:Float = self.gMapView.camera.zoom
        
        if(currentZoomLevel < 15.0 && isFirstLocationUpdate == true){
            currentZoomLevel = 15.0
        }
        let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude,
                                              longitude: location.coordinate.longitude, zoom: currentZoomLevel)
        
        self.gMapView.animate(to: camera)
        
        isFirstLocationUpdate = false
        
        getAddressFrmLocation.setLocation(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        getAddressFrmLocation.setPickUpMode(isPickUpMode: true)
        getAddressFrmLocation.executeProcess(isOpenLoader: false, isAlertShow:false)
    }
    
    func onAddressFound(address: String, location: CLLocation, isPickUpMode:Bool, dataResult:String) {
        if(isPickUpMode == true){
            self.pickUpAddress = address
            
            if(self.progressBarContainerView.isHidden == false){
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {
                    self.progressBarContainerView.isHidden = true
                })
            }
            
            
            let destTapGue = UITapGestureRecognizer()
            destTapGue.addTarget(self, action: #selector(self.openPlaceFinder))
            destView.isUserInteractionEnabled = true
            
            destView.addGestureRecognizer(destTapGue)
        }else{
            self.destLocation = location
            self.destLbl.text = address
        }
        
    }
    
    func openPlaceFinder(){
        let launchPlaceFinder = LaunchPlaceFinder(viewControllerUV: self)
        launchPlaceFinder.currInst = launchPlaceFinder
        launchPlaceFinder.currentTransition =
            JTMaterialTransition(animatedView: self.destPointView, bgColor: UIColor.UCAColor.AppThemeColor.lighter(by: 35)!)
        
        
        launchPlaceFinder.setBiasLocation(sourceLocationPlaceLatitude: (destLocation == nil ? pickUpLocation : destLocation).coordinate.latitude, sourceLocationPlaceLongitude: (destLocation == nil ? pickUpLocation : destLocation).coordinate.longitude)
        
        
        launchPlaceFinder.initializeFinder { (address, latitude, longitude) in
            
            self.destLocation = CLLocation(latitude: latitude, longitude: longitude)

            
            self.destLbl.text = address
            
            let camera = GMSCameraPosition.camera(withLatitude: self.destLocation.coordinate.latitude,
                                                  longitude: self.destLocation.coordinate.longitude, zoom: self.gMapView.camera.zoom)
            
            self.gMapView.moveCamera(GMSCameraUpdate.setCamera(camera))
            
            if(self.requestPickUpView == nil){
//                self.destPinImgview.isHidden = false
//                self.openRequestPickUpView()
                
                self.getDirectionData()
            }
            
        }
    }
    
    
    func openRequestPickUpView(){
        self.destPinImgview.isHidden = false
        requestPickUpView = self.generalFunc.loadView(nibName: "RequestPickUpBottomView", uv: self, isWithOutSize: true)
        requestPickUpView.frame = CGRect(x: 0, y: self.view.frame.size.height + 255, width: Application.screenSize.width, height: 255)
        
        self.view.addSubview(requestPickUpView)
        
        UIView.animate(withDuration: 0.8,
                       animations: {
                        //                        self.requestPickUpView.center = CGPoint(x: 0, y: 310)
                        self.requestPickUpView.frame.origin.y = self.view.frame.size.height - 255
                        self.view.layoutIfNeeded()
        },  completion: { finished in
            
        })
        
        self.noCabTypeLbl.text = self.generalFunc.getLanguageLabel(origValue: "No aervice available in your selected pickup location.", key: "LBL_NO_SERVICE_AVAILABLE_TXT")
        self.noCabTypeLbl.fitText()
        self.noCabTypeLbl.isHidden = true
        
        if(self.cabTypesArr.count < 1){
            self.noCabTypeLbl.isHidden = false
        }
        
        self.payLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_CASH_TXT")
        self.payImgView.image = UIImage(named: "ic_cash_new")
        
        self.bookNowBtn.setButtonTitle(buttonTitle: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_START_TRIP"))
        
        
        self.cabTypeCollectionView.register(UINib(nibName: "CabTypeCVCell", bundle: nil), forCellWithReuseIdentifier: "CabTypeCVCell")
        self.cabTypeCollectionView.dataSource = self
        self.cabTypeCollectionView.delegate = self
//        self.cabTypeCollectionView.reloadData()
        

        
        self.bookNowBtn.clickDelegate = self
        
//        self.bookNowBtn.setButtonEnabled(isBtnEnabled: false)
//        self.bookNowBtn.setButtonTitleColor(color: UIColor(hex: 0x6b6b6b))
        
    }
    
    func getDirectionData(){
        
        self.progressBarContainerView.isHidden = false
        
         let directionURL = "https://maps.googleapis.com/maps/api/directions/json?origin=\(self.pickUpLocation!.coordinate.latitude),\(self.pickUpLocation!.coordinate.longitude)&destination=\(destLocation!.coordinate.latitude),\(destLocation!.coordinate.longitude)&key=\(Configurations.getInfoPlistValue(key: "GOOGLE_SERVER_KEY"))&language=\(Configurations.getGoogleMapLngCode())&sensor=true"

        
        let exeWebServerUrl = ExeServerUrl(dict_data: [String:String](), currentView: self.view, isOpenLoader: false)
        
        exeWebServerUrl.executeGetProcess(completionHandler: { (response) -> Void in
            
            if(response != ""){
                let dataDict = response.getJsonDataDict()
                
                if(dataDict.get("status").uppercased() != "OK" || dataDict.getArrObj("routes").count == 0){
                    return
                }
                
                
                let routesArr = dataDict.getArrObj("routes")
                let legs_arr = (routesArr.object(at: 0) as! NSDictionary).getArrObj("legs")
                let duration = (legs_arr.object(at: 0) as! NSDictionary).getObj("duration").get("value")
                let distance = (legs_arr.object(at: 0) as! NSDictionary).getObj("distance").get("value")
                
                self.getVehicleTypesData(duration: duration, distance: distance)
            }else{
                //                self.generalFunc.setError(uv: self)
                
                self.generalFunc.setAlertMessage(uv: self, title: "", content: self.generalFunc.getLanguageLabel(origValue: "Please try again.", key: "LBL_TRY_AGAIN_TXT"), positiveBtn: self.generalFunc.getLanguageLabel(origValue: "Retry", key: "LBL_RETRY_TXT"), nagativeBtn: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_CANCEL_TXT"), completionHandler: { (btnClickedIndex) in
                    
                    if(btnClickedIndex == 0){
                        self.getDirectionData()
                    }else{
                        
                        self.destLocation = nil
                        self.destLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_ADD_DESTINATION_BTN_TXT")
                        self.destHLbl.text = self.generalFunc.getLanguageLabel(origValue: "Drop at", key: "LBL_DROP_AT")
                    }
                })
                
            }
        }, url: directionURL)
    }
    
    func getVehicleTypesData(duration:String, distance:String){
        self.progressBarContainerView.isHidden = false
        
        
        let parameters = ["type":"getDriverVehicleDetails","UserType": Utils.appUserType, "iDriverId": GeneralFunctions.getMemberd(), "distance": distance, "time": duration]
        
        let exeWebServerUrl = ExeServerUrl(dict_data: parameters, currentView: self.view, isOpenLoader: false)
        exeWebServerUrl.setDeviceTokenGenerate(isDeviceTokenGenerate: false)
        exeWebServerUrl.currInstance = exeWebServerUrl
        exeWebServerUrl.executePostProcess(completionHandler: { (response) -> Void in
            
            if(response != ""){
                let dataDict = response.getJsonDataDict()
                
                
                if(dataDict.get("Action") == "1"){
                    
                    self.carTypeResponse = dataDict
                    
                    let msgDataArr = dataDict.getArrObj(Utils.message_str)
                    
                    self.cabTypesArr.removeAll()
                    
                    for i in 0..<msgDataArr.count{
                        let item = msgDataArr[i] as! NSDictionary
                        if(i == 0){
                            self.selectedCabTypeId = item.get("iVehicleTypeId")
                        }
                        self.cabTypesArr += [item]
                    }
                    
                    
//                    self.cabTypeCollectionView.reloadData()
                    self.openRequestPickUpView()
                    
                }else{
                    self.generalFunc.setAlertMessage(uv: self, title: "", content: self.generalFunc.getLanguageLabel(origValue: "", key: dataDict.get(Utils.message_str)), positiveBtn: self.generalFunc.getLanguageLabel(origValue: "Retry", key: "LBL_RETRY_TXT"), nagativeBtn: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_CANCEL_TXT"), completionHandler: { (btnClickedIndex) in
                        
                        if(btnClickedIndex == 0){
                            self.getVehicleTypesData(duration: duration, distance: distance)
                        }else{
                            self.destLocation = nil
                            self.destLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_ADD_DESTINATION_BTN_TXT")
                            self.destHLbl.text = self.generalFunc.getLanguageLabel(origValue: "Drop at", key: "LBL_DROP_AT")
                        }
                    })
                    
                }
                
            }else{
                self.generalFunc.setAlertMessage(uv: self, title: "", content: self.generalFunc.getLanguageLabel(origValue: "Please try again.", key: "LBL_TRY_AGAIN_TXT"), positiveBtn: self.generalFunc.getLanguageLabel(origValue: "Retry", key: "LBL_RETRY_TXT"), nagativeBtn: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_CANCEL_TXT"), completionHandler: { (btnClickedIndex) in
                    
                    if(btnClickedIndex == 0){
                        self.getVehicleTypesData(duration: duration, distance: distance)
                    }else{
                        
                        self.destLocation = nil
                        self.destLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_ADD_DESTINATION_BTN_TXT")
                        self.destHLbl.text = self.generalFunc.getLanguageLabel(origValue: "Drop at", key: "LBL_DROP_AT")
                    }
                })
                
            }
            
            self.progressBarContainerView.isHidden = true
        })
    
    }

    func myBtnTapped(sender: MyButton) {
        if(self.bookNowBtn != nil && sender == self.bookNowBtn){
            self.generalFunc.setAlertMessage(uv: self, title: "", content: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_CONFIRM_START_TRIP_TXT"), positiveBtn: self.generalFunc.getLanguageLabel(origValue: "Ok", key: "LBL_BTN_OK_TXT"), nagativeBtn: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_CANCEL_TXT"), completionHandler: { (btnClickedIndex) in
                
                if(btnClickedIndex == 0){
                    self.startTrip()
                }
            })
        }
    }
    
    func startTrip(){
        let parameters = ["type":"StartHailTrip","UserType": Utils.appUserType, "iDriverId": GeneralFunctions.getMemberd(), "SelectedCarTypeID": selectedCabTypeId, "PickUpLatitude": "\(self.pickUpLocation.coordinate.latitude)", "PickUpLongitude": "\(self.pickUpLocation.coordinate.longitude)", "PickUpAddress": self.pickUpAddress, "DestLatitude": "\(self.destLocation.coordinate.latitude)", "DestLongitude": "\(self.destLocation.coordinate.longitude)", "DestAddress": self.destLbl.text!]
        
        let exeWebServerUrl = ExeServerUrl(dict_data: parameters, currentView: self.view, isOpenLoader: false)
        exeWebServerUrl.setDeviceTokenGenerate(isDeviceTokenGenerate: false)
        exeWebServerUrl.currInstance = exeWebServerUrl
        exeWebServerUrl.executePostProcess(completionHandler: { (response) -> Void in
            
            if(response != ""){
                let dataDict = response.getJsonDataDict()
                
                
                if(dataDict.get("Action") == "1"){
                    
                    self.releaseAllTask()
                    let window = Application.window
                    
                    let getUserData = GetUserData(uv: self, window: window!)
                    getUserData.getdata()
                    
                    
                }else{
                    self.generalFunc.setAlertMessage(uv: self, title: "", content: self.generalFunc.getLanguageLabel(origValue: "", key: dataDict.get(Utils.message_str)), positiveBtn: self.generalFunc.getLanguageLabel(origValue: "Retry", key: "LBL_RETRY_TXT"), nagativeBtn: "", completionHandler: { (btnClickedIndex) in
                        
                        self.startTrip()
                    })
                    
                }
                
            }else{
                self.generalFunc.setAlertMessage(uv: self, title: "", content: self.generalFunc.getLanguageLabel(origValue: "Please try again.", key: "LBL_TRY_AGAIN_TXT"), positiveBtn: self.generalFunc.getLanguageLabel(origValue: "Retry", key: "LBL_RETRY_TXT"), nagativeBtn: "", completionHandler: { (btnClickedIndex) in
                    
                    self.startTrip()
                })
            }
            
        })
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let iVehicleTypeId = self.cabTypesArr[indexPath.item].get("iVehicleTypeId")
        
        if(self.selectedCabTypeId == iVehicleTypeId){
            openFareInfoView(cabTypeItem: self.cabTypesArr[indexPath.item])
        }else{
            self.selectedCabTypeId = iVehicleTypeId
            collectionView.reloadData()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        
        let screenWidth = Application.screenSize.width
        let totalCellWidth = (120 * cabTypesArr.count)
        
        
        let leftInset = (collectionView.frame.width - CGFloat(totalCellWidth + 0)) / 2;
        let rightInset = leftInset
        
        if(screenWidth < CGFloat(totalCellWidth)){
            return UIEdgeInsetsMake(0, 0, 0, 0)
        }else{
            return UIEdgeInsetsMake(0, leftInset, 0, rightInset)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cabTypesArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CabTypeCVCell", for: indexPath) as! CabTypeCVCell
        
        let tempDict = cabTypesArr[indexPath.item]
        let iVehicleTypeId = tempDict.get("iVehicleTypeId")
        
        //        print("iVehicleTypeId:\(iVehicleTypeId)")
        //        print("selectedCabTypeId:\(selectedCabTypeId)")
        
        if(self.selectedCabTypeId == iVehicleTypeId){
            cell.cabTypeHoverImgView.isHidden = false
            cell.cabTypeImgView.isHidden = true
            cell.cabTypeNameLbl.textColor = UIColor.UCAColor.AppThemeColor
        }else{
            cell.cabTypeHoverImgView.isHidden = true
            cell.cabTypeImgView.isHidden = false
            cell.cabTypeNameLbl.textColor = UIColor(hex: 0x161718)
        }
        
        //        if(Configurations.isRTLMode()){
        //            var scalingTransform : CGAffineTransform!
        //            scalingTransform = CGAffineTransform(scaleX: -1, y: 1);
        //            cell.transform = scalingTransform
        //         }
        
        cell.fareEstLbl.text = Configurations.convertNumToAppLocal(numStr: tempDict.get("SubTotal"))
        
        cell.cabTypeNameLbl.text = tempDict.get("vVehicleTypeName")
        
        Utils.createRoundedView(view: cell.cabTypeImgView, borderColor: UIColor(hex: 0xcbcbcb), borderWidth: 1)
        Utils.createRoundedView(view: cell.cabTypeHoverImgView, borderColor: UIColor.UCAColor.AppThemeColor, borderWidth: 1)
        
        var vCarLogoImg = ""
        var vCarLogoHoverImg = ""
        if(UIScreen.main.scale < 2){
            vCarLogoImg = "1x_\(tempDict.get("vLogo"))"
            vCarLogoHoverImg = "1x_hover_\(tempDict.get("vLogo"))"
        }else if(UIScreen.main.scale < 3){
            vCarLogoImg = "2x_\(tempDict.get("vLogo"))"
            vCarLogoHoverImg = "2x_hover_\(tempDict.get("vLogo"))"
        }else{
            vCarLogoImg = "3x_\(tempDict.get("vLogo"))"
            vCarLogoHoverImg = "3x_hover_\(tempDict.get("vLogo"))"
        }
        let imgUrl = vVehicleImgPath + "\(iVehicleTypeId)/ios/\(vCarLogoImg)"
        
        let hoverImgUrl = vVehicleImgPath + "\(iVehicleTypeId)/ios/\(vCarLogoHoverImg)"
        
        //        print("imgUrl:\(imgUrl)")
        //        print("hoverImgUrl:\(hoverImgUrl)")
        
        cell.cabTypeHoverImgView.sd_setImage(with: URL(string: hoverImgUrl), placeholderImage: UIImage(named: "placeHolder.png"),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
            
            GeneralFunctions.setImgTintColor(imgView: cell.cabTypeHoverImgView, color: UIColor.UCAColor.AppThemeTxtColor)
        })
        cell.cabTypeImgView.sd_setImage(with: URL(string: imgUrl), placeholderImage: UIImage(named: "placeHolder.png"),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
            GeneralFunctions.setImgTintColor(imgView: cell.cabTypeImgView, color: UIColor(hex: 0x999fa2))
        })
        
        cell.cabTypeImgView.backgroundColor = UIColor(hex: 0xebebeb)
        cell.cabTypeHoverImgView.backgroundColor = UIColor.UCAColor.AppThemeColor
        
        GeneralFunctions.setImgTintColor(imgView: cell.cabTypeHoverImgView, color: UIColor.UCAColor.AppThemeTxtColor)
        GeneralFunctions.setImgTintColor(imgView: cell.cabTypeImgView, color: UIColor(hex: 0x999fa2))
        
        if(indexPath.item == 0){
            cell.leftSeperationTopView.isHidden = true
            cell.leftSeperationBottomView.isHidden = true
            
        }else{
            
            cell.leftSeperationTopView.isHidden = false
            cell.leftSeperationBottomView.isHidden = false
        }
        
        if(indexPath.item == (self.cabTypesArr.count - 1)){
            cell.rightSeperationTopView.isHidden = true
            cell.rightSeperationBottomView.isHidden = true
        }else{
            cell.rightSeperationTopView.isHidden = false
            cell.rightSeperationBottomView.isHidden = false
        }
        
        return cell
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func openFareInfoView(cabTypeItem:NSDictionary){
        let height = Application.screenSize.height > 480 ? 450 : (Application.screenSize.height - 60)
        let fareDetailView = FareDetailView(frame: CGRect(x: 0, y: self.view.frame.height - height, width: Application.screenSize.width, height: height))
        
        fareDetailView.setViewHandler { (isViewClose, view, isMoreDetailTapped) in
            fareDetailView.frame.origin.y = Application.screenSize.height + height
            self.view.layoutIfNeeded()
            
            if(isMoreDetailTapped){
                let fareBreakDownUv = GeneralFunctions.instantiateViewController(pageName: "FareBreakDownUV") as! FareBreakDownUV
                fareBreakDownUv.selectedCabTypeId = self.selectedCabTypeId
                fareBreakDownUv.pickUpLocation = self.pickUpLocation
                fareBreakDownUv.destLocation = self.destLocation
                fareBreakDownUv.selectedCabTypeName = cabTypeItem.get("vVehicleTypeName")
                self.pushToNavController(uv: fareBreakDownUv)
            }
        }
        
        
        self.view.addSubview(fareDetailView)
        
        
        fareDetailView.cabTypeNameLbl.text = cabTypeItem.get("vVehicleTypeName")
        
        fareDetailView.doneBtn.setButtonTitle(buttonTitle: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_DONE"))
        
        let vLogo = cabTypeItem.get("vLogo")
        
        var vCarLogoHoverImg = ""
        if(UIScreen.main.scale < 2){
            vCarLogoHoverImg = "1x_hover_\(vLogo)"
        }else if(UIScreen.main.scale < 3){
            vCarLogoHoverImg = "2x_hover_\(vLogo)"
        }else{
            vCarLogoHoverImg = "3x_hover_\(vLogo)"
        }
        
        let hoverImgUrl = vVehicleImgPath + "\(selectedCabTypeId)/ios/\(vCarLogoHoverImg)"
        
        fareDetailView.cabTypeImgView.sd_setImage(with: URL(string: hoverImgUrl), placeholderImage: UIImage(named: "placeHolder.png"),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
            
            GeneralFunctions.setImgTintColor(imgView: fareDetailView.cabTypeImgView, color: UIColor.UCAColor.AppThemeColor)
        })
        
        fareDetailView.capacityHLbl.text = self.generalFunc.getLanguageLabel(origValue: "Capacity", key: "LBL_CAPACITY")
        fareDetailView.capacityVLbl.text = Configurations.convertNumToAppLocal(numStr: cabTypeItem.get("iPersonSize") + " \(self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_PEOPLE_TXT"))")
        
        fareDetailView.fareHLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_FARE_TXT")
        fareDetailView.noteLbl.text = self.generalFunc.getLanguageLabel(origValue: "This fare is based on our estimation. This may vary during trip and final fare.", key: "LBL_GENERAL_NOTE_FARE_EST")
        fareDetailView.noteLbl.fitText()
        
        fareDetailView.moreDetailsLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_MORE_INFO")
        fareDetailView.fareVLbl.text = Configurations.convertNumToAppLocal(numStr: cabTypeItem.get("SubTotal"))
    }


}
