//
//  AddVehiclesUV.swift
//  DriverApp
//
//  Created by NEW MAC on 02/06/17.
//  Copyright © 2017 V3Cube. All rights reserved.
//

import UIKit

class AddVehiclesUV: UIViewController, MyBtnClickDelegate, BEMCheckBoxDelegate, MyTxtFieldClickDelegate {
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var makeTxtField: MyTextField!
    @IBOutlet weak var modelTxtField: MyTextField!
    @IBOutlet weak var yearTxtField: MyTextField!
    @IBOutlet weak var colorTxtField: MyTextField!
    @IBOutlet weak var licPlatTxtField: MyTextField!
    @IBOutlet weak var serviceContainerView: UIStackView!
    @IBOutlet weak var serviceContainerViewHeight: NSLayoutConstraint!
    @IBOutlet weak var submitBtn: MyButton!
    @IBOutlet weak var handiCapView: UIView!
    @IBOutlet weak var handiCapLbl: MyLabel!
    @IBOutlet weak var handiCapChkBox: BEMCheckBox!
    @IBOutlet weak var handiCapViewHeight: NSLayoutConstraint!
    @IBOutlet weak var vehicleInfoAreaHeight: NSLayoutConstraint!
    @IBOutlet weak var vehicleInfoAreaView: UIView!
    
    var userProfileJson:NSDictionary!
    
    let generalFunc = GeneralFunctions()
    
    var iDriverVehicleId = ""
    var currentVehicleData:NSDictionary!
    
    let scrollView = UIScrollView()
    
    var isFromMainPage = false
    var mainScreenUv:MainScreenUV!
    
    var isPageLoaded = false
    
    var cntView:UIView!
    
    var loaderView:UIView!
    
    var window:UIWindow!
    
    var carlistArr = [NSDictionary]()
    var yearListArr = [String]()
    var carTypeArr = [NSDictionary]()
    
    var makeListArr = [String]()
    var modelListArr = [String]()
    var modelsArr = [NSDictionary]()
    
    var makePicker:DownPicker!
    var yearPicker:DownPicker!
    var modelPicker:DownPicker!
    
    var carTypesStatusArr = [Bool]()
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.configureRTLView()
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        window = Application.window!
        
        self.contentView.addSubview(scrollView)
        
        cntView = self.generalFunc.loadView(nibName: "AddVehiclesScreenDesign", uv: self, contentView: contentView)
        
        scrollView.addSubview(cntView)
        
//        self.contentView.addSubview(self.generalFunc.loadView(nibName: "AddVehiclesScreenDesign", uv: self, contentView: contentView))
        
        self.addBackBarBtn()
        
//        setData()
        
        userProfileJson = (GeneralFunctions.getValue(key: Utils.USER_PROFILE_DICT_KEY) as! String).getJsonDataDict().getObj(Utils.message_str)
        
        let HANDICAP_ACCESSIBILITY_OPTION = userProfileJson.get("HANDICAP_ACCESSIBILITY_OPTION")
        
        if(HANDICAP_ACCESSIBILITY_OPTION.uppercased() != "YES"){
            handiCapViewHeight.constant = 0
            handiCapView.isHidden = true
        }else{
            handiCapView.isHidden = false
            self.handiCapChkBox.boxType = .square
            self.handiCapChkBox.offAnimationType = .bounce
            self.handiCapChkBox.onAnimationType = .bounce
            self.handiCapChkBox.onCheckColor = UIColor.UCAColor.AppThemeTxtColor
            self.handiCapChkBox.onFillColor = UIColor.UCAColor.AppThemeColor
            self.handiCapChkBox.onTintColor = UIColor.UCAColor.AppThemeColor
            self.handiCapChkBox.tintColor = UIColor.UCAColor.AppThemeColor_1
            self.handiCapLbl.text = self.generalFunc.getLanguageLabel(origValue: "Handicap accessibility available?", key: "LBL_HANDICAP_QUESTION")
        }
        
        if(userProfileJson.get("APP_TYPE") == Utils.cabGeneralType_UberX){
            self.vehicleInfoAreaHeight.constant = 0
            self.vehicleInfoAreaView.isHidden = true
        }
        
        makeTxtField.disableMenu()
        yearTxtField.disableMenu()
        modelTxtField.disableMenu()
    }

    override func viewDidAppear(_ animated: Bool) {
        if(isPageLoaded == false){
            scrollView.frame = self.contentView.frame
            
            
            let screenHeight = Application.screenSize.height - self.navigationController!.navigationBar.frame.height - UIApplication.shared.statusBarFrame.height
            
            cntView.frame.size = CGSize(width: contentView.frame.width, height: (self.contentView.frame.height > screenHeight ? self.contentView.frame.height : screenHeight))
            
            let yPosition = ((cntView.frame.height / 2) - (self.contentView.frame.height / 2)) >= 0 ? ((cntView.frame.height / 2) - (self.contentView.frame.height / 2)) : contentView.bounds.midY
            
            cntView.center = CGPoint(x: contentView.bounds.midX, y: contentView.bounds.midY + yPosition)
            
            scrollView.setContentViewSize()
            scrollView.bounces = false
            scrollView.backgroundColor = UIColor.clear
            
            setData()
            
            getData()
            
            isPageLoaded = true
        }
    }
    
    func setData(){
        if(userProfileJson.get("APP_TYPE") == Utils.cabGeneralType_UberX){
            self.navigationItem.title = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_MANAGE_VEHICLES")
            self.title = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_MANAGE_VEHICLES")
        }else{
            self.navigationItem.title = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_ADD_VEHICLE")
            self.title = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_ADD_VEHICLE")
        }
        
        self.makeTxtField.setPlaceHolder(placeHolder: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_MAKE"))
        self.modelTxtField.setPlaceHolder(placeHolder: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_MODEL"))
        self.yearTxtField.setPlaceHolder(placeHolder: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_YEAR"))
        self.colorTxtField.setPlaceHolder(placeHolder: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_COLOR_TXT"))
        self.licPlatTxtField.setPlaceHolder(placeHolder: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_LICENCE_PLATE_TXT"))
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {
            self.makeTxtField.addArrowView(color: UIColor(hex: 0xbcbcbc), transform: CGAffineTransform(rotationAngle: 90 * CGFloat(CGFloat.pi/180)))
            self.modelTxtField.addArrowView(color: UIColor(hex: 0xbcbcbc), transform: CGAffineTransform(rotationAngle: 90 * CGFloat(CGFloat.pi/180)))
            self.yearTxtField.addArrowView(color: UIColor(hex: 0xbcbcbc), transform: CGAffineTransform(rotationAngle: 90 * CGFloat(CGFloat.pi/180)))
        })
        self.submitBtn.setButtonTitle(buttonTitle: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_BTN_SUBMIT_TXT"))
        
        self.modelTxtField.setEnable(isEnabled: false)
        self.modelTxtField.myTxtFieldDelegate = self
        
        self.submitBtn.clickDelegate = self
        
        if(self.currentVehicleData != nil){
            self.licPlatTxtField.setText(text: self.currentVehicleData!.get("vLicencePlate"))
            self.colorTxtField.setText(text: self.currentVehicleData!.get("vColour"))
            
            if(self.currentVehicleData!.get("eHandiCapAccessibility") == "Yes"){
                self.handiCapChkBox.on = true
            }
            
            self.navigationItem.title = self.generalFunc.getLanguageLabel(origValue: "Edit Vehicle", key: "LBL_EDIT_VEHICLE")
            self.title = self.generalFunc.getLanguageLabel(origValue: "Edit Vehicle", key: "LBL_EDIT_VEHICLE")
        }
        
        
        self.licPlatTxtField.regexToMatch = "^[a-zA-Z0-9 ]+$"
    }
    
    func myTxtFieldTapped(sender: MyTextField) {
        if(modelTxtField != nil && sender == modelTxtField){
            Utils.showSnakeBar(msg: self.generalFunc.getLanguageLabel(origValue: "Please select make", key: "LBL_SELECT_MAKE_HINT"), uv: self)
        }
    }
    

    func getData(){
        scrollView.isHidden = true
        loaderView =  self.generalFunc.addMDloader(contentView: self.view)
        loaderView.backgroundColor = UIColor.clear
        
        let parameters = ["type":"getUserVehicleDetails","iMemberId": GeneralFunctions.getMemberd(), "UserType": Utils.appUserType]
        
        let exeWebServerUrl = ExeServerUrl(dict_data: parameters, currentView: self.view, isOpenLoader: false)
        exeWebServerUrl.setDeviceTokenGenerate(isDeviceTokenGenerate: false)
        exeWebServerUrl.currInstance = exeWebServerUrl
        exeWebServerUrl.executePostProcess(completionHandler: { (response) -> Void in
            
            if(response != ""){
                let dataDict = response.getJsonDataDict()
                
                if(dataDict.get("Action") == "1"){
                    
                    self.loaderView.isHidden = true
                    self.scrollView.isHidden = false
                    
                    let yearArr = dataDict.getObj(Utils.message_str).getArrObj("year")
                    
                    let carlist = dataDict.getObj(Utils.message_str).getArrObj("carlist")
                    let vehicletypelist = dataDict.getObj(Utils.message_str).getArrObj("vehicletypelist")
                    
                    var selectedYearPosition = -1
                    for i in 0..<yearArr.count{
                        self.yearListArr += [Configurations.convertNumToAppLocal(numStr: yearArr[i] as! String)]
                        
                        if(self.currentVehicleData != nil && (yearArr[i] as! String) == self.currentVehicleData.get("iYear")){
                            selectedYearPosition = i
                        }
                    }
                    
                    var vCarType = [String]()
                    
                    if(self.currentVehicleData != nil){
                        vCarType = self.currentVehicleData!.get("vCarType").components(separatedBy: ",")
                    }
                    
                    for i in 0..<vehicletypelist.count{
                        let item = vehicletypelist[i] as! NSDictionary
                        self.carTypeArr += [item]
                        self.carTypesStatusArr += [false]

                        if((self.currentVehicleData != nil && vCarType.contains(item.get("iVehicleTypeId"))) || (item.get("VehicleServiceStatus") != "" && item.get("VehicleServiceStatus") == "true")){
                            self.carTypesStatusArr[i] = true
                        }
                        
                    }
                    
                    var selectedMakePosition = -1
                    for i in 0..<carlist.count{
                        let item = carlist[i] as! NSDictionary
                        self.makeListArr += [item.get("vMake")]
                        
                        if(self.currentVehicleData != nil && item.get("iMakeId") == self.currentVehicleData.get("iMakeId")){
                            selectedMakePosition = i
                        }
                        
                        self.carlistArr += [item]
                    }
                    
                    self.generateCarTypes()
                    
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {
                        self.makeTxtField.getTextField()!.clearButtonMode = .never
                        self.makePicker = DownPicker(textField: self.makeTxtField.getTextField()!, withData: self.makeListArr as [AnyObject])
                        self.makePicker.setPlaceholder(self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_MAKE"))
                        self.makePicker.addTarget(self, action: #selector(self.makeChanged), for: .valueChanged)
                        
                        if(selectedMakePosition != -1){
                            self.makePicker.selectedIndex = selectedMakePosition
                            self.makeChanged()
                        }
                        
                        self.yearTxtField.getTextField()!.clearButtonMode = .never
                        self.yearPicker = DownPicker(textField: self.yearTxtField.getTextField()!, withData: self.yearListArr as [AnyObject])
                        self.yearPicker.setPlaceholder(self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_YEAR"))
                        self.yearPicker.addTarget(self, action: #selector(self.yearChanged), for: .valueChanged)
                        
                        if(selectedYearPosition != -1){
                            self.yearPicker.selectedIndex = selectedYearPosition
                        }
                    })
                    
                }else{
                    self.generalFunc.setError(uv: self, title: "", content: self.generalFunc.getLanguageLabel(origValue: "", key: dataDict.get("message")))
                }
                
            }else{
                self.generalFunc.setError(uv: self)
            }
            
            
        })
    }
    
    func makeChanged(){
        
        if(self.makePicker.selectedIndex != -1){
            let item = self.carlistArr[self.makePicker.selectedIndex]
            let vModellist = item.getArrObj("vModellist")
            
            self.modelListArr.removeAll()
            self.modelsArr.removeAll()
            
            var selectedModelPosition = -1
            
            for i in 0..<vModellist.count{
                let item = vModellist[i] as! NSDictionary
                self.modelListArr += [item.get("vTitle")]
                
                self.modelsArr += [item]
                
                if(self.currentVehicleData != nil && item.get("iModelId") == self.currentVehicleData.get("iModelId")){
                    selectedModelPosition = i
                }
            }
            
            self.modelTxtField.setText(text: "")
            
            var isSetModel = false
            if(modelPicker == nil && self.currentVehicleData != nil && selectedModelPosition != -1){
                isSetModel = true
            }
            
            self.modelTxtField.setEnable(isEnabled: true)
            self.modelTxtField.myTxtFieldDelegate = nil
            self.modelTxtField.getTextField()!.clearButtonMode = .never
            self.modelPicker = DownPicker(textField: self.modelTxtField.getTextField()!, withData: self.modelListArr as [AnyObject])
            self.modelPicker.setPlaceholder(self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_MODEL"))
            self.modelPicker.addTarget(self, action: #selector(self.modelChanged), for: .valueChanged)
            self.modelPicker.selectedIndex = -1
            
            if(isSetModel){
                self.modelPicker.selectedIndex = selectedModelPosition
                modelChanged()
            }
        }else{
            
            self.modelTxtField.setEnable(isEnabled: false)
            self.modelTxtField.myTxtFieldDelegate = self
        }
        
    }
    
    func yearChanged(){
    }
    
    func modelChanged(){
    }
    
    func generateCarTypes(){
        
        for i in 0..<carTypeArr.count{
            let item = carTypeArr[i]
            
            let carTypeItemView = CarTypeItemView(frame: CGRect(x:0,y:0, width: self.serviceContainerView.frame.width, height: 60))
//            let viewCus = self.generalFunc.loadView(nibName: "CarTypeItemView", uv: self, isWithOutSize: true)
            carTypeItemView.carTypeNameLbl.text = item.get("vVehicleType")

            carTypeItemView.carTypeChkBox.tag = i
            carTypeItemView.carTypeChkBox.boxType = .square
            carTypeItemView.carTypeChkBox.offAnimationType = .bounce
            carTypeItemView.carTypeChkBox.onAnimationType = .bounce
            carTypeItemView.carTypeChkBox.onCheckColor = UIColor.UCAColor.AppThemeTxtColor
            carTypeItemView.carTypeChkBox.onFillColor = UIColor.UCAColor.AppThemeColor
            carTypeItemView.carTypeChkBox.onTintColor = UIColor.UCAColor.AppThemeColor
            carTypeItemView.carTypeChkBox.tintColor = UIColor.UCAColor.AppThemeColor_1
//            carTypeSwitch.addTarget(self, action: #selector(self.carTypeStatusChanged), for: .valueChanged)

            if(self.carTypesStatusArr[i] == true){
                carTypeItemView.carTypeChkBox.setOn(true, animated: true)
            }
            carTypeItemView.carTypeChkBox.delegate = self
            
            self.serviceContainerView.addArrangedSubview(carTypeItemView)
        }
        
        
        self.serviceContainerView.frame.size = CGSize(width: self.view.frame.width, height: CGFloat(self.carTypeArr.count * 60))
        self.serviceContainerViewHeight.constant = CGFloat(self.carTypeArr.count * 60)
        
        _ = self.contentView.frame.height + self.serviceContainerViewHeight.constant + 40
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {
            
            self.cntView.frame.size = CGSize(width: self.contentView.frame.width, height: self.submitBtn.frame.maxY + 20)
            self.scrollView.contentSize = CGSize(width: self.scrollView.contentSize.width, height: self.submitBtn.frame.maxY + 20)
        })
    }
    
    func didTap(_ checkBox: BEMCheckBox) {
        self.carTypesStatusArr[checkBox.tag] = checkBox.on
    }
    
    
//    func carTypeStatusChanged(sender:UISwitch){
//        self.carTypesStatusArr[sender.tag] = sender.isOn
//    }
    
    func myBtnTapped(sender: MyButton) {
        if(sender == self.submitBtn){
            checkData()
        }
    }
    
    func checkData(){
        
        self.view.endEditing(true)
        
        if(userProfileJson.get("APP_TYPE") != Utils.cabGeneralType_UberX){
            if(self.makePicker.selectedIndex == -1){
                Utils.showSnakeBar(msg: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_CHOOSE_MAKE"), uv: self)
                return
            }
            
            
            if(self.modelPicker.selectedIndex == -1){
                Utils.showSnakeBar(msg: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_CHOOSE_VEHICLE_MODEL"), uv: self)
                return
            }
            
            
            if(self.yearPicker.selectedIndex == -1){
                Utils.showSnakeBar(msg: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_CHOOSE_YEAR"), uv: self)
                return
            }
            
            if(Utils.checkText(textField: self.licPlatTxtField.getTextField()!) == false){
                Utils.showSnakeBar(msg: self.generalFunc.getLanguageLabel(origValue: "Please add your car's licence plate no.", key: "LBL_ADD_LICENCE_PLATE"), uv: self)
                return
            }
        }
        
        var isCarTypeSelected = false
        var carTypes_str = ""
        for i in 0..<self.carTypesStatusArr.count{
            if(self.carTypesStatusArr[i] == true){
                isCarTypeSelected = true
                let carTypeId = self.carTypeArr[i].get("iVehicleTypeId")
                carTypes_str = carTypes_str == "" ? carTypeId : (carTypes_str + "," + carTypeId)
            }
        }
        
        if(isCarTypeSelected == false){
            Utils.showSnakeBar(msg: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_SELECT_CAR_TYPE"), uv: self)
            return
        }
        
        if(userProfileJson.get("APP_TYPE") != Utils.cabGeneralType_UberX){
            let vMakeId = self.carlistArr[self.makePicker.selectedIndex].get("iMakeId")
            let vModelId = self.modelsArr[self.modelPicker.selectedIndex].get("iModelId")
            updateCarDetails(carTypes: carTypes_str, vMakeId: vMakeId, vModelId: vModelId)
        }else{
            
            updateCarDetails(carTypes: carTypes_str, vMakeId: "", vModelId: "")
        }
    }
    
    func updateCarDetails(carTypes:String, vMakeId:String, vModelId:String){
        
        let parameters = ["type":"UpdateDriverVehicle","iDriverId": GeneralFunctions.getMemberd(), "UserType": Utils.appUserType, "iMakeId": vMakeId, "iModelId": vModelId, "iYear": Configurations.convertNumToEnglish(numStr:  Utils.getText(textField: self.yearTxtField.getTextField()!)), "vLicencePlate": Utils.getText(textField: self.licPlatTxtField.getTextField()!), "vCarType": carTypes, "iDriverVehicleId": iDriverVehicleId, "vColor": Utils.getText(textField: self.colorTxtField.getTextField()!), "HandiCap": "\(self.handiCapChkBox.on == true ? "Yes" : "No")"]
        
        let exeWebServerUrl = ExeServerUrl(dict_data: parameters, currentView: self.view, isOpenLoader: true)
        exeWebServerUrl.setDeviceTokenGenerate(isDeviceTokenGenerate: false)
        exeWebServerUrl.currInstance = exeWebServerUrl
        exeWebServerUrl.executePostProcess(completionHandler: { (response) -> Void in
            
            if(response != ""){
                let dataDict = response.getJsonDataDict()
                
                if(dataDict.get("Action") == "1"){
                    self.generalFunc.setAlertMessage(uv: self, title: "", content: self.generalFunc.getLanguageLabel(origValue: "", key: dataDict.get("message")), positiveBtn: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_BTN_OK_TXT"), nagativeBtn: "", completionHandler: { (btnClickedId) in
                        
                        if(self.isFromMainPage == true && self.mainScreenUv != nil){
                            self.closeCurrentScreen()
                            if(self.userProfileJson.get("APP_TYPE") != Utils.cabGeneralType_UberX){
                                self.mainScreenUv.openManageVehiclesScreen()
                            }
                            return
                        }
                        
                        if(self.userProfileJson.get("APP_TYPE") != Utils.cabGeneralType_UberX){
                            self.performSegue(withIdentifier: "unwindToManageVehicles", sender: self)
                        }

                    })
                    
                }else{
                    self.generalFunc.setError(uv: self, title: "", content: self.generalFunc.getLanguageLabel(origValue: "", key: dataDict.get("message")))
                }
                
            }else{
                self.generalFunc.setError(uv: self)
            }
            
        })
        
    }
    
}
