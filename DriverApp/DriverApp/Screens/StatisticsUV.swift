//
//  StatisticsUV.swift
//  DriverApp
//
//  Created by NEW MAC on 03/06/17.
//  Copyright © 2017 V3Cube. All rights reserved.
//

import UIKit

class StatisticsUV: UIViewController {

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var yearTxtField: MyTextField!
    @IBOutlet weak var numOfTripHLbl: MyLabel!
    @IBOutlet weak var numOfTripVLbl: MyLabel!
    @IBOutlet weak var totalEarningHLbl: MyLabel!
    @IBOutlet weak var totalEarningVLbl: MyLabel!
    @IBOutlet weak var graphView: UIView!
    
    let generalFunc = GeneralFunctions()
    
    let scrollView = UIScrollView()
    
    var isPageLoaded = false
    
    var cntView:UIView!
    
    var loaderView:UIView!
    
    var window:UIWindow!
    
    var yearPicker:DownPicker!
    
    var yearListArr = [String]()
    
    // Data
    let numberOfDataItems = 12
    
    var scGraphView:ScrollableGraphView!
    
//    lazy var data: [Double] = self.generateRandomData(self.numberOfDataItems, max: 50)
//    lazy var labels: [String] = self.generateSequentialLabels(self.numberOfDataItems, text: "FEB")
    
    var currentSelectedYear = ""
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.configureRTLView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        window = Application.window!
        
        self.view.translatesAutoresizingMaskIntoConstraints = true
        
        self.contentView.addSubview(scrollView)
        
        cntView = self.generalFunc.loadView(nibName: "StatisticsScreenDesign", uv: self, contentView: contentView)
        
        scrollView.addSubview(cntView)
        
//        self.contentView.addSubview(self.generalFunc.loadView(nibName: "StatisticsScreenDesign", uv: self, contentView: contentView))
        
        self.addBackBarBtn()
        
//        scGraphView.set(data: data, withLabels: labels)
        
//        setData()
    }

    override func viewDidAppear(_ animated: Bool) {
        if(isPageLoaded == false){
            scrollView.frame = self.contentView.frame
            
            
            let screenHeight = Application.screenSize.height - self.navigationController!.navigationBar.frame.height - UIApplication.shared.statusBarFrame.height
            
            cntView.frame.size = CGSize(width: contentView.frame.width, height: (self.contentView.frame.height > screenHeight ? self.contentView.frame.height : screenHeight))
            
            let yPosition = ((cntView.frame.height / 2) - (self.contentView.frame.height / 2)) >= 0 ? ((cntView.frame.height / 2) - (self.contentView.frame.height / 2)) : contentView.bounds.midY
            
            cntView.center = CGPoint(x: contentView.bounds.midX, y: contentView.bounds.midY + yPosition)
            
            scrollView.setContentViewSize()
            scrollView.bounces = false
            scrollView.backgroundColor = UIColor.clear
            
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {
                self.yearTxtField.addArrowView(color: UIColor(hex: 0xbcbcbc), transform: CGAffineTransform(rotationAngle: 90 * CGFloat(CGFloat.pi/180)))
            })
            
            scGraphView = createDarkGraph(CGRect(x:0,y:0, width:contentView.frame.width, height: self.graphView.frame.height))
            
            
            self.graphView.addSubview(scGraphView)
            setData()
            
            getData(currentSelectedYear: currentSelectedYear)
            
            yearTxtField.disableMenu()
            isPageLoaded = true
        }
    }
    
    fileprivate func createDarkGraph(_ frame: CGRect) -> ScrollableGraphView {
        let graphView = ScrollableGraphView(frame: frame)
        
        graphView.lineWidth = 1
        graphView.lineColor = UIColor.UCAColor.AppThemeColor
        graphView.lineStyle = ScrollableGraphViewLineStyle.smooth
        
        graphView.shouldFill = true
        graphView.fillType = ScrollableGraphViewFillType.gradient
        graphView.fillGradientType = ScrollableGraphViewGradientType.linear
        graphView.fillGradientStartColor = UIColor.UCAColor.AppThemeColor.lighter(by: 50)!
        graphView.fillGradientEndColor = UIColor.UCAColor.AppThemeColor.lighter(by: 25)!
        
        graphView.dataPointSpacing = 80
        graphView.dataPointSize = 8
        graphView.dataPointFillColor = UIColor.UCAColor.AppThemeColor
        
        graphView.referenceLineLabelFont = UIFont (name: "Roboto-Light", size: 14)!
        graphView.referenceLineColor = UIColor.clear
        graphView.referenceLineLabelColor = UIColor.clear
        graphView.numberOfIntermediateReferenceLines = 0
        graphView.dataPointLabelColor = UIColor(hex: 0x7c7c7c)
        
        graphView.shouldAnimateOnStartup = true
        graphView.shouldAdaptRange = true
        graphView.adaptAnimationType = ScrollableGraphViewAnimationType.elastic
        graphView.animationDuration = 1.5
        graphView.rangeMax = 50
        graphView.shouldRangeAlwaysStartAtZero = true
        
        return graphView
    }
    
    func setData(){
        self.navigationItem.title = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_TRIP_STATISTICS_TXT")
        self.title = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_TRIP_STATISTICS_TXT")
        
        self.yearTxtField.setPlaceHolder(placeHolder: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_YEAR"))
        self.totalEarningHLbl.text = self.generalFunc.getLanguageLabel(origValue: "Total Earnings", key: "LBL_TOTAL_EARNINGS")
        self.numOfTripHLbl.text = self.generalFunc.getLanguageLabel(origValue: "Number of trips", key: "LBL_NUMBER_OF_TRIPS")
        
    }
    
    func getData(currentSelectedYear:String){
        scrollView.isHidden = true
        loaderView =  self.generalFunc.addMDloader(contentView: self.view)
        loaderView.backgroundColor = UIColor.clear
        
        self.yearListArr.removeAll()
        
        let parameters = ["type":"getYearTotalEarnings","iDriverId": GeneralFunctions.getMemberd(), "UserType": Utils.appUserType, "year": currentSelectedYear]
        
        let exeWebServerUrl = ExeServerUrl(dict_data: parameters, currentView: self.view, isOpenLoader: false)
        exeWebServerUrl.setDeviceTokenGenerate(isDeviceTokenGenerate: false)
        exeWebServerUrl.currInstance = exeWebServerUrl
        exeWebServerUrl.executePostProcess(completionHandler: { (response) -> Void in
            
            if(response != ""){
                let dataDict = response.getJsonDataDict()
                
                if(dataDict.get("Action") == "1"){
                    
                    self.loaderView.isHidden = true
                    self.scrollView.isHidden = false
                    
                    let yearArr = dataDict.getObj(Utils.message_str).getArrObj("YearArr")
                    
                    var data = [Double]()
                    var labels = [String]()
                    
                    for i in 0..<yearArr.count{
                        self.yearListArr += [yearArr[i] as! String]
                    }
                    
                    let yearMonthArr = dataDict.getObj(Utils.message_str).getArrObj("YearMonthArr")
                    
                    for i in 0..<yearMonthArr.count{
                        let item = yearMonthArr[i] as! NSDictionary
                        
                        labels += [item.get("CurrentMonth")]
                        
                        data += [GeneralFunctions.parseDouble(origValue: 0.0, data: item.get("TotalEarnings"))]
                        
                        
                    }
                    
                    if(self.yearPicker == nil){
                    
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {
                        
                            self.yearTxtField.getTextField()!.clearButtonMode = .never
                            self.yearPicker = DownPicker(textField: self.yearTxtField.getTextField()!, withData: self.yearListArr as [AnyObject])
                            self.yearPicker.setPlaceholder(self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_YEAR"))
                            self.yearPicker.addTarget(self, action: #selector(self.yearChanged), for: .valueChanged)
                            
                            self.yearPicker.selectedIndex = 0
                        
                    })
                    }
                    if(self.scGraphView != nil){
                        self.scGraphView.removeFromSuperview()
                    }
                    
                    self.scGraphView = self.createDarkGraph(CGRect(x:0,y:0, width:self.contentView.frame.width, height: self.graphView.frame.height))
                    self.scGraphView.rangeMax = GeneralFunctions.parseDouble(origValue: 0.0, data: dataDict.get("MaxEarning"))
                    self.scGraphView.shouldAnimateOnAdapt = false
                    self.scGraphView.shouldAnimateOnStartup = false
                    self.totalEarningVLbl.text = dataDict.get("TotalEarning")
                    self.numOfTripVLbl.text = dataDict.get("TripCount")
                    self.scGraphView.set(data: data, withLabels: labels)
                    self.graphView.addSubview(self.scGraphView)
                    
                }else{
                    self.generalFunc.setError(uv: self, title: "", content: self.generalFunc.getLanguageLabel(origValue: "", key: dataDict.get("message")))
                }
                
            }else{
                self.generalFunc.setError(uv: self)
            }
        })
    }
    
    func yearChanged(){
        self.getData(currentSelectedYear: Utils.getText(textField: self.yearTxtField.getTextField()!))
    }

    // Data Generation
    private func generateRandomData(_ numberOfItems: Int, max: Double) -> [Double] {
        var data = [Double]()
        for _ in 0 ..< numberOfItems {
            var randomNumber = Double(arc4random()).truncatingRemainder(dividingBy: max)
            
            if(arc4random() % 100 < 10) {
                randomNumber *= 3
            }
            
            data.append(randomNumber)
        }
        return data
    }
    
    private func generateSequentialLabels(_ numberOfItems: Int, text: String) -> [String] {
        var labels = [String]()
        for i in 0 ..< numberOfItems {
            labels.append("\(text) \(i+1)")
        }
        return labels
    }
}
