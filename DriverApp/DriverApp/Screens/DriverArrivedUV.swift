//
//  DriverArrivedUV.swift
//  DriverApp
//
//  Created by NEW MAC on 26/05/17.
//  Copyright © 2017 V3Cube. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation

class DriverArrivedUV: UIViewController, GMSMapViewDelegate, OnLocationUpdateDelegate, OnTripCanceledDelegate, MyBtnClickDelegate {

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var googleMapContainerView: UIView!
    @IBOutlet weak var navigateView: UIView!
    @IBOutlet weak var navigateViewHeight: NSLayoutConstraint!
    @IBOutlet weak var arrivedBtn: MyButton!
    @IBOutlet weak var emeImgView: UIImageView!
    
    
    let generalFunc = GeneralFunctions()
    
    var isPageLoaded = false
    
    var currentLocation:CLLocation!
    var currentRotatedLocation:CLLocation!
    var currentHeading:Double = 0
    var isFirstHeadingCompleted = false
    
    var gMapView:GMSMapView!
    
    var navView:UIView!
    
    var window:UIWindow!
    var configPubNub:ConfigPubNub?
    
    var getLocation:GetLocation!
    
    var isFirstLocationUpdate = true
    
    var tripData:NSDictionary!
    
    
    var menu:BTNavigationDropdownMenu!
    
    var updateDriverLoc:UpdateDriverLocations!
    
    var updateDirections:UpdateDirections!
    
    let driverMarker: GMSMarker = GMSMarker()
    let passengerMarker: GMSMarker = GMSMarker()
    
    var locationDialog:OpenLocationEnableView!
    
//    var locationArr = [CLLocation]()
    
    var currPubLoc:CLLocation!
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.configureRTLView()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if(isPageLoaded == false){
            
            isPageLoaded = true
            
            navView = self.generalFunc.loadView(nibName: "navigationVIew", uv: self, isWithOutSize: true)
            
            navView.frame = CGRect(x:0, y:0, width: navigateView.frame.width, height: 94.0)
//            CGSize(width: navigateView.frame.width, height: 94)
            
//            navView.center = CGPoint(x: navigateView.bounds.midX, y: navigateView.bounds.midY)
            navigateView.addSubview(navView)
            
            let camera = GMSCameraPosition.camera(withLatitude: -180, longitude: -180, zoom: 15.0)
            gMapView = GMSMapView.map(withFrame: self.googleMapContainerView.frame, camera: camera)
            //        googleMapContainerView = gMapView
            //        gMapView = GMSMapView()
//            gMapView.isMyLocationEnabled = true
            gMapView.settings.rotateGestures = false
            gMapView.settings.tiltGestures = false
            gMapView.delegate = self
            self.googleMapContainerView.addSubview(gMapView)
            setData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        window = Application.window!
        
        Utils.driverMarkersPositionList.removeAll()
        Utils.driverMarkerAnimFinished = true
        
        self.contentView.addSubview(self.generalFunc.loadView(nibName: "DriverArrivedScreenDesign", uv: self, contentView: contentView))
        
        self.emeImgView.isHidden = true
        
        if(GeneralFunctions.getValue(key: "OPEN_MSG_SCREEN") != nil && (GeneralFunctions.getValue(key: "OPEN_MSG_SCREEN") as! String) == "true"){
            let chatUV = GeneralFunctions.instantiateViewController(pageName: "ChatUV") as! ChatUV
            
            GeneralFunctions.removeValue(key: "OPEN_MSG_SCREEN")
            
            chatUV.receiverId = tripData!.get("PassengerId")
            chatUV.receiverDisplayName = self.tripData!.get("PName")
            chatUV.assignedtripId = self.tripData!.get("TripId")
            self.pushToNavController(uv:chatUV, isDirect: true)
            
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.releaseAllTask), name: NSNotification.Name(rawValue: Utils.releaseAllTaskObserverKey), object: nil)
        
//        locationArr += [CLLocation(latitude: 23.011794, longitude: 72.502510)]
//        locationArr += [CLLocation(latitude: 23.011793, longitude: 72.502526)]
//        locationArr += [CLLocation(latitude: 23.011793, longitude: 72.502543)]
//        locationArr += [CLLocation(latitude: 23.011793, longitude: 72.502560)]
//        locationArr += [CLLocation(latitude: 23.011794, longitude: 72.502579)]
//        locationArr += [CLLocation(latitude: 23.011794, longitude: 72.502599)]
//        locationArr += [CLLocation(latitude: 23.011794, longitude: 72.502622)]
//        locationArr += [CLLocation(latitude: 23.011791, longitude: 72.502636)]
//        locationArr += [CLLocation(latitude: 23.011811, longitude: 72.50264 )]
//        locationArr += [CLLocation(latitude: 23.011831, longitude: 72.502654)]
//        locationArr += [CLLocation(latitude: 23.011852, longitude: 72.502658)]
//        locationArr += [CLLocation(latitude: 23.011851, longitude: 72.502671)]
//        locationArr += [CLLocation(latitude: 23.011848, longitude: 72.502685)]
//        locationArr += [CLLocation(latitude: 23.011846, longitude: 72.502694)]
//        locationArr += [CLLocation(latitude: 23.011827, longitude: 72.502703)]
//        locationArr += [CLLocation(latitude: 23.011814, longitude: 72.502697)]
//        locationArr += [CLLocation(latitude: 23.011799, longitude: 72.502698)]
//        locationArr += [CLLocation(latitude: 23.011799, longitude: 72.502716)]
//        locationArr += [CLLocation(latitude: 23.011798, longitude: 72.502733)]
//        locationArr += [CLLocation(latitude: 23.011795, longitude: 72.502749)]
//        locationArr += [CLLocation(latitude: 23.011795, longitude: 72.502764)]
//        locationArr += [CLLocation(latitude: 23.011796, longitude: 72.502780)]
//        locationArr += [CLLocation(latitude: 23.011796, longitude: 72.502797)]
//        locationArr += [CLLocation(latitude: 23.011794, longitude: 72.502811)]
//        locationArr += [CLLocation(latitude: 23.011798, longitude: 72.502824)]
//        locationArr += [CLLocation(latitude: 23.011795, longitude: 72.502842)]
//        locationArr += [CLLocation(latitude: 23.011796, longitude: 72.502859)]
//        locationArr += [CLLocation(latitude: 23.011794, longitude: 72.502879)]
//        locationArr += [CLLocation(latitude: 23.011814, longitude: 72.502883)]
//        locationArr += [CLLocation(latitude: 23.011831, longitude: 72.502890)]
//        locationArr += [CLLocation(latitude: 23.011856, longitude: 72.502899)]
//        locationArr += [CLLocation(latitude: 23.011853, longitude: 72.502914)]
//        locationArr += [CLLocation(latitude: 23.011852, longitude: 72.502930)]
//        locationArr += [CLLocation(latitude: 23.011841, longitude: 72.502943)]
//        locationArr += [CLLocation(latitude: 23.011820, longitude: 72.502937)]
//        locationArr += [CLLocation(latitude: 23.011801, longitude: 72.502935)]
//        locationArr += [CLLocation(latitude: 23.011784, longitude: 72.502933)]
//        locationArr += [CLLocation(latitude: 23.011789, longitude: 72.502953)]
//        locationArr += [CLLocation(latitude: 23.011790, longitude: 72.502969)]
//        locationArr += [CLLocation(latitude: 23.011793, longitude: 72.502988)]
//        locationArr += [CLLocation(latitude: 23.011799, longitude: 72.503004)]
//        locationArr += [CLLocation(latitude: 23.011796, longitude: 72.503020)]
//        locationArr += [CLLocation(latitude: 23.011791, longitude: 72.503039)]
//        locationArr += [CLLocation(latitude: 23.011791, longitude: 72.503056)]
//        locationArr += [CLLocation(latitude: 23.011793, longitude: 72.503080)]
//        locationArr += [CLLocation(latitude: 23.011798, longitude: 72.503110)]
//        locationArr += [CLLocation(latitude: 23.011775, longitude: 72.503135)]
//        locationArr += [CLLocation(latitude: 23.011758, longitude: 72.503145)]
//        locationArr += [CLLocation(latitude: 23.011756, longitude: 72.503167)]
//        locationArr += [CLLocation(latitude: 23.011777, longitude: 72.503190)]
//        locationArr += [CLLocation(latitude: 23.011803, longitude: 72.503192)]
//        locationArr += [CLLocation(latitude: 23.011800, longitude: 72.503162)]
//        locationArr += [CLLocation(latitude: 23.011794, longitude: 72.503126)]
//        locationArr += [CLLocation(latitude: 23.011795, longitude: 72.503091)]
//        locationArr += [CLLocation(latitude: 23.011753, longitude: 72.503096)]
//        locationArr += [CLLocation(latitude: 23.011750, longitude: 72.503133)]
//        locationArr += [CLLocation(latitude: 23.011753, longitude: 72.503186)]
//        locationArr += [CLLocation(latitude: 23.011758, longitude: 72.503239)]
//        locationArr += [CLLocation(latitude: 23.011764, longitude: 72.503303)]
//        locationArr += [CLLocation(latitude: 23.011793, longitude: 72.503342)]
//        
//        scheduleLocUpdates()
        
    }
//    var locCount = 0
//    
//    func scheduleLocUpdates(){
//        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(2 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {
//            Utils.printLog(msgData: "Location Task Called")
//            
//            self.onLocationUpdate(location: self.locationArr[self.locCount])
//            self.locCount = self.locCount + 1
//            
//            if(self.locCount < self.locationArr.count){
//                self.scheduleLocUpdates()
//            }
//            
//        })
//    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    deinit {
        releaseAllTask()
    }

    func setData(){
        if(self.tripData!.get("REQUEST_TYPE") == Utils.cabGeneralType_Deliver){
            self.navigationItem.title = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_PICKUP_DELIVERY")
            self.title = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_PICKUP_DELIVERY")
        }else{
            self.navigationItem.title = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_PICK_UP_PASSENGER")
            self.title = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_PICK_UP_PASSENGER")
        }
        
        let rightButton = UIBarButtonItem(image: UIImage(named: "ic_menu")!, style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.openPopUpMenu))
        self.navigationItem.rightBarButtonItem = rightButton
        
        self.arrivedBtn.setButtonTitle(buttonTitle: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_BTN_ARRIVED_TXT"))
        
        self.arrivedBtn.clickDelegate = self
        
        if(self.getPubNubConfig().uppercased() == "YES"){
            configPubNub = ConfigPubNub()
            configPubNub!.iTripId = self.tripData.get("TripId")
            configPubNub!.buildPubNub()
        }else{
            
            self.updateDriverLoc = UpdateDriverLocations(uv: self)
            self.updateDriverLoc.scheduleDriverLocUpdate()
        }
        
        
        self.getLocation = GetLocation(uv: self, isContinuous: true)
        self.getLocation.buildLocManager(locationUpdateDelegate: self)
        
        (self.navView.subviews[0].subviews[1].subviews[2] as! MyLabel).text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_NAVIGATE")
        self.navView.subviews[0].subviews[1].backgroundColor = UIColor.UCAColor.darkGreen
//        self.navView.subviews[0].subviews[1].subviews[0].backgroundColor = UIColor(hex: 0xFFFFFF)
        GeneralFunctions.setImgTintColor(imgView: (self.navView.subviews[0].subviews[1].subviews[1] as! UIImageView), color: UIColor.UCAColor.AppThemeTxtColor)
        (self.navView.subviews[0].subviews[1].subviews[2] as! MyLabel).textColor = UIColor.UCAColor.AppThemeTxtColor
        
        let navViewTapGue = UITapGestureRecognizer()
        navViewTapGue.addTarget(self, action: #selector(self.navViewTapped))
        self.navView.subviews[0].subviews[1].isUserInteractionEnabled = true
        self.navView.subviews[0].subviews[1].addGestureRecognizer(navViewTapGue)
        
        self.observeCancelTripRequest()
        initializeMenu()
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.releaseAllTask), name: NSNotification.Name(rawValue: Utils.releaseAllTaskObserverKey), object: nil)
        
        let passengerLocation = CLLocation(latitude: GeneralFunctions.parseDouble(origValue: 0.0, data: tripData!.get("sourceLatitude")), longitude: GeneralFunctions.parseDouble(origValue: 0.0, data: tripData!.get("sourceLongitude")))
        updateDirections = UpdateDirections(uv: self, gMap: gMapView, destinationLocation: passengerLocation, navigateView: navView)
        updateDirections.scheduleDirectionUpdate(eTollSkipped: "")
        
        self.addPassengerMarker(location: passengerLocation)
        
        self.emeImgView.isUserInteractionEnabled = true
        
        self.emeImgView.isHidden = false
        let emeTapGue = UITapGestureRecognizer()
        emeTapGue.addTarget(self, action: #selector(self.emeImgViewTapped))
        self.emeImgView.addGestureRecognizer(emeTapGue)
        
        checkLocationEnabled()
        
        addBackgroundObserver()
        
        
    }
    
    func addBackgroundObserver(){
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: Utils.appFGNotificationKey), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.appInForground), name: NSNotification.Name(rawValue: Utils.appFGNotificationKey), object: nil)
    }
    
    func checkLocationEnabled(){
        if(locationDialog != nil){
            locationDialog.removeView()
            locationDialog = nil
        }
        
        if(GeneralFunctions.hasLocationEnabled() == false || InternetConnection.isConnectedToNetwork() == false){
            
            locationDialog = OpenLocationEnableView(uv: self, containerView: self.contentView, gMapView: self.gMapView, isMapLocEnabled: false)
            locationDialog.show()
            
            return
        }
    }
    
    func appInForground(){
        checkLocationEnabled()
        
        if(self.configPubNub != nil){
            self.configPubNub!.unSubscribeToPrivateChannel()
            self.configPubNub!.subscribeToPrivateChannel()
        }
    }
    
    func emeImgViewTapped(){
        let confirmEmergencyTapUV = GeneralFunctions.instantiateViewController(pageName: "ConfirmEmergencyTapUV") as! ConfirmEmergencyTapUV
        confirmEmergencyTapUV.iTripId = tripData.get("TripId")
        self.pushToNavController(uv: confirmEmergencyTapUV)
    }
    
    func navViewTapped(){
        let openNavOption = OpenNavOption(uv: self, containerView: self.view, placeLatitude: tripData!.get("sourceLatitude"), placeLongitude: tripData!.get("sourceLongitude"))
        openNavOption.chooseOption()
    }
    
    func myBtnTapped(sender: MyButton) {
        if(sender == self.arrivedBtn){
            self.generalFunc.setAlertMessage(uv: self, title: "", content: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_ARRIVED_CONFIRM_DIALOG_TXT"), positiveBtn: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_BTN_OK_TXT"), nagativeBtn: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_CANCEL_TXT"), completionHandler: { (btnClickedId) in
                
                if(btnClickedId == 0){
                    self.setDriverStatusToArrived()
                }else{
                }
            })
        }
    }

    func setDriverStatusToArrived(){
        let parameters = ["type":"DriverArrived","iDriverId": GeneralFunctions.getMemberd()]
        
        let exeWebServerUrl = ExeServerUrl(dict_data: parameters, currentView: self.view, isOpenLoader: true)
        exeWebServerUrl.setDeviceTokenGenerate(isDeviceTokenGenerate: false)
        exeWebServerUrl.currInstance = exeWebServerUrl
        exeWebServerUrl.executePostProcess(completionHandler: { (response) -> Void in
            
            if(response != ""){
                let dataDict = response.getJsonDataDict()
                
                if(dataDict.get("Action") == "1"){
                    
                    let window = Application.window
                    
                    let getUserData = GetUserData(uv: self, window: window!)
                    getUserData.getdata()
                    
                }else if(dataDict.get(Utils.message_str) == "DO_RESTART" || dataDict.get("message") == "LBL_SERVER_COMM_ERROR" || dataDict.get("message") == "GCM_FAILED" || dataDict.get("message") == "APNS_FAILED"){
                    let window = Application.window
                    
                    let getUserData = GetUserData(uv: self, window: window!)
                    getUserData.getdata()
                }else{
                    self.generalFunc.setError(uv: self, title: "", content: self.generalFunc.getLanguageLabel(origValue: "", key: dataDict.get("message")))
                }
                
            }else{
                self.generalFunc.setError(uv: self)
            }
        })
    }
    
    func releaseAllTask(isDismiss:Bool = true){
        
        if(gMapView != nil){
            gMapView!.stopRendering()
            gMapView!.removeFromSuperview()
            gMapView!.clear()
            gMapView!.delegate = nil
            gMapView = nil
        }
        
        if(configPubNub != nil){
            configPubNub!.releasePubNub()
        }
        
        if(self.getLocation != nil){
            self.getLocation!.locationUpdateDelegate = nil
            self.getLocation!.releaseLocationTask()
            self.getLocation = nil
        }
        
        
        if(updateDriverLoc != nil){
            self.updateDriverLoc.releaseTask()
            self.updateDriverLoc = nil
        }
        
        if(updateDirections != nil){
            self.updateDirections.releaseTask()
            if(self.updateDirections.gMap != nil){
                self.updateDirections.gMap!.stopRendering()
                self.updateDirections.gMap!.removeFromSuperview()
                self.updateDirections.gMap!.clear()
                self.updateDirections.gMap!.delegate = nil
                self.updateDirections.gMap = nil
            }
            self.updateDirections = nil
        }
        
        GeneralFunctions.removeObserver(obj: self)
        
        
        if(isDismiss){
            self.dismiss(animated: false, completion: nil)
            self.navigationController?.dismiss(animated: false, completion: nil)
        }
    }
    
    
    func onHeadingUpdate(heading: Double) {
//        driverMarker.isFlat = true
//        driverMarker.rotation = heading
//       
//        self.gMapView.animate(toBearing: heading - 20)
        currentHeading = heading
        
        if(isFirstHeadingCompleted == false){
            updateDriverMarker()
            isFirstHeadingCompleted = true
        }
    }
    
    
    func onLocationUpdate(location: CLLocation) {
        if(gMapView == nil){
            releaseAllTask()
            return
        }
        
        self.currentLocation = location
        
        var currentZoomLevel:Float = self.gMapView.camera.zoom
        
        if(currentZoomLevel < 15.0 && isFirstLocationUpdate == true){
            currentZoomLevel = 15.0
        }
        let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude,
                                              longitude: location.coordinate.longitude, zoom: currentZoomLevel)
        
        self.gMapView.animate(to: camera)
        
        isFirstLocationUpdate = false
        
        updateLocationToPubNub()
        
        updateDriverMarker()
    }
    
    func updateDriverMarker(){
//        driverMarker.position = self.currentLocation.coordinate
        if(currentLocation == nil){
            return
        }
        driverMarker.title = GeneralFunctions.getMemberd()
        
        var rotationAngle:Double = 0
        if(currentRotatedLocation == nil){
            rotationAngle = currentHeading
            
            if(currentHeading > 1){
                currentRotatedLocation = currentLocation
            }
        }else{
            rotationAngle = currentRotatedLocation.bearingToLocationDegrees(destinationLocation: currentLocation, currentRotation: driverMarker.rotation)
            if(rotationAngle == -1){
                rotationAngle = currentHeading
            }else{
                currentRotatedLocation = currentLocation
            }
        }
        
//        Utils.updateMarker(marker: driverMarker, googleMap: self.gMapView, coordinates: currentLocation.coordinate, rotationAngle: rotationAngle, duration: 1.0)
        
//        if(dataDict != nil && self.driverMarker != nil){
        
            let previousItemOfMarker = Utils.getLastLocationDataOfMarker(marker: driverMarker)
            
            var tempData = [String:String]()
            tempData["vLatitude"] = "\(currentLocation.coordinate.latitude)"
            tempData["vLongitude"] = "\(currentLocation.coordinate.longitude)"
            tempData["iDriverId"] = "\(GeneralFunctions.getMemberd())"
            tempData["RotationAngle"] = "\(rotationAngle)"
            tempData["LocTime"] = "\(Utils.currentTimeMillis())"
            
            if(previousItemOfMarker.get("LocTime") != "" && (tempData as NSDictionary).get("LocTime") != ""){
                
                let locTime = Int64(previousItemOfMarker.get("LocTime"))
                let newLocTime = Int64((tempData as NSDictionary).get("LocTime"))
                
                if(locTime != nil && newLocTime != nil){
                    
                    if((newLocTime! - locTime!) > 0 && Utils.driverMarkerAnimFinished == false){
                        Utils.driverMarkersPositionList.append(tempData as NSDictionary)
                    }else if((newLocTime! - locTime!) > 0){
                        Utils.updateMarkerOnTrip(marker: driverMarker, googleMap: self.gMapView, coordinates: currentLocation.coordinate, rotationAngle: rotationAngle, duration: 0.8, iDriverId: GeneralFunctions.getMemberd(), LocTime: (tempData as NSDictionary).get("LocTime"))
                    }
                    
                }else if((locTime == nil || newLocTime == nil) && Utils.driverMarkerAnimFinished == false){
                    Utils.driverMarkersPositionList.append(tempData as NSDictionary)
                }else{
                    Utils.updateMarkerOnTrip(marker: driverMarker, googleMap: self.gMapView, coordinates: currentLocation.coordinate, rotationAngle: rotationAngle, duration: 0.8, iDriverId: GeneralFunctions.getMemberd(), LocTime: (tempData as NSDictionary).get("LocTime"))
                }
                
            }else if(Utils.driverMarkerAnimFinished == false){
                Utils.driverMarkersPositionList.append(tempData as NSDictionary)
            }else{
                Utils.updateMarkerOnTrip(marker: driverMarker, googleMap: self.gMapView, coordinates: currentLocation.coordinate, rotationAngle: rotationAngle, duration: 0.8, iDriverId: GeneralFunctions.getMemberd(), LocTime: (tempData as NSDictionary).get("LocTime"))
            }
            
//        }else{
//            Utils.updateMarkerOnTrip(marker: driverMarker, googleMap: self.gMapView, coordinates: currentLocation.coordinate, rotationAngle: rotationAngle, duration: 1.0, iDriverId: GeneralFunctions.getMemberd(), LocTime: "")
//        }

        
        driverMarker.icon = UIImage(named: "ic_driver_car_pin")
        driverMarker.map = self.gMapView
        driverMarker.infoWindowAnchor = CGPoint(x: 0.5, y: 0.5)
        driverMarker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
        driverMarker.isFlat = true
        driverMarker.title = GeneralFunctions.getMemberd()
        
        var currentZoomLevel:Float = gMapView.camera.zoom
        
        if(currentZoomLevel < 15.0){
            currentZoomLevel = 15.0
        }
        let camera = GMSCameraPosition.camera(withLatitude: self.currentLocation.coordinate.latitude,
                                                          longitude: self.currentLocation.coordinate.longitude, zoom: currentZoomLevel)
        
        self.gMapView.animate(to: camera)
    }
    
    func addPassengerMarker(location: CLLocation){
        
        passengerMarker.position = location.coordinate
        
        passengerMarker.icon = UIImage(named: "ic_passenger")
        passengerMarker.map = self.gMapView
        passengerMarker.infoWindowAnchor = CGPoint(x: 0.5, y: 0.5)
        
    }
    
    
    
    func updateLocationToPubNub(){
        if(currentLocation != nil)
        {
            if(currPubLoc == nil || currPubLoc.distance(from:currentLocation!) > 9){
                currPubLoc = currentLocation!
                configPubNub?.publishMsg(channelName: GeneralFunctions.getLocationUpdateChannel(), content: GeneralFunctions.buildLocationJson(location: currentLocation!, msgType: "LocationUpdateOnTrip"))
            }
            
        }
    }
    
//    func updateLocationToPubNub(){
//        if(currentLocation != nil){
//            configPubNub?.publishMsg(channelName: GeneralFunctions.getLocationUpdateChannel(), content: GeneralFunctions.buildLocationJson(location: currentLocation!, msgType: "LocationUpdateOnTrip"))
//        }
//    }
    
    func initializeMenu(){
        
        var items = [self.generalFunc.getLanguageLabel(origValue: "", key: tripData!.get("REQUEST_TYPE") == Utils.cabGeneralType_Deliver ? "LBL_VIEW_DELIVERY_DETAILS" : "LBL_VIEW_PASSENGER_DETAIL"), self.generalFunc.getLanguageLabel(origValue: "", key: tripData!.get("REQUEST_TYPE") == Utils.cabGeneralType_Deliver ? "LBL_CANCEL_DELIVERY" : "LBL_CANCEL_TRIP")]
        
        if(self.tripData!.get("eHailTrip").uppercased() == "YES"){
            items.remove(at: 0)
        }
        
        if(tripData!.get("REQUEST_TYPE").uppercased() != Utils.cabGeneralType_UberX.uppercased()){
            items += [self.generalFunc.getLanguageLabel(origValue: "Way Bill", key: "LBL_MENU_WAY_BILL")]
        }
        
        if(self.menu == nil){
            menu = BTNavigationDropdownMenu(navigationController: self.navigationController, title: items.first!, items: items as [AnyObject])
            
            menu.cellHeight = 65
            menu.cellBackgroundColor = UIColor.UCAColor.AppThemeColor.lighter(by: 10)
            menu.cellSelectionColor = UIColor.UCAColor.AppThemeColor
            menu.cellTextLabelColor = UIColor.UCAColor.AppThemeTxtColor
            menu.cellTextLabelFont = UIFont(name: "Roboto-Light", size: 20)
            menu.cellSeparatorColor = UIColor.UCAColor.AppThemeColor
            
            if(Configurations.isRTLMode()){
                menu.cellTextLabelAlignment = NSTextAlignment.right
            }else{
                menu.cellTextLabelAlignment = NSTextAlignment.left
            }
            menu.arrowPadding = 15
            menu.animationDuration = 0.5
            menu.maskBackgroundColor = UIColor.black
            menu.maskBackgroundOpacity = 0.5
            menu.menuStateHandler = { (isMenuOpen: Bool) -> () in
                
                //                if(isMenuOpen){
                //                    self.rightButton.setBackgroundImage(nil, for: .normal, barMetrics: .default)
                //
                //                }else{
                //                    self.rightButton.setBackgroundImage(UIImage(color : UIColor.UCAColor.AppThemeColor.lighter(by: 10)!), for: .normal, barMetrics: .default)
                //                }
                
            }
            menu.didSelectItemAtIndexHandler = {(indexPath: Int) -> () in
                
                var indexPath = indexPath
                if(self.tripData!.get("eHailTrip").uppercased() == "YES"){
                    indexPath = indexPath + 1
                }
                
                if(indexPath == 0){
                    let openPassengerDetail = OpenPassengerDetail(uv:self, containerView: self.contentView)
                    openPassengerDetail.tripData = self.tripData
                    openPassengerDetail.currInst = openPassengerDetail
                    openPassengerDetail.showDetail()
                    
                    
                }else if(indexPath == 1){
                    
                    let openCancelTrip = OpenCancelTrip(uv:self, containerView: self.contentView)
                    openCancelTrip.tripData = self.tripData
                    openCancelTrip.currInst = openCancelTrip
                    
                    openCancelTrip.setDelegate(onTripCanceledDelegate: self)
                    openCancelTrip.cancelTrip()
                }else if(indexPath == 2){
                    let wayBillUV = GeneralFunctions.instantiateViewController(pageName: "WayBillUV") as! WayBillUV
                    self.pushToNavController(uv: wayBillUV)
                }
            }
        }else{
            menu.updateItems(items as [AnyObject])
        }
    }
    
    func openPopUpMenu(){
        
        initializeMenu()
        
        if(menu.isShown){
            menu.hideMenu()
            return
        }else{
            menu.showMenu()
        }
    }
    
    func onTripViewClosed(openCancelTrip:OpenCancelTrip) {
        openCancelTrip.setDelegate(onTripCanceledDelegate: nil)
    }
    
    func onTripCanceled(reason: String, comment: String, openCancelTrip:OpenCancelTrip) {
        openCancelTrip.setDelegate(onTripCanceledDelegate: nil)
        
        let parameters = ["type":"cancelTrip","iDriverId": GeneralFunctions.getMemberd(), "iUserId": tripData!.get("PassengerId"), "iTripId": tripData!.get("TripId"), "UserType": Utils.appUserType, "Reason": reason, "Comment": comment]
        
        let exeWebServerUrl = ExeServerUrl(dict_data: parameters, currentView: self.view, isOpenLoader: true)
        exeWebServerUrl.setDeviceTokenGenerate(isDeviceTokenGenerate: false)
        exeWebServerUrl.currInstance = exeWebServerUrl
        exeWebServerUrl.executePostProcess(completionHandler: { (response) -> Void in
            
            if(response != ""){
                let dataDict = response.getJsonDataDict()
                
                if(dataDict.get("Action") == "1"){
                    
                    let window = Application.window
                    
                    let getUserData = GetUserData(uv: self, window: window!)
                    getUserData.getdata()
                    
                }else{
                    self.generalFunc.setError(uv: self, title: "", content: self.generalFunc.getLanguageLabel(origValue: "", key: dataDict.get("message")))
                }
                
            }else{
                self.generalFunc.setError(uv: self)
            }
        })
    }
}
