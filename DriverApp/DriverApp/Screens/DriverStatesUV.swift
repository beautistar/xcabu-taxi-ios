//
//  DriverStatesUV.swift
//  DriverApp
//
//  Created by NEW MAC on 19/06/17.
//  Copyright © 2017 V3Cube. All rights reserved.
//

import UIKit

class DriverStatesUV: UIViewController, UITableViewDelegate, UITableViewDataSource, MyBtnClickDelegate {

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    var window:UIWindow!
    
    let generalFunc = GeneralFunctions()
    
    var numOfItems = 0
    
    var selectedRow = -1
    
    var statusOfStates = [Bool]()
    
    var loaderView:UIView!
    
    var isPageLoaded = false
    
    var cntView:UIView!

    override func viewWillAppear(_ animated: Bool) {
        
        self.configureRTLView()
        
        if(statusOfStates.count > 0){
            reloadData()
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        cntView = self.generalFunc.loadView(nibName: "DriverStatesScreenDesign", uv: self, contentView: contentView)
        self.contentView.addSubview(cntView)
        
        window = Application.window!

        self.tableView.tableFooterView = UIView()
        self.tableView.register(UINib(nibName: "DriverStateTVCell", bundle: nil), forCellReuseIdentifier: "DriverStateTVCell")
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        showLoader()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if(isPageLoaded == false){
            
//            cntView.frame.size = self.view.frame.size
            reloadData()
            isPageLoaded = true
        }
    }
    
    override func viewDidLayoutSubviews() {
            if(cntView != nil){
                self.cntView.frame = self.view.frame
            }
        }
    
    func showLoader(){
        
        if(loaderView == nil){
            loaderView =  self.generalFunc.addMDloader(contentView: self.contentView)
            loaderView.backgroundColor = UIColor.clear
            loaderView.isHidden = false
        }else{
            loaderView.isHidden = false
        }
        
    }
    
    deinit {
        releaseAllTask()
    }
    
    func setData(){
    }
    
    func reloadData(){
        
        loadData()
    
    }
    
    func loadData(){
    
        self.statusOfStates.removeAll()
        
        showLoader()
        
//        if(loaderView != nil){
//            loaderView.removeFromSuperview()
//        }
        
//        loaderView =  self.generalFunc.addMDloader(contentView: self.contentView)
//        loaderView.backgroundColor = UIColor.clear
        
        
        let parameters = ["type":"getDriverStates","UserType": Utils.appUserType, "iDriverId": GeneralFunctions.getMemberd()]
        
        let exeWebServerUrl = ExeServerUrl(dict_data: parameters, currentView: self.view, isOpenLoader: false)
        exeWebServerUrl.setDeviceTokenGenerate(isDeviceTokenGenerate: false)
        exeWebServerUrl.currInstance = exeWebServerUrl
        exeWebServerUrl.executePostProcess(completionHandler: { (response) -> Void in
            
            if(response != ""){
                let dataDict = response.getJsonDataDict()
                
                if(dataDict.get("Action") == "1"){
                    
                    self.statusOfStates += [true]
                    
                    if(dataDict.get("IS_DOCUMENT_PROCESS_COMPLETED") != "Yes"){
                        self.statusOfStates += [false]
                    }else{
                        self.statusOfStates += [true]
                    }
                    
                    if(dataDict.get("IS_VEHICLE_PROCESS_COMPLETED") != "Yes"){
                        self.statusOfStates += [false]
                    }else{
                        self.statusOfStates += [true]
                    }
                    
                    if(dataDict.get("IS_DRIVER_STATE_ACTIVATED") != "Yes"){
                        self.statusOfStates += [false]
                    }else{
                        self.statusOfStates += [true]
                    }
                    
                    var isAllCompleted = true

                    for i in 0..<self.statusOfStates.count{
                        if(self.statusOfStates[i] == false){
                            isAllCompleted = false
                            break
                        }
                    }
                    
                    if(isAllCompleted == true){
                        let window = Application.window!
                        
                        let getUserData = GetUserData(uv: self, window: window)
                        getUserData.getdata()
                        return
                    }
                    self.numOfItems = 4
                    
                    self.tableView.reloadData()
                    
                }else{
                    self.generalFunc.setAlertMessage(uv: self, title: "", content: self.generalFunc.getLanguageLabel(origValue: "", key: dataDict.get(Utils.message_str)), positiveBtn: self.generalFunc.getLanguageLabel(origValue: "Retry", key: "LBL_RETRY_TXT"), nagativeBtn: "", completionHandler: { (btnClickedIndex) in
                        
                        self.loadData()
                    })
                }
                
            }else{
                self.generalFunc.setAlertMessage(uv: self, title: "", content: self.generalFunc.getLanguageLabel(origValue: "Please try again.", key: "LBL_TRY_AGAIN_TXT"), positiveBtn: self.generalFunc.getLanguageLabel(origValue: "Retry", key: "LBL_RETRY_TXT"), nagativeBtn: "", completionHandler: { (btnClickedIndex) in
                    
                    self.loadData()
                })
            }
            
            self.loaderView.isHidden = true
        })
    }

    func releaseAllTask(){
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return numOfItems
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DriverStateTVCell", for: indexPath) as! DriverStateTVCell
        
        if(indexPath.item == 0){
            cell.stateNumLbl.isHidden = true
            cell.stateFinishedImgView.isHidden = false
            cell.topSeperatorView.isHidden = true
        }else{
            cell.stateNumLbl.isHidden = false
            cell.stateFinishedImgView.isHidden = true
            cell.topSeperatorView.isHidden = false
        }
        
        
        switch indexPath.item {
        case 0:
            cell.stateTitleLbl.text = self.generalFunc.getLanguageLabel(origValue: "Registration Successful", key: "LBL_REGISTRATION_SUCCESS")

            break
        case 1:
            cell.stateTitleLbl.text = self.generalFunc.getLanguageLabel(origValue: "Upload your documents", key: "LBL_UPLOAD_YOUR_DOCS")
            cell.stateNoteLbl.text = self.generalFunc.getLanguageLabel(origValue: "We need to verify your driving documents to activate your account.", key: "LBL_UPLOAD_YOUR_DOCS_NOTE")
            cell.actionBtn.setButtonTitle(buttonTitle: self.generalFunc.getLanguageLabel(origValue: "Upload Document", key: "LBL_UPLOAD_DOC"))

            cell.actionBtn.isHidden = false
            
            cell.actionBtn.btnType = "UPLOAD_DOC"
            if(statusOfStates[indexPath.item] == true){
                cell.stateTitleLbl.text = self.generalFunc.getLanguageLabel(origValue: "Documents uploaded successfully.", key: "LBL_UPLOADDOC_SUCCESS")
            }
            break
        case 2:
            cell.stateTitleLbl.text = self.generalFunc.getLanguageLabel(origValue: "Add vehicles with document", key: "LBL_ADD_VEHICLE_AND_DOC")
            cell.stateNoteLbl.text = self.generalFunc.getLanguageLabel(origValue: "Please add your vehicles and its document. After that we will verify its registration.", key: "LBL_ADD_VEHICLE_AND_DOC_NOTE")
            cell.actionBtn.setButtonTitle(buttonTitle: self.generalFunc.getLanguageLabel(origValue: "Add Vehicle", key: "LBL_ADD_VEHICLE"))
            cell.actionBtn.isHidden = false
            cell.actionBtn.btnType = "ADD_VEHICLE"
            if(statusOfStates[indexPath.item] == true){
                cell.stateTitleLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_VEHICLE_ADD_SUCCESS")
            }
            break
        case 3:
            cell.stateTitleLbl.text = self.generalFunc.getLanguageLabel(origValue: "Waiting for admin's approval", key: "LBL_WAIT_ADMIN_APPROVE")
            cell.stateNoteLbl.text = self.generalFunc.getLanguageLabel(origValue: "We will check your provided information and get back to you soon.", key: "LBL_WAIT_ADMIN_APPROVE_NOTE")
            cell.actionBtn.isHidden = true
            cell.actionBtn.btnType = "CONTACT_US"
            break
        default:
            break
            
        }
        
        cell.actionBtn.clickDelegate = self
        
        cell.stateNoteLbl.fitText()
        
        
        cell.stateNumLbl.textColor = UIColor.UCAColor.AppThemeTxtColor
        
        
        GeneralFunctions.setImgTintColor(imgView: cell.stateFinishedImgView, color: UIColor.UCAColor.AppThemeColor)
        
        cell.topSeperatorView.backgroundColor = UIColor.UCAColor.AppThemeColor
        
        cell.stateSeperatorView.backgroundColor = UIColor.UCAColor.AppThemeColor
        
//        if(self.selectedRow == indexPath.item){
//            cell.stateNoteLbl.isHidden = false
//        }else{
//            cell.stateNoteLbl.isHidden = true
//        }
        
        if((indexPath.item + 1) == self.numOfItems){
            cell.stateSeperatorView.isHidden = true
        }else{
            cell.stateSeperatorView.isHidden = false
        }
        
        if(statusOfStates[indexPath.item] == false){
            cell.stateNoteLbl.isHidden = false
            cell.stateFinishedImgView.isHidden = true
            cell.stateNumLbl.isHidden = false
            cell.stateNumLbl.backgroundColor = UIColor.UCAColor.AppThemeColor
        }else{
            cell.stateNoteLbl.isHidden = true
//            cell.stateNumLbl.isHidden = true
            cell.stateFinishedImgView.isHidden = false
            cell.stateNumLbl.backgroundColor = UIColor.UCAColor.AppThemeTxtColor
        }
        
        
        cell.stateNumLbl.text = "\(indexPath.item + 1)"
        
        Utils.createRoundedView(view: cell.stateFinishedImgView, borderColor: UIColor.clear, borderWidth: 0)
        Utils.createRoundedView(view: cell.stateNumLbl, borderColor: UIColor.clear, borderWidth: 0)
        
        cell.selectionStyle = .none
        cell.backgroundColor = UIColor.clear
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if(statusOfStates[indexPath.item] == false){
            self.selectedRow = indexPath.item
            self.tableView.reloadData()
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        || indexPath.item != self.selectedRow
        if(indexPath.item == 0 || (statusOfStates.count > 0 && statusOfStates[indexPath.item] == true)){
            return 55
        }
        
        var noteTextHeight:CGFloat = 29
        
        switch indexPath.item {
        case 1:
            noteTextHeight = self.generalFunc.getLanguageLabel(origValue: "We need to verify your driving documents to activate your account.", key: "LBL_UPLOAD_YOUR_DOCS_NOTE").height(withConstrainedWidth: Application.screenSize.width - 80, font: UIFont (name: "Roboto-Light", size: 15)!)
            break
        case 2:
            noteTextHeight = self.generalFunc.getLanguageLabel(origValue: "Please add your vehicles and its document. After that we will verify its registration.", key: "LBL_ADD_VEHICLE_AND_DOC_NOTE").height(withConstrainedWidth: Application.screenSize.width - 80, font: UIFont (name: "Roboto-Light", size: 15)!)
            break
        case 3:
            noteTextHeight = self.generalFunc.getLanguageLabel(origValue: "We will check your provided information and get back to you soon.", key: "LBL_WAIT_ADMIN_APPROVE_NOTE").height(withConstrainedWidth: Application.screenSize.width - 80, font: UIFont (name: "Roboto-Light", size: 15)!)
            
            noteTextHeight = noteTextHeight - 60 - 10
            break
        default:
            break
            
        }
        
        return 165 + noteTextHeight - 20
    }
    
    func myBtnTapped(sender: MyButton) {
        if(sender.btnType == "UPLOAD_DOC"){
            let listOfDocumentUV = GeneralFunctions.instantiateViewController(pageName: "ListOfDocumentUV") as! ListOfDocumentUV
            self.pushToNavController(uv: listOfDocumentUV)
        }else if(sender.btnType == "ADD_VEHICLE"){
            let manageVehiclesUv = GeneralFunctions.instantiateViewController(pageName: "ManageVehiclesUV") as! ManageVehiclesUV
            self.pushToNavController(uv: manageVehiclesUv)
        }
    }
}
