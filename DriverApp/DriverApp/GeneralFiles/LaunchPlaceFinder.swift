//
//  LaunchPlaceFinder.swift
//  DriverApp
//
//  Created by NEW MAC on 29/05/17.
//  Copyright © 2017 V3Cube. All rights reserved.
//

import UIKit
import GooglePlaces

class LaunchPlaceFinder: NSObject, GMSAutocompleteViewControllerDelegate {
    typealias CompletionHandler = (_ address:String, _ latitude:Double, _ longitude:Double) -> Void
    
    var autocompleteController:GMSAutocompleteViewController?
    
    let generalFunc = GeneralFunctions()
    
    var sourceLocationPlaceLatitude = 0.0
    var sourceLocationPlaceLongitude = 0.0
    
    var viewControllerUV:UIViewController?
    
    var completeBlock:CompletionHandler?
    
    var currInst:LaunchPlaceFinder!
    var currentTransition:JTMaterialTransition!
    
    init(viewControllerUV:UIViewController) {
        self.viewControllerUV = viewControllerUV
        super.init()
    }
    
    func setBiasLocation(sourceLocationPlaceLatitude:Double, sourceLocationPlaceLongitude:Double){
        self.sourceLocationPlaceLatitude = sourceLocationPlaceLatitude
        self.sourceLocationPlaceLongitude = sourceLocationPlaceLongitude
    }
    
    func initializeFinder(completionHandler: @escaping CompletionHandler){
        
        self.completeBlock = completionHandler
        
        autocompleteController = GMSAutocompleteViewController()
        autocompleteController!.delegate = currInst
        autocompleteController!.configureRTLView()
        
        if(sourceLocationPlaceLongitude != 0.0 && sourceLocationPlaceLatitude != 0.0){
            
            
            let neBoundsCorner = CLLocationCoordinate2D(latitude: sourceLocationPlaceLatitude,
                                                        longitude: sourceLocationPlaceLongitude)
            let swBoundsCorner = CLLocationCoordinate2D(latitude: sourceLocationPlaceLatitude,
                                                        longitude: sourceLocationPlaceLongitude)
            let bounds = GMSCoordinateBounds(coordinate: neBoundsCorner,
                                             coordinate: swBoundsCorner)
            
            autocompleteController!.autocompleteBounds = bounds
        }
        
        let searchBarTextAttributes = [NSForegroundColorAttributeName: UIColor.UCAColor.AppThemeTxtColor]
        //        let attributedPlaceholder = NSAttributedString(string: self.generalFunc.getLanguageLabel("", key: "LBL_Search"), attributes: searchBarTextAttributes)
        
        let attributedPlaceholder = NSAttributedString(string: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_Search"))
        
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).defaultTextAttributes = searchBarTextAttributes
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).attributedPlaceholder = attributedPlaceholder
        
        //
        //        let transition: CATransition = CATransition()
        //        transition.duration = 0.4
        //        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        //        transition.type = kCATransitionFromTop
        //        Application.window!.layer.add(transition, forKey: kCATransition)
        
        if(self.currentTransition != nil){
            autocompleteController!.modalPresentationStyle = .custom
            autocompleteController!.transitioningDelegate = currentTransition
        }
        
        //        self.viewControllerUV!.present(autocompleteController!, animated: true)
//        self.viewControllerUV!.present(autocompleteController!, animated: true, completion: nil)
        self.viewControllerUV!.pushToNavController(uv: autocompleteController!)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        if(place.coordinate.latitude == 0.0 || place.coordinate.latitude == -180.0 || place.coordinate.longitude == 0.0 || place.coordinate.longitude == -180.0){
            self.generalFunc.setError(uv: self.viewControllerUV!, title: "", content: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_NO_LOCATION_FOUND_TXT"))
            return
        }
        
        //        print("Place name: \(place.name)")
        //        print("Place address: \(place.formattedAddress)")
        //        print("Place attributions: \(place.attributions)")
        //        print("Place Corr: Latitude:\(place.coordinate.latitude) :Longitude:\(place.coordinate.longitude)")
        
            
            if(self.completeBlock != nil){
                self.completeBlock!(place.formattedAddress!, place.coordinate.latitude, place.coordinate.longitude)
            }
        autocompleteController!.closeCurrentScreen()
//        autocompleteController!.dismiss(animated: true, completion: {
//            
//            if(self.completeBlock != nil){
//                self.completeBlock!(place.formattedAddress!, place.coordinate.latitude, place.coordinate.longitude)
//            }
//        })
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("Error: ", error.localizedDescription)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
//        autocompleteController!.dismiss(animated: true, completion: nil)
        autocompleteController!.closeCurrentScreen()
    }
    
    
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}
