//
//  UpdateDirections.swift
//  DriverApp
//
//  Created by NEW MAC on 27/05/17.
//  Copyright © 2017 V3Cube. All rights reserved.
//

import UIKit
import GoogleMaps

@objc protocol OnDirectionUpdateDelegate:class
{
    func onDirectionUpdate(directionResultDict:NSDictionary)
}

class UpdateDirections: NSObject, OnLocationUpdateDelegate {
    
    var userLocation:CLLocation!
    var timer:Timer!
    var uv:UIViewController!
    
    var getLoc:GetLocation!
    var gMap:GMSMapView!
    var navigateView:UIView!
    
    var destinationLocation:CLLocation!
    var fromLocation:CLLocation!
    
    var listOfPoints = [String()]
    
    var listOfPaths = [GMSPolyline]()
    
    let generalFunc = GeneralFunctions()
    
    var eTollSkipped = ""
    
    var isCurrentLocationEnabled = true
    
    var onDirectionUpdateDelegate:OnDirectionUpdateDelegate!
    var userProfileJson:NSDictionary!

    init(uv:UIViewController, gMap:GMSMapView, destinationLocation:CLLocation, navigateView:UIView){
        self.uv = uv
        self.gMap = gMap
        self.destinationLocation = destinationLocation
        self.navigateView = navigateView
        
        super.init()
    }
    
    init(uv:UIViewController, gMap:GMSMapView, fromLocation:CLLocation, destinationLocation:CLLocation, isCurrentLocationEnabled:Bool){
        self.uv = uv
        self.gMap = gMap
        self.fromLocation = fromLocation
        self.destinationLocation = destinationLocation
        self.isCurrentLocationEnabled = isCurrentLocationEnabled
        
        super.init()
    }
    
    func setCurrentLocEnabled(isCurrentLocationEnabled:Bool){
        self.isCurrentLocationEnabled = isCurrentLocationEnabled
    }
    
    func scheduleDirectionUpdate(eTollSkipped:String){
        self.eTollSkipped = eTollSkipped
        
        userProfileJson = (GeneralFunctions.getValue(key: Utils.USER_PROFILE_DICT_KEY) as! String).getJsonDataDict().getObj(Utils.message_str)
        
        if(navigateView != nil){
            (navigateView.subviews[0].subviews[0] as! MyLabel).text = generalFunc.getLanguageLabel(origValue: "", key: "LBL_LOAD_ADDRESS")
        }
        
        
        if(timer != nil){
            timer!.invalidate()
        }
        let DESTINATION_UPDATE_TIME_INTERVAL = GeneralFunctions.getValue(key: "DESTINATION_UPDATE_TIME_INTERVAL")
        
        
        let DESTINATION_UPDATE_TIME_INTERVAL_value = GeneralFunctions.parseDouble(origValue: 30, data: DESTINATION_UPDATE_TIME_INTERVAL == nil ? "30" : (DESTINATION_UPDATE_TIME_INTERVAL as! String)) * 60
        
        
        Utils.printLog(msgData: "DESTINATION_UPDATE_TIME_INTERVAL:\(DESTINATION_UPDATE_TIME_INTERVAL_value)")
        
        timer =  Timer.scheduledTimer(timeInterval: DESTINATION_UPDATE_TIME_INTERVAL_value, target: self, selector: #selector(updateDirections), userInfo: nil, repeats: true)
        
        timer.fire()
        
        
        if(isCurrentLocationEnabled){
            if(getLoc == nil){
                getLoc = GetLocation(uv: self.uv, isContinuous: true)
                getLoc.buildLocManager(locationUpdateDelegate: self)
            }else{
                getLoc.buildLocManager(locationUpdateDelegate: self)
                getLoc.resumeLocationUpdates()
            }
        }
        
//        if(getLoc == nil){
//            getLoc = GetLocation(uv: self.uv, isContinuous: true)
//            getLoc.buildLocManager(locationUpdateDelegate: self)
//        }else{
//            getLoc.buildLocManager(locationUpdateDelegate: self)
//            getLoc.resumeLocationUpdates()
//        }
        
        
        
    }
    
    func stopFrequentUpdate(){
        if(timer != nil){
            timer!.invalidate()
        }
        
        if(getLoc != nil){
            getLoc.releaseLocationTask()
            getLoc.locationUpdateDelegate = nil
        }
    }
    
    func releaseTask(){
        if(timer != nil){
            timer!.invalidate()
            timer = nil
        }
        
        if(getLoc != nil){
            getLoc.releaseLocationTask()
            getLoc.locationUpdateDelegate = nil
            getLoc = nil
        }
    }
    
    func onLocationUpdate(location: CLLocation) {
        //        if(self.userLocation != nil){
        //            let distance = location.distance(from: self.userLocation)
        //
        //            if(distance > 80){
        //                self.userLocation = location
        //                updateDriverStatus()
        //            }
        //        }
        if(self.userLocation == nil){
            self.userLocation = location
            updateDirections()
        }
        
        self.userLocation = location
    }
    
    func updateDirections(){
        let fromLocation = userLocation == nil ? self.fromLocation : userLocation
        let destinationLocation = self.destinationLocation
        
//        if(gMap == nil || destinationLocation == nil || userLocation == nil || destinationLocation!.coordinate.latitude == 0.0 || destinationLocation!.coordinate.longitude == 0.0){
//            return
//        }

        if(gMap == nil || destinationLocation == nil || fromLocation == nil || destinationLocation!.coordinate.latitude == 0.0 || destinationLocation!.coordinate.longitude == 0.0){
            return
        }
        
//        var directionURL = "https://maps.googleapis.com/maps/api/directions/json?origin=\(self.userLocation!.coordinate.latitude),\(self.userLocation!.coordinate.longitude)&destination=\(destinationLocation!.coordinate.latitude),\(destinationLocation!.coordinate.longitude)&key=\(CommonUtils.google_server_key)&language=\(Configurations.getGoogleMapLngCode())&sensor=true"
        
         var directionURL = "https://maps.googleapis.com/maps/api/directions/json?origin=\(fromLocation!.coordinate.latitude),\(fromLocation!.coordinate.longitude)&destination=\(destinationLocation!.coordinate.latitude),\(destinationLocation!.coordinate.longitude)&key=\(Configurations.getInfoPlistValue(key: "GOOGLE_SERVER_KEY"))&language=\(Configurations.getGoogleMapLngCode())&sensor=true"
//        Utils.printLog(msgData: "directionURL:\(directionURL)")
        
        Utils.printLog(msgData: "EUnit:\(userProfileJson.get("eUnit"))")
        
        if(userProfileJson != nil && userProfileJson.get("eUnit") != "KMs"){
            directionURL = directionURL + "&units=imperial"
        }

        if(eTollSkipped == "Yes"){
            directionURL = directionURL + "&avoid=tolls"
        }
        
        let exeWebServerUrl = ExeServerUrl(dict_data: [String:String](), currentView: self.uv.view, isOpenLoader: false)
        
        exeWebServerUrl.executeGetProcess(completionHandler: { (response) -> Void in
            
            if(response != ""){
                let dataDict = response.getJsonDataDict()
                
                if(dataDict.get("status").uppercased() != "OK" || dataDict.getArrObj("routes").count == 0){
                    return
                }
                
                if(self.onDirectionUpdateDelegate != nil){
                    self.onDirectionUpdateDelegate!.onDirectionUpdate(directionResultDict: dataDict)
                }
                
                let routesArr = dataDict.getArrObj("routes")
                let legs_arr = (routesArr.object(at: 0) as! NSDictionary).getArrObj("legs")
                let steps_arr = (legs_arr.object(at: 0) as! NSDictionary).getArrObj("steps")
//                let start_address = (legs_arr.object(at: 0) as! NSDictionary).get("start_address")
                let end_address = (legs_arr.object(at: 0) as! NSDictionary).get("end_address")
                let distance_value = (legs_arr.object(at: 0) as! NSDictionary).getObj("distance").get("value")
                let time_str = (legs_arr.object(at: 0) as! NSDictionary).getObj("duration").get("text")
                
                var distance_final = GeneralFunctions.parseDouble(origValue: 0.0, data: distance_value)
                
                if(self.userProfileJson != nil && self.userProfileJson.get("eUnit") != "KMs"){
                    distance_final = distance_final * 0.000621371
                }else{
                    distance_final = distance_final * 0.00099999969062399994
                }
                
                distance_final = distance_final.roundTo(places: 2)
                
                var distance_str = ""
                
                if(self.userProfileJson != nil && self.userProfileJson.get("eUnit") != "KMs"){
                    distance_str = "\(distance_final) \(self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_MILE_DISTANCE_TXT"))"
                }else{
                    distance_str = "\(distance_final) \(self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_KM_DISTANCE_TXT"))"
                }
                
                if(self.navigateView != nil){
                    (self.navigateView.subviews[0].subviews[0] as! MyLabel).text = end_address
                    
                    if(self.navigateView.subviews[1].isHidden == true){
                        self.navigateView.subviews[1].isHidden = false
//                        self.navigateView.frame.size = CGSize(width: self.navigateView.frame.width, height: 94 + 25)
                        
                    }
                    
                    (self.navigateView.subviews[1] as! MyLabel).text = "\(time_str) \(self.generalFunc.getLanguageLabel(origValue: "to reach", key: "LBL_TO_REACH")) & \(distance_str) \(self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_AWAY"))"
                    
                    (self.navigateView.subviews[1] as! MyLabel).backgroundColor = UIColor.UCAColor.AppThemeColor
                    
                    var extraHeight = (self.navigateView.subviews[1] as! MyLabel).text!.height(withConstrainedWidth: self.navigateView.frame.width - 20, font: (self.navigateView.subviews[1] as! MyLabel).font!) - 30
                    
                    if(extraHeight < 0){
                        extraHeight = 0
                    }
                    
                    if(self.uv .isKind(of: DriverArrivedUV.self)){
                        (self.uv as! DriverArrivedUV).navigateViewHeight.constant = CGFloat(94 + 30 + extraHeight)
                    }else if(self.uv .isKind(of: ActiveTripUV.self)){
                        (self.uv as! ActiveTripUV).navigateViewHeight.constant = CGFloat(94 + 30 + extraHeight)
                    }
                    
                }
                
                
                
//                print("legs_arr::\(legs_arr)")
//                print("end_address::\(end_address)")
                for i in 0..<self.listOfPaths.count{
                    self.listOfPaths[i].map = nil
                }
                self.listOfPaths.removeAll()
                self.listOfPoints.removeAll()
                
                for i in 0..<steps_arr.count{
                    let polyPoints = (steps_arr.object(at: i) as! NSDictionary).getObj("polyline").get("points")
                    
                    self.listOfPoints += [polyPoints]
                    
                    self.addPolyLineWithEncodedStringInMap(encodedString: polyPoints)
                }
            }else{
                //                self.generalFunc.setError(uv: self)
            }
        }, url: directionURL)
    }
    
    func addPolyLineWithEncodedStringInMap(encodedString: String) {
        
        let path = GMSMutablePath(fromEncodedPath: encodedString)
        let polyLine = GMSPolyline(path: path)
        polyLine.strokeWidth = 5
        polyLine.strokeColor = UIColor.UCAColor.AppThemeColor_1
        polyLine.map = gMap
        
        self.listOfPaths += [polyLine]
    }
}
