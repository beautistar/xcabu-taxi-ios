//
//  AppLoginUV.swift
//  PassengerApp
//
//  Created by NEW MAC on 06/05/17.
//  Copyright © 2017 V3Cube. All rights reserved.
//

import UIKit
import Crashlytics

class AppLoginUV: UIViewController, MyBtnClickDelegate {

    @IBOutlet weak var registerBtn: MyButton!
    @IBOutlet weak var signInBtn: MyButton!
    @IBOutlet weak var bgImgView: UIImageView!
    
    @IBOutlet weak var languageSelectView: UIView!
    @IBOutlet weak var currencySelectView: UIView!
    @IBOutlet weak var lngLbl: MyLabel!
    @IBOutlet weak var currencyLbl: MyLabel!
    @IBOutlet weak var selectStackViewHeight: NSLayoutConstraint!
    @IBOutlet weak var selectStackViewBottomMargin: NSLayoutConstraint!

    @IBOutlet weak var currencyTxtField: MyTextField!
    @IBOutlet weak var languageTxtField: MyTextField!
    
//    var languageTxtField = MyTextField()
//    var currencyTxtField = MyTextField()
    
//    @IBOutlet weak var introLbl: MyLabel!
    @IBOutlet weak var introSubLbl: MyLabel!
    
    var selectedCurrency = ""
    var selectedLngCode = ""
    
    var languageNameList = [String]()
    var languageCodes = [String]()
    
    var languagePicker: DownPicker!
    
    var currenyList = [String]()
    
    var currencyPicker: DownPicker!

    let generalFunc = GeneralFunctions()
    
    var languageArrCount = 0
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.configureRTLView()
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.addSubview(self.generalFunc.loadView(nibName: "AppLoginScreenDesign", uv: self))
        
        self.bgImgView.image = UIImage(named: "app_login")
        
        switch UIDevice().type {
        case .iPhone4:
            self.bgImgView.image = UIImage(named: "app_login@640x960")
        case .iPhone4S:
            self.bgImgView.image = UIImage(named: "app_login@640x960")
        case .iPhone5:
            self.bgImgView.image = UIImage(named: "app_login@640x1136")
        case .iPhone5S:
            self.bgImgView.image = UIImage(named: "app_login@640x1136")
        case .iPhone6:
            self.bgImgView.image = UIImage(named: "app_login@750x1334")
        case .iPhone6plus:
            self.bgImgView.image = UIImage(named: "app_login@1242x2208")
        case .iPhone6S:
            self.bgImgView.image = UIImage(named: "app_login@750x1334")
        case .iPhone6Splus:
            self.bgImgView.image = UIImage(named: "app_login@1242x2208")
        case .iPhone7:
            self.bgImgView.image = UIImage(named: "app_login@750x1334")
        case .iPhone7plus:
            self.bgImgView.image = UIImage(named: "app_login@1242x2208")
        case .iPhoneSE:
            self.bgImgView.image = UIImage(named: "app_login@640x1136")
        default:
            print("default")
        }
        
        
        self.bgImgView.backgroundColor = self.bgImgView.image!.getPixelColor(pos: CGPoint(x:20, y:10))
        
//        for family: String in UIFont.familyNames
//        {
//            print("\(family)")
//            for names: String in UIFont.fontNames(forFamilyName: family)
//            {
//                print("== \(names)")
//            }
//        }
        
        registerBtn.clickDelegate = self
        signInBtn.clickDelegate = self
        
        setLabels()
        
        self.lngLbl.text = GeneralFunctions.getValue(key: Utils.DEFAULT_LANGUAGE_TITLE_KEY) as? String
        self.currencyLbl.text = GeneralFunctions.getValue(key: Utils.DEFAULT_CURRENCY_TITLE_KEY) as? String
        
//        let lngTapGue = UITapGestureRecognizer()
//        let currencyTapGue = UITapGestureRecognizer()
//        
//        lngTapGue.addTarget(self, action: #selector(self.lngSelectViewTapped))
//        currencyTapGue.addTarget(self, action: #selector(self.currencySelectViewTapped))
//        
//        self.languageSelectView.isUserInteractionEnabled = true
//        self.currencySelectView.isUserInteractionEnabled = true
//        
//        self.languageSelectView.addGestureRecognizer(lngTapGue)
//        self.currencySelectView.addGestureRecognizer(currencyTapGue)
        
        setLanguage()
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        currencyTxtField.disableMenu()
        currencyTxtField.getTextField()!.dividerNormalColor = UIColor.UCAColor.AppThemeTxtColor
        currencyTxtField.getTextField()!.dividerActiveColor = UIColor.UCAColor.AppThemeTxtColor
        currencyTxtField.getTextField()!.placeholderNormalColor = UIColor.UCAColor.AppThemeTxtColor
        currencyTxtField.getTextField()!.placeholderActiveColor = UIColor.UCAColor.AppThemeTxtColor
        currencyTxtField.getTextField()!.textColor = UIColor.UCAColor.AppThemeTxtColor
        
        languageTxtField.disableMenu()
        languageTxtField.getTextField()!.dividerNormalColor = UIColor.UCAColor.AppThemeTxtColor
        languageTxtField.getTextField()!.dividerActiveColor = UIColor.UCAColor.AppThemeTxtColor
        languageTxtField.getTextField()!.placeholderNormalColor = UIColor.UCAColor.AppThemeTxtColor
        languageTxtField.getTextField()!.placeholderActiveColor = UIColor.UCAColor.AppThemeTxtColor
        languageTxtField.getTextField()!.textColor = UIColor.UCAColor.AppThemeTxtColor
        
        
    }

    func setLabels(){
//        introLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_INTRODUCTION")
        introSubLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_HOME_PASSENGER_INTRO_DETAILS")
        introSubLbl.fitText()
        
        self.signInBtn.setButtonTitle(buttonTitle: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_SIGN_IN_TXT"))
        self.registerBtn.setButtonTitle(buttonTitle: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_SIGN_UP"))
        
        
        self.languageTxtField.setPlaceHolder(placeHolder: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_LANGUAGE_TXT"))
        self.currencyTxtField.setPlaceHolder(placeHolder: self.generalFunc.getLanguageLabel(origValue: "Currency", key: "LBL_CURRENCY_TXT"))
    }
    
   
    func setLanguage(){
        let dataArr = GeneralFunctions.getValue(key: Utils.LANGUAGE_LIST_KEY) as! NSArray
        self.languageArrCount = dataArr.count
        
        for i in 0 ..< dataArr.count{
            let tempItem = dataArr[i] as! NSDictionary
            
            if((GeneralFunctions.getValue(key: Utils.LANGUAGE_CODE_KEY) as! String) == tempItem.get("vCode")){
                languageTxtField.setText(text: tempItem.get("vTitle"))
                self.selectedLngCode = tempItem.get("vCode")
            }
            
            languageNameList += [tempItem.get("vTitle")]
            languageCodes += [tempItem.get("vCode")]
            
        }
        
        DispatchQueue.main.async {
            self.languageTxtField.getTextField()!.clearButtonMode = .never
            self.languagePicker = DownPicker(textField: self.languageTxtField.getTextField()!, withData: self.languageNameList as [AnyObject])
            self.languagePicker.addTarget(self, action: #selector(self.lngValueChanged), for: .valueChanged)
        }
        
        if(dataArr.count < 2){
            languageSelectView.isHidden = true
        }
        
        setCurrency()
    }
    
    func lngValueChanged(){
        if(self.languagePicker.selectedIndex != -1){
            self.selectedLngCode = self.languageCodes[self.languagePicker.selectedIndex]
            
            if((GeneralFunctions.getValue(key: Utils.LANGUAGE_CODE_KEY) as! String) != self.selectedLngCode){
            changeLanguage()
            }
            else{
                self.languagePicker.selectedIndex = self.languageCodes.index(of: GeneralFunctions.getValue(key: Utils.LANGUAGE_CODE_KEY) as! String)!
            }
        }
        else
        {
            self.languagePicker.selectedIndex = self.languageCodes.index(of: GeneralFunctions.getValue(key: Utils.LANGUAGE_CODE_KEY) as! String)!
        }
    }
    
    func changeLanguage(){
        let parameters = ["type":"changelanguagelabel","vLang": self.selectedLngCode]
        
        let exeWebServerUrl = ExeServerUrl(dict_data: parameters, currentView: self.view, isOpenLoader: true)
        exeWebServerUrl.setDeviceTokenGenerate(isDeviceTokenGenerate: true)
        exeWebServerUrl.currInstance = exeWebServerUrl
        exeWebServerUrl.executePostProcess(completionHandler: { (response) -> Void in
            
            if(response != ""){
                let dataDict = response.getJsonDataDict()
                
                if(dataDict.get("Action") == "1"){
                    let window = UIApplication.shared.delegate!.window!
                    
                    GeneralFunctions.saveValue(key: Utils.languageLabelsKey, value: dataDict.getObj(Utils.message_str))
                    
                    GeneralFunctions.saveValue(key: Utils.LANGUAGE_CODE_KEY, value: dataDict.get("vCode") as AnyObject)
                    GeneralFunctions.saveValue(key: Utils.LANGUAGE_IS_RTL_KEY, value: dataDict.get("eType") as AnyObject)
                    GeneralFunctions.saveValue(key: Utils.DEFAULT_LANGUAGE_TITLE_KEY, value: dataDict.get("vTitle") as AnyObject)
                    GeneralFunctions.saveValue(key: Utils.GOOGLE_MAP_LANGUAGE_CODE_KEY, value: dataDict.get("vGMapLangCode") as AnyObject)
                    Configurations.setAppLocal()
                    GeneralFunctions.restartApp(window: window!)
                    
                }else{
                    self.languagePicker.selectedIndex = self.languageCodes.index(of: GeneralFunctions.getValue(key: Utils.LANGUAGE_CODE_KEY) as! String)!
                    self.generalFunc.setError(uv: self, title: "", content: self.generalFunc.getLanguageLabel(origValue: "", key: dataDict.get("message")))
                }
                
            }else{
                self.languagePicker.selectedIndex = self.languageCodes.index(of: GeneralFunctions.getValue(key: Utils.LANGUAGE_CODE_KEY) as! String)!
                self.generalFunc.setError(uv: self)
            }
        })
    }
    
    func setCurrency(){
        let dataArr = GeneralFunctions.getValue(key: Utils.CURRENCY_LIST_KEY) as! NSArray
        
        for i in 0 ..< dataArr.count{
            let tempItem = dataArr[i] as! NSDictionary
            
            if((GeneralFunctions.getValue(key: Utils.DEFAULT_CURRENCY_TITLE_KEY) as! String) == tempItem.get("vName")){
                self.currencyTxtField.setText(text: tempItem.get("vName"))
                self.selectedCurrency = tempItem.get("vName")
            }
            
            currenyList += [tempItem.get("vName")]
            
        }
        
        DispatchQueue.main.async {
            self.currencyTxtField.getTextField()!.clearButtonMode = .never
            self.currencyPicker = DownPicker(textField: self.currencyTxtField.getTextField()!, withData: self.currenyList as [AnyObject])
            self.currencyPicker.addTarget(self, action: #selector(self.currencyValueChanged), for: .valueChanged)
        }
        
        if(dataArr.count < 2){
            currencySelectView.isHidden = true
            
            if(languageArrCount < 2){
                selectStackViewHeight.constant = 0
                selectStackViewBottomMargin.constant = 0
            }
        }
    }
    
    func currencyValueChanged(){
        if(self.currencyPicker.selectedIndex != -1){
            self.selectedCurrency = self.currenyList[self.currencyPicker.selectedIndex]
            self.currencyLbl.text = self.selectedCurrency
            
            GeneralFunctions.saveValue(key: Utils.DEFAULT_CURRENCY_TITLE_KEY, value: self.selectedCurrency as AnyObject)
        }
    }

    func myBtnTapped(sender: MyButton) {

        if(sender == signInBtn){
            self.pushToNavController(uv: GeneralFunctions.instantiateViewController(pageName: "SignInUV"))
        }else if(sender == registerBtn){
            self.pushToNavController(uv: GeneralFunctions.instantiateViewController(pageName: "SignUpUV"))
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
