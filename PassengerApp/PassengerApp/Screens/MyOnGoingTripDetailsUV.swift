//
//  MyOnGoingTripDetailsUV.swift
//  PassengerApp
//
//  Created by NEW MAC on 18/07/17.
//  Copyright © 2017 V3Cube. All rights reserved.
//

import UIKit
import GoogleMaps

class MyOnGoingTripDetailsUV: UIViewController, UITableViewDelegate, UITableViewDataSource, OnTaskRunCalledDelegate {

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var detailBottomVIew: UIView!
    @IBOutlet weak var providerImgView: UIImageView!
    @IBOutlet weak var sourceAddLbl: MyLabel!
    @IBOutlet weak var providerNameLbl: MyLabel!
    @IBOutlet weak var bottomPointViewHeight: NSLayoutConstraint!
    @IBOutlet weak var providerDetailView: UIView!
    @IBOutlet weak var ratingView: RatingView!
    @IBOutlet weak var statusTitleLbl: MyLabel!
    @IBOutlet weak var gMapContainerView: UIView!
    
    @IBOutlet weak var tableView: UITableView!
    
    var gMapView:GMSMapView!
    
    var dataDict:NSDictionary!
    
    let generalFunc = GeneralFunctions()
    
    var loaderView:UIView!
    
    var dataArrList = [NSDictionary]()
    
    var cntView:UIView!
    var menu:BTNavigationDropdownMenu!
    
    var configPubNub:ConfigPubNub?
    
    var isDriverArrived = false
    var isTripStarted = false
    var isTripFinished = false
    
    var assignedDriverMarker:GMSMarker!
    var assignedDriverRotatedLocation:CLLocation!
    var assignedDriverLocation:CLLocation!
    
    var updateFreqDriverLocTask:UpdateFreqTask!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.addBackBarBtn()
        
        cntView = self.generalFunc.loadView(nibName: "MyOnGoingTripDetailsScreenDesign", uv: self, contentView: contentView)
        
        self.contentView.addSubview(cntView)
        
        setData()
        
        self.tableView.delegate = self
        
        self.tableView.dataSource = self
        self.tableView.tableFooterView = UIView()
        self.tableView.register(UINib(nibName: "MyOnGoingTripDetailsTVCell", bundle: nil), forCellReuseIdentifier: "MyOnGoingTripDetailsTVCell")
        self.tableView.contentInset = UIEdgeInsets(top: 8, left: 0, bottom: 8, right: 0)
        
        getData()
        
        
        print("Data:")
        print(dataDict)
        
        self.navigationItem.rightBarButtonItem =  UIBarButtonItem(image: UIImage(named: "ic_menu")!, style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.openPopUpMenu))
        
        
        let userProfileJson = (GeneralFunctions.getValue(key: Utils.USER_PROFILE_DICT_KEY) as! String).getJsonDataDict().getObj(Utils.message_str)
        
        if(self.getPubNubConfig().uppercased() == "YES"){
            configPubNub = ConfigPubNub()
            configPubNub!.iDriverId = dataDict.get("iDriverId")
            configPubNub!.myOnGoingTripDetailsUv = self
            configPubNub!.iTripId = self.dataDict.get("iTripId")
            configPubNub!.buildPubNub()
        }
        
        if(dataDict.get("driverStatus") == "Arrived"){
            self.isDriverArrived = true
        }
        
        if(dataDict.get("driverStatus") == "On Going Trip"){
            self.isDriverArrived = true
            self.isTripStarted = true
        }
        
        if(self.configPubNub == nil){
            
            let DRIVER_LOC_FETCH_TIME_INTERVAL = GeneralFunctions.parseFloat(origValue: 5, data: userProfileJson.get("DRIVER_LOC_FETCH_TIME_INTERVAL"))
            updateFreqDriverLocTask = UpdateFreqTask(interval: CGFloat(DRIVER_LOC_FETCH_TIME_INTERVAL))
            updateFreqDriverLocTask.currInst = updateFreqDriverLocTask
            updateFreqDriverLocTask.setTaskRunListener(onTaskRunCalled: self)
        }
        
        if((isDriverArrived == false || dataDict.get("eFareType") == "Regular") && configPubNub != nil){
            subscribeToDriverLocChannel()
        }
        
        self.addDriverNotificationObserver()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let bottomPointImg = UIImage(named: "ic_bottom_anchor_point", in: Bundle(for: MyOnGoingTripDetailsUV.self), compatibleWith: self.traitCollection)
        
        let iv = UIImageView(image: bottomPointImg)
        
        detailBottomVIew.backgroundColor = UIColor(patternImage: UIImage(named: "ic_bottom_anchor_point")!)
        bottomPointViewHeight.constant = iv.frame.height
    }
    
    override func closeCurrentScreen() {
        releaseAllTask()
        if(self.menu != nil){
            self.menu.hideMenu()
        }
        super.closeCurrentScreen()
    }
    
    deinit {
        print("MyOnGoingTripDetails Called")
        releaseAllTask()
    }
    
    func subscribeToDriverLocChannel(){
        var channels =  [String]()
        channels += [Utils.PUBNUB_UPDATE_LOC_CHANNEL_PREFIX_DRIVER+self.dataDict.get("iDriverId")]
        if(configPubNub != nil){
            self.configPubNub?.subscribeToChannels(channels: channels)
        }
    }
    
    func unSubscribeToDriverLocChannel(){
        var channels =  [String]()
        channels += [Utils.PUBNUB_UPDATE_LOC_CHANNEL_PREFIX_DRIVER+self.dataDict.get("iDriverId")]
        if(configPubNub != nil){
            self.configPubNub?.subscribeToChannels(channels: channels)
        }
    }
    
    func setData(){
        self.navigationItem.title = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_BOOKING") + "# " + Configurations.convertNumToAppLocal(numStr: dataDict.get("vRideNo"))
        self.title = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_BOOKING") + "# " + Configurations.convertNumToAppLocal(numStr: dataDict.get("vRideNo"))
        
        self.statusTitleLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_PROGRESS_HINT")
        self.statusTitleLbl.textColor = UIColor.UCAColor.AppThemeColor
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addDriverNotificationObserver(){
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.driverCallBackReceived(sender:)), name: NSNotification.Name(rawValue: Utils.driverCallBackNotificationKey), object: nil)
    }
    
    
    
    
    func driverCallBackReceived(sender: NSNotification){
        let userInfo = sender.userInfo
        let msgData = (userInfo!["body"] as! String).getJsonDataDict()
        
        let msgStr = msgData.get("Message")
        
        
        if(dataDict.get("iTripId") != msgData.get("iTripId")){
            return
        }
        Utils.resetAppNotifications()
        Utils.closeKeyboard(uv: self)
        
        if(msgStr == "TripStarted"){
            if(self.isTripStarted == true){
                return
            }
            self.isTripStarted = true
            
//            LocalNotification.dispatchlocalNotification(with: "", body: (GeneralFunctions()).getLanguageLabel(origValue: "", key: "LBL_START_TRIP_DIALOG_TXT"), at: Date().addedBy(seconds: 1))
            
            self.generalFunc.setAlertMessage(uv: self, title: "", content: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_START_TRIP_DIALOG_TXT"), positiveBtn: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_BTN_OK_TXT"), nagativeBtn: "", completionHandler: { (btnClickedIndex) in
                
                self.getData()
            })
            
        }else if(msgStr == "TripCancelledByDriver" || msgStr == "TripEnd"){
            
            if(self.isTripFinished == true){
                return
            }
            self.isTripFinished = true
            
//            LocalNotification.dispatchlocalNotification(with: "", body: (GeneralFunctions()).getLanguageLabel(origValue: "", key: msgStr == "TripCancelledByDriver" ? "LBL_CANCELED_TXT" : "LBL_FINISHED_TXT"), at: Date().addedBy(seconds: 1))
            
            self.generalFunc.setAlertMessage(uv: self, title: self.generalFunc.getLanguageLabel(origValue: "", key: msgStr == "TripCancelledByDriver" ? "LBL_CANCELED_TXT" : "LBL_FINISHED_TXT"), content: msgStr == "TripCancelledByDriver" ? (self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_PREFIX_TRIP_CANCEL_DRIVER") + " " + msgData.get("Reason") + " " + self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_CANCEL_TRIP_BY_DRIVER_MSG_SUFFIX")) : self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_END_TRIP_DIALOG_TXT"), positiveBtn: self.generalFunc.getLanguageLabel(origValue: "Ok", key: "LBL_BTN_OK_TXT"), nagativeBtn: "", completionHandler: { (btnClickedIndex) in
                
//                let window = Application.window
//                
//                let getUserData = GetUserData(uv: self, window: window!)
//                getUserData.getdata()
                
                self.releaseAllTask()
                self.performSegue(withIdentifier: "unwindToMyOnGoingTripsScreen", sender: self)
                
            })
        }
    }
    
    func initializeMenu(){
        
        var items = [self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_CALL_TXT"),self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_MESSAGE_TXT")]
        
        if(self.isDriverArrived == false || dataDict.get("eFareType") == "Regular"){
            items += [self.gMapView == nil ? self.generalFunc.getLanguageLabel(origValue: "Live Track", key: "LBL_LIVE_TRACK_TXT") : self.generalFunc.getLanguageLabel(origValue: "JOB PROGRESS", key: "LBL_PROGRESS_HINT")]
        }
        
        items += [self.generalFunc.getLanguageLabel(origValue: "SOS", key: "LBL_EMERGENCY_SOS_TXT")]
        
        if(isTripStarted == false){
            items += [self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_CANCEL_TRIP")]
        }
        
        
        if(self.menu == nil){
        
            menu = BTNavigationDropdownMenu(navigationController: self.navigationController, title: items.first!, items: items as [AnyObject])
            
            menu.cellHeight = 65
            menu.cellBackgroundColor = UIColor.UCAColor.AppThemeColor.lighter(by: 10)
            menu.cellSelectionColor = UIColor.UCAColor.AppThemeColor
            menu.cellTextLabelColor = UIColor.UCAColor.AppThemeTxtColor
            menu.cellTextLabelFont = UIFont(name: "Roboto-Light", size: 20)
            menu.cellSeparatorColor = UIColor.UCAColor.AppThemeColor
            
            if(Configurations.isRTLMode()){
                menu.cellTextLabelAlignment = NSTextAlignment.right
            }else{
                menu.cellTextLabelAlignment = NSTextAlignment.left
            }
            menu.arrowPadding = 15
            menu.animationDuration = 0.5
            menu.maskBackgroundColor = UIColor.black
            menu.maskBackgroundOpacity = 0.5
            menu.menuStateHandler = { (isMenuOpen: Bool) -> () in
                
                //                if(isMenuOpen){
                //                    self.rightButton.setBackgroundImage(nil, for: .normal, barMetrics: .default)
                //
                //                }else{
                //                    self.rightButton.setBackgroundImage(UIImage(color : UIColor.UCAColor.AppThemeColor.lighter(by: 10)!), for: .normal, barMetrics: .default)
                //                }
                
            }
            
            menu.didSelectItemAtIndexHandler = {(indexPath: Int) -> () in
                var indexPath = indexPath
                
                if(self.isDriverArrived == true && self.dataDict.get("eFareType") != "Regular" && indexPath > 1){
                    indexPath = indexPath + 1
                }
                
                if(indexPath == 0){
                    UIApplication.shared.openURL(NSURL(string:"telprompt:" + self.dataDict.get("driverMobile"))! as URL)
                }else if(indexPath == 1){
                    let chatUV = GeneralFunctions.instantiateViewController(pageName: "ChatUV") as! ChatUV
                    chatUV.receiverId = self.dataDict.get("iDriverId")
                    chatUV.receiverDisplayName = self.dataDict.get("driverName")
                    chatUV.assignedtripId = self.dataDict.get("iTripId")
                    chatUV.pPicName = self.dataDict.get("driverImage")
                    
                    self.pushToNavController(uv:chatUV, isDirect: true)
                }else if(indexPath == 2){
                    
                    if(self.gMapView != nil){
                        self.removeDriverTracking()
                    }else{
                        self.gMapContainerView.isHidden = false

                        let camera = GMSCameraPosition.camera(withLatitude: 0.0, longitude: 0.0, zoom: 0.0)
                        let gMapView = GMSMapView.map(withFrame: CGRect(x: 0, y: 0, width: self.gMapContainerView.frame.width, height: self.gMapContainerView.frame.height), camera: camera)
//                        self.gMapView.frame = self.gMapContainerView.frame
//                        gMapView.center = self.gMapContainerView.center
//                        gMapView.bounds = self.gMapContainerView.bounds
                        
                        self.gMapView = gMapView
                        
                        
                        //        googleMapContainerView = gMapView
                        //        gMapView = GMSMapView()
                        //            gMapView.isMyLocationEnabled = true
//                        gMapView.delegate = self
                        self.gMapContainerView.addSubview(gMapView)
                        self.gMapContainerView.backgroundColor = UIColor.black
                        self.statusTitleLbl.text = self.generalFunc.getLanguageLabel(origValue: "Live Track", key: "LBL_LIVE_TRACK_TXT")
                        
                        if(self.assignedDriverLocation == nil){
                            self.assignedDriverLocation = CLLocation(latitude: GeneralFunctions.parseDouble(origValue: 0.0, data: self.dataDict.get("driverLatitude")), longitude: GeneralFunctions.parseDouble(origValue: 0.0, data: self.dataDict.get("driverLongitude")))
                        }
                        
                        self.gMapView.moveCamera(GMSCameraUpdate.setCamera(GMSCameraPosition.camera(withLatitude: self.assignedDriverLocation.coordinate.latitude,
                                                                                                        longitude: self.assignedDriverLocation.coordinate.longitude, zoom: 15)))
                        
                         self.updateAssignedDriverMarker(driverLocation: self.assignedDriverLocation)
                    }
                    
                    
                }else if(indexPath == 3){
                    
                    let confirmEmergencyTapUV = GeneralFunctions.instantiateViewController(pageName: "ConfirmEmergencyTapUV") as! ConfirmEmergencyTapUV
                    confirmEmergencyTapUV.iTripId = self.dataDict.get("iTripId")
                    self.pushToNavController(uv: confirmEmergencyTapUV)
                    
                }else if(indexPath == 4){
                    
                    self.cancelBooking()
                    
                }
            }
            
        }else{
            menu.updateItems(items as [AnyObject])
        }
    
    }
    
    func removeDriverTracking(){
        self.gMapContainerView.isHidden = true
        
        if(self.gMapView != nil){
            self.gMapView.removeFromSuperview()
            self.gMapView = nil
        }
        
        self.statusTitleLbl.text = self.generalFunc.getLanguageLabel(origValue: "JOB PROGRESS", key: "LBL_PROGRESS_HINT")
    }
    
    func updateDriverLocation(iDriverId:String, latitude:String, longitude:String){
        
        if(self.gMapView != nil){
            updateAssignedDriverMarker(driverLocation: CLLocation(latitude: GeneralFunctions.parseDouble(origValue: 0.0, data: latitude), longitude: GeneralFunctions.parseDouble(origValue: 0.0, data: longitude)))
        }
    }
    
    func onTaskRun(currInst: UpdateFreqTask) {
        checkDriverLocation()
    }
    
    func checkDriverLocation(){
        
        if(self.gMapView == nil){
            return
        }
        
        let parameters = ["type":"getDriverLocations", "iUserId": GeneralFunctions.getMemberd(), "iDriverId": self.dataDict.get("iDriverId"), "UserType": Utils.appUserType]
        
        let exeWebServerUrl = ExeServerUrl(dict_data: parameters, currentView: self.view, isOpenLoader: true)
        exeWebServerUrl.executePostProcess(completionHandler: { (response) -> Void in
            
            if(response != ""){
                let dataDict = response.getJsonDataDict()
                
                if(dataDict.get("Action") == "1"){
                    
                    let vLatitude = dataDict.get("vLatitude")
                    let vLongitude = dataDict.get("vLongitude")
                    let vTripStatus = dataDict.get("vTripStatus")
                    
                    if(vTripStatus == "Arrived" && self.isDriverArrived == false){
                        self.setDriverArrivedStatus()
                    }
                    
                    if(vTripStatus == "Arrived"){
                        self.isDriverArrived = true
                        self.isTripStarted = true
                    }
                    
                    if(vTripStatus == "On Going Trip"){
                        self.isDriverArrived = true
                        self.isTripStarted = true
                    }
                    
                    if(vLatitude != "" && vLatitude != "0.0" && vLatitude != "-180.0" && vLongitude != "" && vLongitude != "0.0" && vLongitude != "-180.0"){
                        
                        self.updateAssignedDriverMarker(driverLocation: CLLocation(latitude: GeneralFunctions.parseDouble(origValue: 0.0, data: vLatitude), longitude: GeneralFunctions.parseDouble(origValue: 0.0, data: vLongitude)))
                    }
                }else{
                    
                    
                    //                    self.generalFunc.setError(uv: self, title: "", content: self.generalFunc.getLanguageLabel(origValue: "", key: dataDict.get("message")))
                }
                
            }else{
                //                self.generalFunc.setError(uv: self)
            }
        })
        
    }
    
    func updateAssignedDriverMarker(driverLocation:CLLocation){
        
        self.assignedDriverLocation = driverLocation
        
        if(self.assignedDriverMarker == nil){
            let driverMarker = GMSMarker()
            self.assignedDriverMarker = driverMarker
        }
        
        var zoom:Float = self.gMapView.camera.zoom
        if(assignedDriverRotatedLocation == nil){
            zoom = 15.0
        }
        
        
        let camera = GMSCameraPosition.camera(withLatitude: self.assignedDriverLocation.coordinate.latitude,
                                              longitude: self.assignedDriverLocation.coordinate.longitude, zoom: zoom)
        self.gMapView.moveCamera(GMSCameraUpdate.setCamera(camera))
        
        
        var rotationAngle:Double = -1
        if(assignedDriverRotatedLocation != nil){
            rotationAngle = assignedDriverRotatedLocation.bearingToLocationDegrees(destinationLocation: driverLocation, currentRotation: assignedDriverMarker.rotation)
            //            Utils.printLog(msgData: "rotationAngle0:\(rotationAngle)")
            if(rotationAngle != -1){
                assignedDriverRotatedLocation = driverLocation
            }
        }else{
            assignedDriverRotatedLocation = driverLocation
        }
        
        //        Utils.printLog(msgData: "rotationAngle1:\(rotationAngle)")
        Utils.updateMarker(marker: assignedDriverMarker, googleMap: self.gMapView, coordinates: driverLocation.coordinate, rotationAngle: rotationAngle, duration: 1.0)
        
        
//        let providerView = self.getProviderMarkerView(providerImage: UIImage(named: "ic_no_pic_user")!)
//        assignedDriverMarker.icon = UIImage(view: providerView)
        
//        (providerView.subviews[1] as! UIImageView).sd_setImage(with: URL(string: CommonUtils.driver_image_url + "\(dataDict.get("iDriverId"))/\(dataDict.get("driverImage"))"), placeholderImage: UIImage(named: "ic_no_pic_user"),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
//            self.assignedDriverMarker.icon = UIImage(view: providerView)
//        })
        
        
        assignedDriverMarker.icon = UIImage(named: "ic_driver_car_pin")
        assignedDriverMarker.map = self.gMapView
        assignedDriverMarker.infoWindowAnchor = CGPoint(x: 0.5, y: 0.5)
        assignedDriverMarker.isFlat = true
        
    }
    
    
    func getProviderMarkerView(providerImage:UIImage) -> UIView {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "ProviderMapMarkerView", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        view.frame.size = CGSize(width: 64, height: 100)
        
        GeneralFunctions.setImgTintColor(imgView: view.subviews[0] as! UIImageView, color: UIColor.UCAColor.AppThemeColor)
        
        view.subviews[1].layer.cornerRadius = view.subviews[1].frame.width / 2
        view.subviews[1].layer.masksToBounds = true
        let providerImgView = view.subviews[1] as! UIImageView
        providerImgView.image = providerImage
        
        return view
    }
    

    func setDriverArrivedStatus(){
        if(self.isDriverArrived == false){
            
            self.generalFunc.setAlertMessage(uv: self, title: "", content: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_DRIVER_ARRIVE"), positiveBtn: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_BTN_OK_TXT"), nagativeBtn: "", completionHandler: { (btnClickedIndex) in
                self.removeDriverTracking()
                self.getData()
            })
        }
        
        if(configPubNub != nil && dataDict.get("eFareType") != "Regular"){
            unSubscribeToDriverLocChannel()
        }
        
    }
    
    func cancelBooking(){
        self.view.endEditing(true)
        
        self.generalFunc.setAlertMessage(uv: self, title: "", content: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_TRIP_CANCEL_TXT"), positiveBtn: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_CANCEL_TRIP_NOW"), nagativeBtn: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_CONTINUE_TRIP_TXT"), completionHandler: { (btnClickedIndex) in
            
            if(btnClickedIndex == 0){
                self.continueCancelTrip()
            }
        })

    }
    
    func continueCancelTrip(status : String = "", reference: String = "", code: String = ""){
        self.view.endEditing(true)
        
        var parameters = ["type":"cancelTrip", "iUserId": GeneralFunctions.getMemberd(), "iDriverId": self.dataDict.get("iDriverId"), "UserType": Utils.appUserType, "iTripId": dataDict.get("iTripId")]
        if(GeneralFunctions.checkCardType() == "PayStack") {
            parameters["email"] = GeneralFunctions.getEmail()
            parameters["status"] = status
            parameters["reference"] = reference
            parameters["code"] = code
        }
        let exeWebServerUrl = ExeServerUrl(dict_data: parameters, currentView: self.view, isOpenLoader: true)
        exeWebServerUrl.executePostProcess(completionHandler: { (response) -> Void in
            
            if(response != ""){
                let dataDict = response.getJsonDataDict()
                
                if(dataDict.get("Action") == "1"){
                    
                    self.generalFunc.setAlertMessage(uv: self, title: "", content: self.generalFunc.getLanguageLabel(origValue: "Your trip is successfully canceled.", key: "LBL_SUCCESS_TRIP_CANCELED"), positiveBtn: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_BTN_OK_TXT"), nagativeBtn: "", completionHandler: { (btnClickedIndex) in
                        
//                        let window = Application.window
//                        
//                        let getUserData = GetUserData(uv: self, window: window!)
//                        getUserData.getdata()
                        self.releaseAllTask()
                        self.performSegue(withIdentifier: "unwindToMyOnGoingTripsScreen", sender: self)
                        
                    })
                }else{
                    
                    if(dataDict.get(Utils.message_str) == "DO_RESTART"){
//                        let window = Application.window
//                        
//                        let getUserData = GetUserData(uv: self, window: window!)
//                        getUserData.getdata()
                        self.releaseAllTask()
                        self.performSegue(withIdentifier: "unwindToMyOnGoingTripsScreen", sender: self)
                        return
                    }
                    else if (dataDict.get(Utils.message_str) == "send_pin") {
                        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "VerifyViewController") as! VerifyViewController
                        vc.finishDelegate = self
                        vc.reference = dataDict.get("reference")
                        vc.status = "send_pin"
                        vc.amount = dataDict.get("amount")
                        vc.modalPresentationStyle = .overCurrentContext
                        self.present(vc, animated: true, completion: nil)
                    }
                    else if (dataDict.get(Utils.message_str) == "send_otp") {
                        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "VerifyViewController") as! VerifyViewController
                        vc.finishDelegate = self
                        vc.reference = dataDict.get("reference")
                        vc.status = "send_otp"
                        vc.amount = dataDict.get("amount")
                        vc.modalPresentationStyle = .overCurrentContext
                        self.present(vc, animated: true, completion: nil)
                    }
                    else if (dataDict.get(Utils.message_str) == "send_phone") {
                        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "VerifyViewController") as! VerifyViewController
                        vc.finishDelegate = self
                        vc.reference = dataDict.get("reference")
                        vc.status = "send_phone"
                        vc.amount = dataDict.get("amount")
                        vc.modalPresentationStyle = .overCurrentContext
                        self.present(vc, animated: true, completion: nil)
                    }
                    else {
                    
                        self.generalFunc.setError(uv: self, title: "", content: self.generalFunc.getLanguageLabel(origValue: "", key: dataDict.get("message")))
                    }
                }
                
            }else{
                self.generalFunc.setError(uv: self)
            }
        })
        
    }
    
    func openPopUpMenu(){
        
        initializeMenu()
        
        if(menu.isShown){
            menu.hideMenu()
            return
        }else{
            menu.showMenu()
        }
    }
    
    func getData(){
        self.dataArrList.removeAll()
        self.tableView.reloadData()
        loaderView =  self.generalFunc.addMDloader(contentView: self.contentView)
        loaderView.backgroundColor = UIColor.clear
        self.cntView.isHidden = true
        let parameters = ["type":"getTripDeliveryLocations", "iTripId": dataDict.get("iTripId"), "userType": Utils.appUserType,"iUserId": GeneralFunctions.getMemberd(), "UserType": Utils.appUserType]
        
        let exeWebServerUrl = ExeServerUrl(dict_data: parameters, currentView: self.view, isOpenLoader: false)
        exeWebServerUrl.setDeviceTokenGenerate(isDeviceTokenGenerate: false)
        exeWebServerUrl.currInstance = exeWebServerUrl
        exeWebServerUrl.executePostProcess(completionHandler: { (response) -> Void in
            
            //            print("Response:\(response)")
            if(response != ""){
                let dataDict = response.getJsonDataDict()
                
                if(dataDict.get("Action") == "1"){
                    
                    let driverDetails = dataDict.getObj(Utils.message_str).getObj("driverDetails")
                    Utils.createRoundedView(view: self.providerImgView, borderColor: UIColor.clear, borderWidth: 0)
                    
                    self.providerImgView.sd_setImage(with: URL(string: CommonUtils.driver_image_url + "\(driverDetails.get("iDriverId"))/\(driverDetails.get("driverImage"))"), placeholderImage: UIImage(named: "ic_no_pic_user"),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                        
                    })
                    self.providerNameLbl.textColor = UIColor.UCAColor.AppThemeColor
                    self.providerNameLbl.text = driverDetails.get("driverName")
                    self.ratingView.rating = GeneralFunctions.parseFloat(origValue: 0, data: driverDetails.get("driverRating"))
                    self.sourceAddLbl.text = driverDetails.get("tSaddress")
                    
                    
                    if(driverDetails.get("driverStatus") == "Arrived"){
                        self.removeDriverTracking()
                        self.isDriverArrived = true
                    }
                    
                    if(driverDetails.get("On Going Trip") == "On Going Trip"){
                        self.isDriverArrived = true
                        self.isTripStarted = true
                    }
                    
                    let dataArr = dataDict.getObj(Utils.message_str).getArrObj("States")
                    
                    for i in 0 ..< dataArr.count{
                        let dataTemp = dataArr[i] as! NSDictionary
                        
                        self.dataArrList += [dataTemp]
                        
                    }
                    
                    self.tableView.reloadData()
                    
                    
                }else{
                    _ = GeneralFunctions.addMsgLbl(contentView: self.view, msg: self.generalFunc.getLanguageLabel(origValue: "", key: dataDict.get(Utils.message_str)))
                }
                
                self.cntView.isHidden = false
                
            }else{
                self.generalFunc.setError(uv: self)
            }
            
            self.loaderView.isHidden = true
        })
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        
        return self.dataArrList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyOnGoingTripDetailsTVCell", for: indexPath) as! MyOnGoingTripDetailsTVCell
        
        let item = self.dataArrList[indexPath.item]
        
        cell.progressMsgLbl.text = item.get("text")
        cell.progressTimeLbl.text = item.get("time")
        cell.progressPastTimeLbl.text = item.get("timediff")
        cell.noLbl.text = "\(indexPath.item + 1)"
        Utils.createRoundedView(view: cell.noView, borderColor: UIColor.clear, borderWidth: 0)
        
        cell.noView.backgroundColor = UIColor.UCAColor.AppThemeColor
        cell.noLbl.textColor = UIColor.UCAColor.AppThemeTxtColor
        cell.progressTimeLbl.textColor = UIColor.UCAColor.AppThemeColor
        
        cell.selectionStyle = .none
        cell.backgroundColor = UIColor.clear
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    
    
    func releaseAllTask(){
        
        if(gMapView != nil){
            gMapView!.stopRendering()
            gMapView!.removeFromSuperview()
            gMapView!.clear()
            gMapView!.delegate = nil
            gMapView = nil
        }
        
        if(configPubNub != nil){
            configPubNub!.releasePubNub()
            configPubNub = nil
        }
        
        if(self.updateFreqDriverLocTask != nil){
            self.updateFreqDriverLocTask.stopRepeatingTask()
            self.updateFreqDriverLocTask.onTaskRunCalled = nil
            self.updateFreqDriverLocTask = nil
        }
        
        GeneralFunctions.removeObserver(obj: self)
        
    }

}
extension MyOnGoingTripDetailsUV: VerifyRequestDelegate {
    func verifyRequested(_ code: String, reference: String, status: String, amount: String) {
        self.continueCancelTrip(status: status, reference: reference, code: code)
    }
}
