//
//  MainScreenUV.swift
//  PassengerApp
//
//  Created by NEW MAC on 11/05/17.
//  Copyright © 2017 V3Cube. All rights reserved.
//

import UIKit
import GoogleMaps

class MainScreenUV: UIViewController, GMSMapViewDelegate, OnLocationUpdateDelegate, AddressFoundDelegate, UICollectionViewDelegate, UICollectionViewDataSource , MyBtnClickDelegate, OnDirectionUpdateDelegate, OnTaskRunCalledDelegate{

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var googleMapContainerView: UIView!
    @IBOutlet weak var myLocImgView: UIImageView!
    @IBOutlet weak var myLocBottomMargin: NSLayoutConstraint!
    @IBOutlet weak var setPrefLocImgView: UIImageView!
    @IBOutlet weak var menuImgView: UIImageView!
    @IBOutlet weak var topRightView: UIView!
    @IBOutlet weak var bottomLeftView: UIView!
    @IBOutlet weak var sourcePinImgView: UIImageView!
    @IBOutlet weak var sourcePinImgViewWidth: NSLayoutConstraint!
    @IBOutlet weak var sourcePinImgViewHeight: NSLayoutConstraint!
    @IBOutlet weak var sourcePinImgViewOffset: NSLayoutConstraint!
    @IBOutlet weak var sourcePickUpEtaLbl: MyLabel!
    @IBOutlet weak var setPrefImgViewBottomMargin: NSLayoutConstraint!
    @IBOutlet weak var myLocImgViewBottomMargin: NSLayoutConstraint!
    
    //UFX releted Outlets
    @IBOutlet weak var ufxTableView: UITableView!
    
    //GenderView related OutLets
    @IBOutlet weak var genderVCloseImgView: UIImageView!
    @IBOutlet weak var genderHLbl: MyLabel!
    @IBOutlet weak var maleImgView: UIImageView!
    @IBOutlet weak var femaleImgView: UIImageView!
    @IBOutlet weak var maleLbl: MyLabel!
    @IBOutlet weak var femaleLbl: MyLabel!
    
    
    //Request PickUp BottomView OutLets
    @IBOutlet weak var cabTypeContainerViewHeight: NSLayoutConstraint!
    @IBOutlet weak var cabTypeCollectionView: UICollectionView!
    @IBOutlet weak var noCabTypeLbl: MyLabel!
    @IBOutlet weak var cashPaymentView: UIView!
    @IBOutlet weak var cardPaymentView: UIView!
    @IBOutlet weak var cashPayLbl: MyLabel!
    @IBOutlet weak var cardPayLbl: MyLabel!
    @IBOutlet weak var paymentContainerView: UIView!
    @IBOutlet weak var promoCodeLbl: MyLabel!
    @IBOutlet weak var promoCodeView: UIView!
    @IBOutlet weak var requestNowBtn: MyButton!
    @IBOutlet weak var rideLaterBtn: MyButton!
    @IBOutlet weak var rideLaterImgView: UIImageView!
    @IBOutlet weak var paymentOptionContainerView: UIView!
    @IBOutlet weak var paymentOptionContainerViewHeight: NSLayoutConstraint!
    @IBOutlet weak var seperaterViewXPostion: NSLayoutConstraint!
    @IBOutlet weak var payViewHeight: NSLayoutConstraint!
    @IBOutlet weak var payView: UIView!
    @IBOutlet weak var payImgView: UIImageView!
    @IBOutlet weak var payLbl: MyLabel!
    
    //Confirm Card Outlets
    @IBOutlet weak var confirmCardHLbl: MyLabel!
    @IBOutlet weak var confirmCardVLbl: MyLabel!
    @IBOutlet weak var confirmCardLbl: MyLabel!
    @IBOutlet weak var changeCardLbl: MyLabel!
    @IBOutlet weak var cancelCardLbl: MyLabel!
    
    // Promocode Outlets
    @IBOutlet weak var promoViewHLbl: MyLabel!
    @IBOutlet weak var promoTxtField: MyTextField!
    @IBOutlet weak var confirmPromoLbl: MyLabel!
    @IBOutlet weak var cancelPromoLbl: MyLabel!
    
    // RequestCab OutLets
    @IBOutlet weak var rippleBgView: UIView!
    @IBOutlet weak var cancelCabReqImgView: UIImageView!
    @IBOutlet weak var cancelCabReqBgView: UIView!
    @IBOutlet weak var reqNoteLbl: MyLabel!
    @IBOutlet weak var retryReqBtn: MyButton!
    @IBOutlet weak var requestCabNavBar: UINavigationBar!
    @IBOutlet weak var requestCanNavBarTitleView: UINavigationItem!
    @IBOutlet weak var requestCabTitleLbl: MyLabel!
    @IBOutlet weak var requestCabHeaderBarHeight: NSLayoutConstraint!
    
    // DriverDetail View OutLets
    @IBOutlet weak var deliveryConfirmCodeLbl: MyLabel!
    @IBOutlet weak var deliveryConfirmCodeLblHeight: NSLayoutConstraint!
    @IBOutlet weak var driverPicImgView: UIImageView!
    @IBOutlet weak var driverNameLbl: MyLabel!
    @IBOutlet weak var driverRatingBar: RatingView!
    @IBOutlet weak var driverRatingLbl: MyLabel!
    @IBOutlet weak var driverCardNameLbl: MyLabel!
    @IBOutlet weak var driverCarType: MyLabel!
    @IBOutlet weak var carIcImgView: UIImageView!
    @IBOutlet weak var carIcBgView: UIView!
    @IBOutlet weak var driverCallImgView: UIImageView!
    @IBOutlet weak var driverCarPlateNoLbl: MyLabel!
    @IBOutlet weak var driverCallView: UIView!
    @IBOutlet weak var driverCallLbl: MyLabel!
    @IBOutlet weak var driverMsgImgView: UIImageView!
    @IBOutlet weak var driverMsgView: UIView!
    @IBOutlet weak var driverMsgLbl: MyLabel!
    @IBOutlet weak var driverCancelTripImgView: UIImageView!
    @IBOutlet weak var driverCancelTripView: UIView!
    @IBOutlet weak var driverCancelTripLbl: MyLabel!
    @IBOutlet weak var driverShareImgView: UIImageView!
    @IBOutlet weak var driverShareView: UIView!
    @IBOutlet weak var driverShareLbl: MyLabel!
    
    // Surge Price OutLets
    @IBOutlet weak var surgePriceHLbl: MyLabel!
    @IBOutlet weak var surgePriceVLbl: MyLabel!
    @IBOutlet weak var surgePayAmtLbl: MyLabel!
    @IBOutlet weak var surgeAcceptBtn: MyButton!
    @IBOutlet weak var surgeLaterLbl: MyLabel!
    
    var genderView:UIView!
    
    var navItem:UINavigationItem!
    
    var userProfileJson:NSDictionary!
    
    var getLocation:GetLocation!
    
    var gMapView:GMSMapView!
    
    var currentLocation:CLLocation?
    var pickUpLocation:CLLocation!
    var pickUpAddress = ""
    var destLocation:CLLocation!
    var destAddress = ""
    
    var vVehicleImgPath = CommonUtils.webServer + "webimages/icons/VehicleType/"
    
    var configPubNub:ConfigPubNub?
    var window:UIWindow!
    var isDataSet = false
    
    let generalFunc = GeneralFunctions()
    
    var isFirstLocationUpdate = true
    
    var ufxCurrentLoadedDriverList = [NSDictionary]()
    var currentLoadedDriverList = [NSDictionary]()
    var currentLoadedDriverIdsArr = [String]()
    
    var selectedCabTypeId = ""
    
    var loadAvailableCab:LoadAvailableCab!
    
    var addressContainerView:AddressContainerView!
    var requestPickUpView:UIView!
    
    
    var pickUpAddViewFrame:CGRect!
    var destAddViewFrame:CGRect!
    
    var getAddressFrmLocation:GetAddressFromLocation!
    
    var isPickUpMode = true
    
    var cabTypesArr = [NSDictionary]()
    var cabTypesFareArr = [NSDictionary]()
    
    var isCashPayment = true
    
    var isCardValidated = false
    
    var promoCodeDialogView:UIView!
    var promoCodeBGDialogView:UIView!
    
    var surgePriceView:UIView!
    var surgePriceBGView:UIView!
    
    var confirmCardDialogView:UIView!
    var confirmCardBGDialogView:UIView!
    
    var requestCabView:UIView!
    var driverDetailView:UIView!
    
    var appliedPromoCode = ""
    
    var isDriverAssigned = false
    var isTripStarted = false
    var isTripFinished = false
    var assignedDriverId = ""
    var assignedTripId = ""
    var eTripType = ""
    
    var waitingMarker:GMSMarker!
    
    var assignedDriverData = [String:String]()
    
    var selectedDate = ""
    var selectedTimeZone = ""
    
    var isRideLaterFromSurge = false
    
    var isDriverArrived = false
    
    var assignedDriverLocation:CLLocation!
    
    var assignedDriverRotatedLocation:CLLocation!
    
    var assignedDriverMarker:GMSMarker!
    
    var updateDirection:UpdateDirections!
    
    var updateFreqDriverLocTask:UpdateFreqTask!
    
    var driverRequestQueueTimer:Timer!
    
    var currDriverReqPosition = 0
    
    var titleLbl:MyLabel!
    
    var emeImgView:UIImageView!
    
//    var transitionBottomLeft: JTMaterialTransition?
//    
//    var transitionTopRight: JTMaterialTransition?
    var currentTransition:JTMaterialTransition?
    
    var currentPickUpCountry = ""
    var currentPickUpCity = ""
    
    var cntView:UIView!
    
    var pickUpAddGeoCodeResult = ""
    var destAddGeoCodeResult = ""
    
    var tempPickUpAdd = ""
    var tempDestAdd = ""
    var tempPickUpGeoCodeResult = ""
    var tempDestGeoCodeResult = ""
    
    var locationDialog:OpenEnableLocationView!

    var currentCabGeneralType = Utils.cabGeneralType_Ride
    
    var isDeliveryDataEntered = false
    
    var receiverName = ""
    var receiverMobile = ""
    var pickUpIns = ""
    var deliveryIns = ""
    var packageTypeId = ""
    var packageDetails = ""
    
    var isRideLater_payBox = false
    var isAutoContinue_payBox = false
    
    var ufxSelectedVehicleTypeId = ""
    var ufxSelectedVehicleTypeName = ""
    
    var UFX_PAGE_MODE = ""
    
    var ufxLoaderView:UIView!
    
    var listLbl:MyLabel!
    var mapLbl:MyLabel!
    
    
    var ufxSelectedLatitude = ""
    var ufxSelectedLongitude = ""
    
    var ufxSelectedQty = ""
    
    var isPreferFemaleDriverEnable = false
    var isHandicapPrefEnabled = false
    
    var isMyLocationEnabled = true
    
    var isTollChecked = false
    
    var currTollPrice = ""
    var currTollPriceCurrencyCode = ""
    var currTollSkipped = ""
    
    var ufxSelectedServiceProviderId = ""
    
    var providerDetailMarkerView:ProviderDetailMarkerView!
    
    var ufxDriverAcceptedReqNow = false
    
    var RIDER_REQUEST_ACCEPT_TIME = 30
    
    var defaultRecentLocationViewHeight:CGFloat = 0
    var recentLocView:RecentLocationView!
    var arrivingNotificationCounter1 = false
    var arrivingNotificationCounter2 = false
    var arrivingNotificationCounter3 = false
    var lastArrivingNotificationTime:Int64!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //        let leftButton: UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "ic_menu_all")!, style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.openMenu))
        //        self.navigationItem.leftBarButtonItem = leftButton
        
        cntView = self.generalFunc.loadView(nibName: "MainScreenDesign", uv: self, contentView: contentView) //, isStatusBarAvail: true
        //        cntView.frame.size = CGSize(width: cntView.frame.width, height: cntView.frame.height - 100)
        self.contentView.addSubview(cntView)
        
        Utils.driverMarkersPositionList.removeAll()
        Utils.driverMarkerAnimFinished = true
        
        
        window = Application.window!
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.appInBackground), name: NSNotification.Name(rawValue: Utils.appBGNotificationKey), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.appInForground), name: NSNotification.Name(rawValue: Utils.appFGNotificationKey), object: nil)
        
        let userProfileJson = (GeneralFunctions.getValue(key: Utils.USER_PROFILE_DICT_KEY) as! String).getJsonDataDict().getObj(Utils.message_str)
        self.userProfileJson = userProfileJson
        
        if(self.userProfileJson.get("APP_TYPE").uppercased() == "DELIVERY"){
            self.currentCabGeneralType = Utils.cabGeneralType_Deliver
        }else if(self.userProfileJson.get("APP_TYPE").uppercased() == "UBERX"){
            self.currentCabGeneralType = Utils.cabGeneralType_UberX
        }
        
        setNavBar(isHidden: true)
        self.ufxTableView.isHidden = true
        
        self.menuImgView.isHidden = true
        
        //        transitionBottomLeft = JTMaterialTransition(animatedView: self.bottomLeftView, bgColor: UIColor.UCAColor.AppThemeColor.lighter(by: 35))
        //        transitionTopRight = JTMaterialTransition(animatedView: self.topRightView, bgColor: UIColor.UCAColor.AppThemeColor.lighter(by: 35))
        
        
        
        GeneralFunctions.removeValue(key: "isDriverAssigned")
        
        
        RIDER_REQUEST_ACCEPT_TIME = GeneralFunctions.getValue(key: Utils.RIDER_REQUEST_ACCEPT_TIME_KEY) == nil ? 30 : GeneralFunctions.parseInt(origValue: 30, data: (GeneralFunctions.getValue(key: Utils.RIDER_REQUEST_ACCEPT_TIME_KEY) as! String))
        
        Utils.printLog(msgData: "RIDER_REQUEST_ACCEPT_TIME:\(RIDER_REQUEST_ACCEPT_TIME)")
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.configureRTLView()
        
        
        let userProfileJson = (GeneralFunctions.getValue(key: Utils.USER_PROFILE_DICT_KEY) as! String).getJsonDataDict().getObj(Utils.message_str)
        self.userProfileJson = userProfileJson
        
        if(userProfileJson.get("APP_TYPE").uppercased() != "UBERX"){
            self.navigationController?.navigationBar.isTranslucent = true
        }
        
//        UIApplication.shared.isStatusBarHidden = true
        
        if(recentLocView != nil){
            recentLocView.checkPlaces()
        }
        self.view.layoutIfNeeded()
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if(self.loadAvailableCab != nil){
            self.loadAvailableCab.onPauseCalled()
        }
        
        if(userProfileJson.get("APP_TYPE").uppercased() != "UBERX"){
            self.navigationController?.navigationBar.isTranslucent = false
        }
        UIControl().sendAction(Selector(("_performMemoryWarning")), to: UIApplication.shared, for: nil)
        UIControl().sendAction(Selector(("_performMemoryWarning")), to: UIApplication.shared, for: nil)
        //        if(self.recentLocView != nil && self.isDriverAssigned == false && self.requestPickUpView == nil){
        //
        //        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
    }
    
    
    override func closeCurrentScreen() {
        if(self.requestPickUpView != nil && self.gMapView != nil && self.loadAvailableCab != nil){
            removeAddReqPickUpView()
            
            ufxSelectedServiceProviderId = ""
            let ufxPickLoc = CLLocation(latitude: GeneralFunctions.parseDouble(origValue: 0.0, data: self.ufxSelectedLatitude), longitude: GeneralFunctions.parseDouble(origValue: 0.0, data: self.ufxSelectedLongitude))
            self.pickUpLocation = ufxPickLoc
            
            let camera = GMSCameraPosition.camera(withLatitude: self.pickUpLocation!.coordinate.latitude,
                                                  longitude: self.pickUpLocation!.coordinate.longitude, zoom: self.gMapView.camera.zoom)
            self.gMapView.moveCamera(GMSCameraUpdate.setCamera(camera))
            
            getAddressFrmLocation.setLocation(latitude: pickUpLocation!.coordinate.latitude, longitude: pickUpLocation!.coordinate.longitude)
            getAddressFrmLocation.setPickUpMode(isPickUpMode: isPickUpMode)
            getAddressFrmLocation.executeProcess(isOpenLoader: false, isAlertShow:false)
            
            self.loadAvailableCab.filterDrivers(isCheckAgain: false)
            return
        }
        
        super.closeCurrentScreen()
    }
    
    override func didReceiveMemoryWarning() {
        Utils.printLog(msgData: "MemoryWarningReceived")
    }
    
    
    func checkLocationEnabled(){
        if(locationDialog != nil){
            locationDialog.closeView()
            locationDialog = nil
        }
        
        
        if((GeneralFunctions.hasLocationEnabled() == false && self.currentLocation == nil) || InternetConnection.isConnectedToNetwork() == false)
        {
            
            locationDialog = OpenEnableLocationView(uv: self, containerView: self.contentView, menuImgView: self.menuImgView)
            locationDialog.currentInst = locationDialog
            locationDialog.setViewHandler(handler: { (latitude, longitude, address, isMenuOpen) in
//                self.currentLocation = CLLocation(latitude: latitude, longitude: longitude)
//                self.setTripLocation(selectedAddress: address, selectedLocation: CLLocation(latitude: latitude, longitude: longitude))
                
                if(isMenuOpen){
                    if(InternetConnection.isConnectedToNetwork() == false){
                        self.continueOpenMenu()
                    }else{
                    self.openMenu()
                    }
                }else{
                    self.locationDialog.closeView()
                    self.locationDialog = nil
                    self.onLocationUpdate(location: CLLocation(latitude: latitude, longitude: longitude))
                }
            })
            locationDialog.show()
            
            return
        }else{
            if(self.currentLocation != nil && self.gMapView.isMyLocationEnabled != self.isMyLocationEnabled){
                self.gMapView.isMyLocationEnabled = self.isMyLocationEnabled
            }
        }
    }
    
    deinit {
        releaseAllTask()
    }
    
    func appInBackground(){
        if(self.loadAvailableCab != nil){
            self.loadAvailableCab.onPauseCalled()
        }
        
        if(self.isDriverAssigned == true){
            self.unSubscribeToDriverLocChannel()
        }
        UIControl().sendAction(Selector(("_performMemoryWarning")), to: UIApplication.shared, for: nil)
        UIControl().sendAction(Selector(("_performMemoryWarning")), to: UIApplication.shared, for: nil)
    }
    
    func appInForground(){
        if(self.loadAvailableCab != nil && requestCabView == nil && isDriverAssigned == false){
            self.loadAvailableCab.onResumeCalled()
        }
        
        if(self.isDriverAssigned == true){
            self.subscribeToDriverLocChannel()
        }
        
//        if(self.driverDetailView != nil){
//            driverDetailView.frame = CGRect(x: 0, y: self.contentView.frame.height - 220, width: Application.screenSize.width, height: 220)
//        }
//        if(self.addressContainerView != nil){
//            addressContainerView.frame = CGRect(x: self.contentView.bounds.midX - ((Application.screenSize.width - 60) / 2), y: 60, width: Application.screenSize.width - 60, height: 100)
//        }
        
        if(userProfileJson != nil && userProfileJson.get("APP_TYPE").uppercased() != "UBERX"){
            checkLocationEnabled()
        }
        UIControl().sendAction(Selector(("_performMemoryWarning")), to: UIApplication.shared, for: nil)
        UIControl().sendAction(Selector(("_performMemoryWarning")), to: UIApplication.shared, for: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if(isDataSet == false){

            let camera = GMSCameraPosition.camera(withLatitude: 0.0, longitude: 0.0, zoom: 0.0)
            gMapView = GMSMapView.map(withFrame: self.googleMapContainerView.frame, camera: camera)
            //        googleMapContainerView = gMapView
            //        gMapView = GMSMapView()
//            gMapView.isMyLocationEnabled = true
            
            if(GeneralFunctions.hasLocationEnabled() == true){
                gMapView.isMyLocationEnabled = self.isMyLocationEnabled
            }
            
//            gMapView.delegate = self
            self.googleMapContainerView.addSubview(gMapView)
            
            self.gMapView.settings.allowScrollGesturesDuringRotateOrZoom = false
            
            
            setData()
            
            let vTripStatus = userProfileJson.get("vTripStatus")
            
            if(userProfileJson.get("APP_TYPE").uppercased() != "UBERX" && vTripStatus != "Active" && vTripStatus != "On Going Trip"){
                checkLocationEnabled()
            }
            
            if(vTripStatus == "Active" || vTripStatus == "On Going Trip"){
                let tripDetailJson = userProfileJson.getObj("TripDetails")
                let pickUpLocation = CLLocation(latitude: GeneralFunctions.parseDouble(origValue: 0.0, data: tripDetailJson.get("tStartLat")), longitude: GeneralFunctions.parseDouble(origValue: 0.0, data: tripDetailJson.get("tStartLong")))
                
                onLocationUpdate(location: pickUpLocation)
            }
            
            
            isDataSet = true
            
        }
        
        if(self.loadAvailableCab != nil && requestCabView == nil && isDriverAssigned == false){
            self.loadAvailableCab.onResumeCalled()
        }
        
        
//        if(self.driverDetailView != nil){
//            driverDetailView.frame = CGRect(x: 0, y: self.contentView.frame.height - 220, width: Application.screenSize.width, height: 220)
//        }
//        
//        if(self.addressContainerView != nil){
//            addressContainerView.frame = CGRect(x: self.contentView.bounds.midX - ((Application.screenSize.width - 60) / 2), y: 60, width: Application.screenSize.width - 60, height: 100)
//        }
    }
    
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        
        if(sourcePinImgView.isHidden == true){
            self.sourcePickUpEtaLbl.isHidden = true
            return
        }

        if(isPickUpMode){
            self.addressContainerView.pickUpAddressLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_SELECTING_LOCATION_TXT")
                
            self.tempPickUpAdd = self.pickUpAddress
            self.tempPickUpGeoCodeResult = self.pickUpAddGeoCodeResult
                
            if(self.loadAvailableCab != nil){
                self.loadAvailableCab.pickUpAddress = ""
            }
                
                self.pickUpAddGeoCodeResult = ""
            }else{
                self.tempDestAdd = self.destAddress
                self.tempDestGeoCodeResult = self.destAddGeoCodeResult
                
                self.addressContainerView.destAddressLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_SELECTING_LOCATION_TXT")
                self.destAddGeoCodeResult = ""
            }
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        if(self.currentCabGeneralType == Utils.cabGeneralType_UberX && isDriverAssigned == false && requestPickUpView == nil){
            
            if(providerDetailMarkerView != nil){
                providerDetailMarkerView.closeViewTapped()
            }
            
           
            let driver_id = marker.title!.replace("DriverId", withString: "")
            
            for i in 0..<currentLoadedDriverList.count{
            
                let driverId = currentLoadedDriverList[i].get("driver_id")
                
                if(driverId == driver_id){
                    
                    let providerDetailMarkerView = ProviderDetailMarkerView(frame: CGRect(x: 0, y: self.view.frame.height + 160, width: Application.screenSize.width, height: 160))
                    self.providerDetailMarkerView = providerDetailMarkerView
                    providerDetailMarkerView.setViewHandler(handler: { (view, isViewClose, isMoreInfoBtnTapped) in
                        if(isViewClose){
//                            self.myLocImgView.frame.origin.y = self.view.frame.size.height - 15 - self.myLocImgView.frame.height
                            self.myLocBottomMargin.constant = 15
                            providerDetailMarkerView.frame.origin.y = Application.screenSize.height + 2500
                            providerDetailMarkerView.removeFromSuperview()
                        }else if(isMoreInfoBtnTapped == true){
//                            self.myLocImgView.frame.origin.y = self.view.frame.size.height - 15 - self.myLocImgView.frame.height
                            self.myLocBottomMargin.constant = 15
                            providerDetailMarkerView.frame.origin.y = Application.screenSize.height + 2500
                            providerDetailMarkerView.removeFromSuperview()
                        }
                    })
                    
                    let vName = currentLoadedDriverList[i].get("Name") + " " + currentLoadedDriverList[i].get("LastName")
                    
                    providerDetailMarkerView.providerNameLbl.text = vName
                    
                    providerDetailMarkerView.providerImgView.sd_setImage(with: URL(string: CommonUtils.driver_image_url + "\(currentLoadedDriverList[i].get("driver_id"))/\(currentLoadedDriverList[i].get("driver_img"))"), placeholderImage: UIImage(named: "ic_no_pic_user"),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                        
                    })
                    Utils.createRoundedView(view: providerDetailMarkerView.providerImgView, borderColor: UIColor.UCAColor.AppThemeColor, borderWidth: 1)
                    
                    providerDetailMarkerView.ratingBar.rating = GeneralFunctions.parseFloat(origValue: 0, data: currentLoadedDriverList[i].get("average_rating"))
                    
                    if(currentLoadedDriverList[i].get("fAmount") != ""){
                        providerDetailMarkerView.priceLbl.text = currentLoadedDriverList[i].get("fAmount")
                        providerDetailMarkerView.priceLbl.isHidden = false
                    }else{
                        providerDetailMarkerView.priceLbl.isHidden = true
                    }
                    
                    if(self.userProfileJson.get("eUnit") == "KMs"){
                        providerDetailMarkerView.distanceLbl.text = "\((GeneralFunctions.parseDouble(origValue: 0, data: currentLoadedDriverList[i].get("DIST_TO_PICKUP")).roundTo(places: 2)))" + " \(self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_KM_DISTANCE_TXT")) \(self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_AWAY"))"
                    }else{
                        providerDetailMarkerView.distanceLbl.text = "\(((GeneralFunctions.parseDouble(origValue: 0, data: currentLoadedDriverList[i].get("DIST_TO_PICKUP")) * 0.621371).roundTo(places: 2)))" + " \(self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_MILE_DISTANCE_TXT")) " + self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_AWAY")
                    }
                    providerDetailMarkerView.moreInfoBtn.btnType = "UFX_DRIVER_MORE_INFO"
                    providerDetailMarkerView.moreInfoBtn.tag = i
                    providerDetailMarkerView.moreInfoBtn.clickDelegate = self
                    
                     self.view.addSubview(providerDetailMarkerView)
                    
                    
                    UIView.animate(withDuration: 0.5,
                                   animations: {
                                    //                        self.requestPickUpView.center = CGPoint(x: 0, y: 310)
                                    providerDetailMarkerView.frame.origin.y = self.view.frame.size.height - 160
//                                    self.myLocImgView.frame.origin.y = self.myLocImgView.frame.origin.y - 160
                                    self.myLocBottomMargin.constant = self.myLocBottomMargin.constant + 160
                                    self.view.layoutIfNeeded()
                    },  completion: { finished in
                        
                    })
                }
                
            
            }
            
            return true
        }
        mapView.selectedMarker = nil
        return true
    }
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        if(self.isDriverAssigned == true){
            return
        }
        
        if(sourcePinImgView.isHidden == true){
            self.sourcePickUpEtaLbl.isHidden = true
            return
        }
        
        changeAddress()
    }
    
    func changeAddress(){
        var isSameLocation = false
        
        if(isPickUpMode){
            if(self.pickUpLocation != nil && self.pickUpLocation!.coordinate.latitude == getCenterLocation().coordinate.latitude && self.pickUpLocation!.coordinate.longitude == getCenterLocation().coordinate.longitude){
                isSameLocation = true
            }else{
                self.addressContainerView.pickUpAddressLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_SELECTING_LOCATION_TXT")
                if(self.loadAvailableCab != nil){
                    self.loadAvailableCab.pickUpAddress = ""
                }
                
                self.pickUpAddGeoCodeResult = ""
            }
            
        }else{
            if(self.destLocation != nil && self.destLocation!.coordinate.latitude == getCenterLocation().coordinate.latitude && self.destLocation!.coordinate.longitude == getCenterLocation().coordinate.longitude){
                isSameLocation = true
            }else{
                self.addressContainerView.destAddressLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_SELECTING_LOCATION_TXT")
                self.destAddGeoCodeResult = ""
            }
            
        }
        
        
        
        if(isSameLocation == false){
            getAddressFrmLocation.setLocation(latitude: getCenterLocation().coordinate.latitude, longitude: getCenterLocation().coordinate.longitude)
            getAddressFrmLocation.setPickUpMode(isPickUpMode: isPickUpMode)
            getAddressFrmLocation.executeProcess(isOpenLoader: false, isAlertShow:false)
            
//            if(self.requestPickUpView != nil){
//                estimateFare()
//            }
        }else{
            if(isPickUpMode){
                self.addressContainerView.pickUpAddressLbl.text = self.tempPickUpAdd
                if(self.loadAvailableCab != nil){
                    self.loadAvailableCab.pickUpAddress = self.tempPickUpAdd
                }
                
                self.pickUpAddGeoCodeResult = self.tempPickUpGeoCodeResult
            }else{
                self.addressContainerView.destAddressLbl.text = self.tempDestAdd
                self.destAddGeoCodeResult = self.tempDestGeoCodeResult
            }
        }
    }
    
    func getCenterLocation() -> CLLocation{
        return CLLocation(latitude: self.gMapView.camera.target.latitude, longitude: self.gMapView.camera.target.longitude)
    }
    
    
    func setData(){
        
        let userProfileJson = (GeneralFunctions.getValue(key: Utils.USER_PROFILE_DICT_KEY) as! String).getJsonDataDict().getObj(Utils.message_str)
        self.userProfileJson = userProfileJson
        
        if(self.getPubNubConfig().uppercased() == "YES"){
            configPubNub = ConfigPubNub()
            configPubNub!.mainScreenUv = self
            configPubNub!.buildPubNub()
        }
        
        getAddressFrmLocation = GetAddressFromLocation(uv: self)
        getAddressFrmLocation.addressFoundDelegate = self
        
        self.getLocation = GetLocation(uv: self, isContinuous: true)
        self.getLocation.buildLocManager(locationUpdateDelegate: self)
        
        let userLocTapGue = UITapGestureRecognizer()
        userLocTapGue.addTarget(self, action: #selector(self.myLocImgTapped))
        self.myLocImgView.isUserInteractionEnabled = true
        self.myLocImgView.addGestureRecognizer(userLocTapGue)
        
        
        let setPrefTapGue = UITapGestureRecognizer()
        setPrefTapGue.addTarget(self, action: #selector(self.myPrefTapped))
        
        self.setPrefLocImgView.isUserInteractionEnabled = true
        self.setPrefLocImgView.addGestureRecognizer(setPrefTapGue)
        
        let HANDICAP_ACCESSIBILITY_OPTION = userProfileJson.get("HANDICAP_ACCESSIBILITY_OPTION")
        let FEMALE_RIDE_REQ_ENABLE = userProfileJson.get("FEMALE_RIDE_REQ_ENABLE")
        
        if((HANDICAP_ACCESSIBILITY_OPTION.uppercased() != "YES" && FEMALE_RIDE_REQ_ENABLE.uppercased() != "YES") || (FEMALE_RIDE_REQ_ENABLE.uppercased() == "YES" && userProfileJson.get("eGender") == "Male" && HANDICAP_ACCESSIBILITY_OPTION.uppercased() != "YES")){
            self.setPrefLocImgView.isHidden = true
        }
        
        let menuImgTapGue = UITapGestureRecognizer()
        menuImgView.isUserInteractionEnabled = true
        menuImgTapGue.addTarget(self, action: #selector(self.openMenu))
        menuImgView.addGestureRecognizer(menuImgTapGue)
        
        GeneralFunctions.setImgTintColor(imgView: menuImgView, color: UIColor(hex: 0x000000))
        
        if(userProfileJson.get("eEmailVerified").uppercased() != "YES" || userProfileJson.get("ePhoneVerified").uppercased() != "YES" ){
            let verifyBtn = Utils.showSnakeBar(msg: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_ACCOUNT_VERIFY_ALERT_RIDER_TXT"), uv: self, btnTitle: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_BTN_VERIFY_TXT"), delayShow: 1, delayHide: 15)
            verifyBtn.addTarget(self, action: #selector(self.openAccountVerify), for: UIControlEvents.touchUpInside)
        }
        
        if(userProfileJson.get("APP_TYPE").uppercased() == "UBERX" && GeneralFunctions.hasLocationEnabled() == false){
            if(self.ufxSelectedLatitude != "" && self.ufxSelectedLongitude != ""){
                
                let ufxPickLoc = CLLocation(latitude: GeneralFunctions.parseDouble(origValue: 0.0, data: self.ufxSelectedLatitude), longitude: GeneralFunctions.parseDouble(origValue: 0.0, data: self.ufxSelectedLongitude))
                
                //                self.pickUpLocation = ufxPickLoc
                //                let camera = GMSCameraPosition.camera(withLatitude: ufxPickLoc.coordinate.latitude,
                //                                                      longitude: ufxPickLoc.coordinate.longitude, zoom: self.gMapView.camera.zoom)
                //
                //                self.gMapView.moveCamera(GMSCameraUpdate.setCamera(camera))
                
                self.onLocationUpdate(location: ufxPickLoc)
            }
        }
    }
    
    func openAccountVerify(){

        self.snackbarController?.animate(snackbar: .hidden, delay: 0)

        let accountVerificationUv = GeneralFunctions.instantiateViewController(pageName: "AccountVerificationUV") as! AccountVerificationUV
        if(userProfileJson.get("eEmailVerified").uppercased() != "YES" && userProfileJson.get("ePhoneVerified").uppercased() != "YES" ){
            accountVerificationUv.requestType = "DO_EMAIL_PHONE_VERIFY"
        }else if(userProfileJson.get("eEmailVerified").uppercased() != "YES"){
            accountVerificationUv.requestType = "DO_EMAIL_VERIFY"
        }else{
            accountVerificationUv.requestType = "DO_PHONE_VERIFY"
        }
        accountVerificationUv.mainScreenUv = self
        self.pushToNavController(uv: accountVerificationUv)
    }
    
    func setNavBar(isHidden:Bool){
        
        if(isHidden == true){
            self.navigationController?.navigationBar.isHidden = true
            self.menuImgView.isHidden = false
        }else{
            self.navigationController?.navigationBar.isHidden = false
            self.menuImgView.isHidden = true
            
            UIApplication.shared.statusBarStyle = UIStatusBarStyle.lightContent
            
            if(navItem != nil){
                let leftButton: UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "ic_menu_all_nav")!, style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.openMenu))
                self.navItem.leftBarButtonItem = leftButton
            }
            
        }
    }
    
    func setLblToTitleView(){
        
        self.navItem.titleView = nil
        if(self.titleLbl == nil){
            titleLbl = MyLabel(frame: CGRect(x: 0, y:0, width: Application.screenSize.width, height: 50))
        }
        titleLbl.font = UIFont(name: "Roboto-Light", size: 20)!
        titleLbl.textAlignment = .center
        titleLbl.textColor = UIColor.UCAColor.AppThemeTxtColor
        
        self.navItem.titleView = titleLbl
    }
    
    func openMenu(){
        if(self.requestPickUpView != nil){
            removeAddReqPickUpView()
            return
        }
        
        continueOpenMenu()
        
    }
    func continueOpenMenu(){
        if(Configurations.isRTLMode()){
            //            self.navigationDrawerController?.setRightViewOpened(isRightViewOpened: false)
            self.navigationDrawerController?.toggleRightView()
            
            //            self.navigationDrawerController?.setRightViewOpened(isRightViewOpened: true)
        }else{
            self.navigationDrawerController?.toggleLeftView()
        }
    }
    
    func removeAddReqPickUpView(isDriverAssigned:Bool = false){
        if(self.requestPickUpView != nil){
            
//            if(self.currentCabGeneralType.uppercased() == Utils.cabGeneralType_UberX.uppercased()){
//                self.myLocImgView.frame.origin.y = self.myLocImgView.frame.origin.y + (255 - 134)
//            }else{
//                self.myLocImgView.frame.origin.y = self.myLocImgView.frame.origin.y + 255
//            }
//            self.myLocImgView.frame.origin.y = self.myLocImgView.frame.origin.y + self.requestPickUpView.frame.height
            
//            self.myLocImgView.frame.origin.y = self.view.frame.size.height - 15 - self.myLocImgView.frame.height
            self.myLocBottomMargin.constant = 15 + defaultRecentLocationViewHeight
            
            sourcePinImgViewWidth.constant = 50
            sourcePinImgViewHeight.constant = 50
            sourcePinImgViewOffset.constant = -25
            self.sourcePickUpEtaLbl.isHidden = true
            
            self.requestPickUpView.removeFromSuperview()
            self.requestPickUpView = nil
            self.menuImgView.image = UIImage(named: "ic_menu_all")!
            GeneralFunctions.setImgTintColor(imgView: menuImgView, color: UIColor(hex: 0x000000))
            
            if(self.addressContainerView != nil){
                self.addressContainerView.pickUpTapped(isOpenSelection: false)
            }
            
            if(self.currentCabGeneralType == Utils.cabGeneralType_UberX){
                self.addressContainerView.isHidden = true
            }
            
            sourcePinImgView.isHidden = true
            
            if(isDriverAssigned == false){
                self.destLocation = nil
                self.destAddress = ""
                self.appliedPromoCode = ""
                
                addressContainerView.destAddressLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_ADD_DESTINATION_BTN_TXT")
                
                self.selectedCabTypeId = getFirstCarTypeID()
                
                if(self.loadAvailableCab != nil && requestCabView == nil && isDriverAssigned == false){
                    self.loadAvailableCab.onResumeCalled()
                }
            }
           
            return
        }
    }
    
    func myPrefTapped(){
        Utils.printLog(msgData: "Gender::\(self.userProfileJson.get("eGender"))")
        if(self.userProfileJson.get("eGender") == "" && userProfileJson.get("FEMALE_RIDE_REQ_ENABLE").uppercased() == "YES"){
            openGenderView()
        }else{
            let openPrefOptionsView = OpenPrefOptionsView(uv: self, containerView: self.view)
            openPrefOptionsView.currentInst = openPrefOptionsView
            openPrefOptionsView.isPreferFemaleDriverEnable = self.isPreferFemaleDriverEnable
            openPrefOptionsView.isHandicapPrefEnabled = self.isHandicapPrefEnabled
            openPrefOptionsView.setViewHandler { (isPreferFemaleDriverEnable, isHandicapPrefEnabled) in
                self.isPreferFemaleDriverEnable = isPreferFemaleDriverEnable
                self.isHandicapPrefEnabled = isHandicapPrefEnabled
                
                if(self.loadAvailableCab != nil){
                    self.loadAvailableCab.changeCabs()
                }
            }
            
            openPrefOptionsView.show()
        }
        
    }
    
    func openGenderView(){
        
        genderView = self.generalFunc.loadView(nibName: "GenderView", uv: self, isWithOutSize: true)
        
        genderView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
       
        self.view.addSubview(genderView)
        
        let closeTapGue = UITapGestureRecognizer()
        closeTapGue.addTarget(self, action: #selector(self.closeGenderView))
        
        self.genderVCloseImgView.isUserInteractionEnabled = true
        self.genderVCloseImgView.addGestureRecognizer(closeTapGue)
        
        self.genderHLbl.text = self.generalFunc.getLanguageLabel(origValue: "Select your gender to continue", key: "LBL_SELECT_GENDER")
        self.maleLbl.text = self.generalFunc.getLanguageLabel(origValue: "Male", key: "LBL_MALE_TXT")
        self.femaleLbl.text = self.generalFunc.getLanguageLabel(origValue: "Female", key: "LBL_FEMALE_TXT")
        GeneralFunctions.setImgTintColor(imgView: self.genderVCloseImgView, color: UIColor.white)
        
        let maleTapGue = UITapGestureRecognizer()
        maleTapGue.addTarget(self, action: #selector(self.maleImgTapped))
        
        self.maleImgView.isUserInteractionEnabled = true
        self.maleImgView.addGestureRecognizer(maleTapGue)
        
        
        let femaleTapGue = UITapGestureRecognizer()
        femaleTapGue.addTarget(self, action: #selector(self.femaleImgTapped))
        
        self.femaleImgView.isUserInteractionEnabled = true
        self.femaleImgView.addGestureRecognizer(femaleTapGue)
    }
    
    func maleImgTapped(){
    
        self.closeGenderView()
        updateUserGender(eGender: "Male")
    }
    
    func femaleImgTapped(){
        self.closeGenderView()
        updateUserGender(eGender: "Female")
    }
    
    func closeGenderView(){
        if(self.genderView != nil){
            self.genderView.removeFromSuperview()
        }
    }
    
    func updateUserGender(eGender:String){
        
        let parameters = ["type":"updateUserGender", "iMemberId": GeneralFunctions.getMemberd(), "UserType": Utils.appUserType, "eGender": eGender]
        
        let exeWebServerUrl = ExeServerUrl(dict_data: parameters, currentView: self.view, isOpenLoader: true)
        exeWebServerUrl.setDeviceTokenGenerate(isDeviceTokenGenerate: false)
        exeWebServerUrl.currInstance = exeWebServerUrl
        exeWebServerUrl.executePostProcess(completionHandler: { (response) -> Void in
            
            if(response != ""){
                let dataDict = response.getJsonDataDict()
                
                if(dataDict.get("Action") == "1"){
                    
                    GeneralFunctions.saveValue(key: Utils.USER_PROFILE_DICT_KEY, value: response as AnyObject)
                    
                    let userProfileJson = (GeneralFunctions.getValue(key: Utils.USER_PROFILE_DICT_KEY) as! String).getJsonDataDict().getObj(Utils.message_str)
                    
                    self.userProfileJson = userProfileJson
                    
                    self.myPrefTapped()
                    
                }else{
                    self.generalFunc.setError(uv: self, title: "", content: self.generalFunc.getLanguageLabel(origValue: "", key: dataDict.get("message")))
                }
                
            }else{
                self.generalFunc.setError(uv: self)
            }
        })
        
    }
    
    func myLocImgTapped(){
        
        if(GeneralFunctions.hasLocationEnabled() == true)
        {
        if(self.currentLocation == nil){
            return
        }
        
        let camera = GMSCameraPosition.camera(withLatitude: self.currentLocation!.coordinate.latitude,
                                              longitude: self.currentLocation!.coordinate.longitude, zoom: self.gMapView.camera.zoom)
        
        
        if(self.isPickUpMode && self.isDriverAssigned == false && self.addressContainerView != nil && self.loadAvailableCab != nil){
            self.addressContainerView.pickUpAddressLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_SELECTING_LOCATION_TXT")
            self.loadAvailableCab.pickUpAddress = ""
        }
        
        if(self.isPickUpMode && self.isDriverAssigned == false){
            
            self.tempPickUpAdd = self.pickUpAddress
            self.tempPickUpGeoCodeResult = self.pickUpAddGeoCodeResult
            
            self.pickUpAddGeoCodeResult = ""
//            self.changeAddress()
            
            if(self.requestPickUpView == nil){
                
                self.gMapView.moveCamera(GMSCameraUpdate.setCamera(camera))
                
                self.changeAddress()
            }else{
                self.gMapView.moveCamera(GMSCameraUpdate.setCamera(camera))
            }
        }else{
            
            self.gMapView.moveCamera(GMSCameraUpdate.setCamera(camera))
            }
        }
        else
        {
            self.generalFunc.setError(uv: self, title: "", content: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_GPSENABLE_TXT"))
            
        }
        
    }
    
    func onLocationUpdate(location: CLLocation) {
        if(gMapView == nil){
            releaseAllTask()
            return
        }
        
        self.currentLocation = location
        
        
        var currentZoomLevel:Float = self.gMapView.camera.zoom
        
        if(currentZoomLevel < 15.0 && isFirstLocationUpdate == true){
            currentZoomLevel = 15.0
        }
        if(isFirstLocationUpdate == true){
//             || self.isDriverAssigned == true || self.requestPickUpView == nil
            let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude,
                                                  longitude: location.coordinate.longitude, zoom: currentZoomLevel)
            
            self.gMapView.moveCamera(GMSCameraUpdate.setCamera(camera))
            
            self.pickUpLocation = currentLocation
            
        }
        
        checkLocationEnabled()
        
        if(isFirstLocationUpdate){
            initializeView()
        }
        
        isFirstLocationUpdate = false
        
        
        
    }
    
    var isFirstAddress = true
    
    func onAddressFound(address: String, location: CLLocation, isPickUpMode:Bool, dataResult:String) {
        if(isPickUpMode){
            addressContainerView.pickUpAddressLbl.text = address
            self.pickUpLocation = location
            self.pickUpAddress = address
            
            self.pickUpAddGeoCodeResult = dataResult
            
            var isFirstSearch = false
            
            if(loadAvailableCab == nil){
                loadAvailableCab = LoadAvailableCab(gMapView: self.gMapView, mainScreenUv: self)
                
                loadAvailableCab.pickUpAddress = address
                loadAvailableCab.currentGeoCodeResult = dataResult
                
                
                self.selectedCabTypeId = getFirstCarTypeID()
                
                if(pickUpLocation == nil){
                    self.pickUpLocation = self.currentLocation!
                }
                loadAvailableCab.setCabTypeId(selectedCabTypeId: self.selectedCabTypeId)
                loadAvailableCab.setPickUpLocation(pickUpLocation: location)
                loadAvailableCab.changeCabs()
                
                isFirstSearch = true
            }
            
            if(self.loadAvailableCab != nil && isFirstSearch == false){
                self.loadAvailableCab.setPickUpLocation(pickUpLocation: location)
                self.loadAvailableCab.pickUpAddress = address
                self.loadAvailableCab.currentGeoCodeResult = dataResult
                self.loadAvailableCab.changeCabs()
                if(isPickUpAddressStateChanged(address: address) == true){
                    self.loadAvailableCab.checkAvailableCabs()
                }
            }
            
            
            
            if(isFirstAddress == true){
                self.gMapView.delegate = self
                isFirstAddress = false
                
                addressContainerView.destViewTapped(isAutoOpenSelection: false)
            }
            
        }else{
            
            self.destAddGeoCodeResult = dataResult
            
            self.destLocation = location
            self.destAddress = address
            
            addressContainerView.destAddressLbl.text = address
        }
        
        
        if(requestPickUpView != nil){
            self.cabTypesFareArr.removeAll()
            self.cabTypeCollectionView.reloadData()
            estimateFare()
        }
        
    }
    
    func isPickUpAddressStateChanged(address:String) -> Bool{
         let addressComponents = address.components(separatedBy: ",")
        
        var isCountryMatched = false
        var isCityMatched = false
        
        for i in 0..<addressComponents.count{
            let addr = addressComponents[i].trim()
            
            if(self.currentPickUpCountry != "" && self.currentPickUpCountry.containsIgnoringCase(find: addr)){
                isCountryMatched = true
            }
            
            if(self.currentPickUpCity != "" && self.currentPickUpCity.containsIgnoringCase(find: addr)){
                isCityMatched = true
            }
            
        }
        
        if(isCountryMatched == false || isCityMatched == false){
            return true
        }
        
        return false
    }
    
    func releaseAllTask(isDismiss:Bool = true){
        Utils.printLog(msgData: "ReleaseAllTask Called: MainScreen")
        
        if(gMapView != nil){
            gMapView!.stopRendering()
            gMapView!.removeFromSuperview()
            gMapView!.clear()
            gMapView!.delegate = nil
            gMapView = nil
        }
        if(getAddressFrmLocation != nil){
            getAddressFrmLocation!.addressFoundDelegate = nil
            getAddressFrmLocation = nil
        }
        
        if(configPubNub != nil){
            configPubNub!.releasePubNub()
            configPubNub = nil
        }
        
        if(self.getLocation != nil){
            self.getLocation!.locationUpdateDelegate = nil
            self.getLocation!.releaseLocationTask()
            self.getLocation = nil
        }
        
        if(self.updateDirection != nil){
            self.updateDirection.releaseTask()
            self.updateDirection.onDirectionUpdateDelegate = nil
            self.updateDirection = nil
        }
        
        if(self.updateFreqDriverLocTask != nil){
            self.updateFreqDriverLocTask.stopRepeatingTask()
            self.updateFreqDriverLocTask.onTaskRunCalled = nil
            self.updateFreqDriverLocTask = nil
        }
        
        stopDriverRequestQueue()
        
        GeneralFunctions.removeObserver(obj: self)
        
        
        if(isDismiss){
            self.dismiss(animated: false, completion: nil)
            self.navigationController?.dismiss(animated: false, completion: nil)
        }
    }
    
    
    func initializeView(){
        
        addressContainerView = AddressContainerView(frame: CGRect(x: self.contentView.bounds.midX - ((Application.screenSize.width - 60) / 2), y: -140, width: Application.screenSize.width - 60, height: 100), mainScreenUv: self)
        
        addressContainerView.setViewHandler(handler: { (isPickUpMode, view) in
            self.isPickUpMode = isPickUpMode
            if(self.recentLocView != nil){
                self.recentLocView.checkRecentPlaces()
            }
        })
        
        self.contentView.addSubview(addressContainerView)
        
        if(userProfileJson.get("APP_TYPE").uppercased() == "UBERX"){
            self.addressContainerView.isHidden = true
        }
        
        UIView.animate(withDuration: 0.8, delay: 0.1, options: .transitionFlipFromTop,
                                animations: {
//                                    self.addressContainerView.center = CGPoint(x: self.contentView.bounds.midX, y: 140)
                                    self.addressContainerView.frame.origin.y = self.userProfileJson.get("APP_TYPE").uppercased() != "UBERX" ? 75 : 20
                                    self.view.layoutIfNeeded()
                                },  completion: { finished in
                                    
        })
        
        let vTripStatus = userProfileJson.get("vTripStatus")
        if(userProfileJson.get("APP_TYPE").uppercased() != "UBERX" && (vTripStatus == "Active" || vTripStatus == "On Going Trip")){
            
            setNavBar(isHidden: false)
            setLblToTitleView()
            self.titleLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: vTripStatus == "Active" ? "LBL_DRIVER_ARRIVING_TXT" : "LBL_EN_ROUTE_TXT")
            
            configureAssignedDriver(isAppRestarted: true)
            
            
        }else{
            if(userProfileJson.get("APP_TYPE").uppercased() != "UBERX"){
                self.menuImgView.isHidden = false
                self.ufxTableView.isHidden = true
                
            }else{
                let rightNavBarView = UIView(frame: CGRect(x: Application.screenSize.width - 150, y: 0, width: 150, height: 40))
                rightNavBarView.backgroundColor = UIColor.UCAColor.AppThemeColor.darker(by: 10)
                listLbl = MyLabel(frame: CGRect(x: 0, y:0, width: 75, height: 40))
                listLbl.font = UIFont(name: "Roboto-Medium", size: 19)!
                listLbl.textAlignment = .center
                listLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_LIST_TXT")
                listLbl.textColor = UIColor.UCAColor.AppThemeColor
                listLbl.backgroundColor = UIColor.UCAColor.AppThemeTxtColor
                rightNavBarView.addSubview(listLbl)
                
                let listTapGue = UITapGestureRecognizer()
                listTapGue.addTarget(self, action: #selector(self.ufxSelectListPage))
                
                listLbl.isUserInteractionEnabled = true
                listLbl.addGestureRecognizer(listTapGue)
                
                mapLbl = MyLabel(frame: CGRect(x: 75, y:0, width: 75, height: 40))
                mapLbl.font = UIFont(name: "Roboto-Medium", size: 19)!
                mapLbl.textAlignment = .center
                mapLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_MAP_TXT")
                mapLbl.textColor = UIColor.UCAColor.AppThemeTxtColor
                rightNavBarView.addSubview(mapLbl)
                
                let mapTapGue = UITapGestureRecognizer()
                mapTapGue.addTarget(self, action: #selector(self.ufxSelectMapPage))
                
                mapLbl.isUserInteractionEnabled = true
                mapLbl.addGestureRecognizer(mapTapGue)
                
                if(self.ufxSelectedLatitude != "" && self.ufxSelectedLongitude != ""){
                    
                    let ufxPickLoc = CLLocation(latitude: GeneralFunctions.parseDouble(origValue: 0.0, data: self.ufxSelectedLatitude), longitude: GeneralFunctions.parseDouble(origValue: 0.0, data: self.ufxSelectedLongitude))
                    
                    self.pickUpLocation = ufxPickLoc
                    let camera = GMSCameraPosition.camera(withLatitude: ufxPickLoc.coordinate.latitude,
                                                          longitude: ufxPickLoc.coordinate.longitude, zoom: self.gMapView.camera.zoom)
                    
                    self.gMapView.moveCamera(GMSCameraUpdate.setCamera(camera))
                }
            
                
                
                
                self.ufxSelectListPage()
                
                self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: rightNavBarView)
                
            }
            
            if(self.userProfileJson.get("APP_TYPE").uppercased() == "DELIVERY"){
                self.currentCabGeneralType = Utils.cabGeneralType_Deliver
            }else if(self.userProfileJson.get("APP_TYPE").uppercased() == "UBERX"){
                self.currentCabGeneralType = Utils.cabGeneralType_UberX
            }
            
            
            getAddressFrmLocation.setLocation(latitude: pickUpLocation!.coordinate.latitude, longitude: pickUpLocation!.coordinate.longitude)
            getAddressFrmLocation.setPickUpMode(isPickUpMode: isPickUpMode)
            getAddressFrmLocation.executeProcess(isOpenLoader: false, isAlertShow:false)
            
            addDriverNotificationObserverForRideLater()
            
            /* add Recent Location View if APP_TYPE != 'UFX'*/
            
            if(self.userProfileJson.get("APP_TYPE").uppercased() != Utils.cabGeneralType_UberX){
                //                defaultRecentLocationViewHeight = 50
                ////                + (Application.statusBarHeight > Application.statusBarHeight ? Application.statusBarHeight : 0)
                //                self.setPrefImgViewBottomMargin.constant = self.setPrefImgViewBottomMargin.constant + defaultRecentLocationViewHeight
                //                self.myLocBottomMargin.constant = self.myLocBottomMargin.constant + defaultRecentLocationViewHeight
                //
                //                recentLocView = RecentLocationView(frame: CGRect(x:0, y:0, width:0, height: 0))
                //                recentLocView.mainScreenUV = self
                //                recentLocView.initializeDragView(dragViewAnimatedTopSpace: self.addressContainerView.frame.origin.y + self.addressContainerView.frame.size.height + 20, viewDefaultHeightConstant: defaultRecentLocationViewHeight, containerView: self.view)
                //                self.view.addSubview(recentLocView)
                
            }
            
        }
    }
    
    func ufxSelectListPage(){
        
        if(self.requestPickUpView != nil){
            return
        }
        
        listLbl.textColor = UIColor.UCAColor.AppThemeColor
        listLbl.backgroundColor = UIColor.UCAColor.AppThemeTxtColor
        
        mapLbl.textColor = UIColor.UCAColor.AppThemeTxtColor
        mapLbl.backgroundColor = UIColor.clear
        
//        self.ufxNoProviderView.isHidden = false
        
       
        
        if(providerDetailMarkerView != nil){
            providerDetailMarkerView.closeViewTapped()
        }
        
        
        configUFXView(choiceType: Utils.Cab_UberX_Type_List, autoLoad: false)
        
        self.addressContainerView.isHidden = true
    }
    
    func ufxSelectMapPage(){
        
        if(self.requestPickUpView != nil){
            return
        }
        
        listLbl.textColor = UIColor.UCAColor.AppThemeTxtColor
        listLbl.backgroundColor = UIColor.clear
        
        mapLbl.textColor = UIColor.UCAColor.AppThemeColor
        mapLbl.backgroundColor = UIColor.UCAColor.AppThemeTxtColor
        
        
//        self.ufxNoProviderView.isHidden = true
        
        configUFXView(choiceType: Utils.Cab_UberX_Type_Map, autoLoad: false)
        
        self.addressContainerView.isHidden = true

        if(self.currentLoadedDriverList.count == 0){
            //                    self.ufxNoProviderView.isHidden = false
            
            
            
        }
    }
    
    func configUFXView(choiceType:String, autoLoad:Bool){
        if(autoLoad == true && self.UFX_PAGE_MODE == Utils.Cab_UberX_Type_Map){
            return
        }
        
        if(choiceType == Utils.Cab_UberX_Type_List){
            self.ufxTableView.isHidden = false
            
            if(ufxLoaderView == nil){
                ufxLoaderView =  self.generalFunc.addMDloader(contentView: self.contentView)
                ufxLoaderView.frame.size = CGSize(width: Application.screenSize.width, height: 150)
                
                ufxLoaderView.center = CGPoint(x: Application.screenSize.width / 2, y: (Application.screenSize.height / 2) - (ufxLoaderView.frame.height / 2))

                let searchLbl = MyLabel(frame: CGRect(x: 20, y: 90, width: Application.screenSize.width - 10, height: 50))
                
                
                searchLbl.center = CGPoint(x: Application.screenSize.width / 2, y: searchLbl.center.y)
                
                searchLbl.numberOfLines = 2
                searchLbl.textAlignment = .center
//                searchLbl.text = "Searching for Provider"
                ufxLoaderView.addSubview(searchLbl)
                ufxLoaderView.backgroundColor = UIColor.clear
            }
            
        }else{
            self.ufxTableView.isHidden = true
            
            self.ufxLoaderView.isHidden = true
        }
        self.UFX_PAGE_MODE = choiceType
        
        if(choiceType == Utils.Cab_UberX_Type_List){
            
            
            self.ufxCurrentLoadedDriverList.removeAll()
            self.ufxLoaderView.isHidden = false
//            self.ufxNoProviderView.isHidden = true
            self.ufxTableView.reloadData()
            
//            self.ufxSearchIndicator.hidden = false
            (self.ufxLoaderView.subviews[1] as! MyLabel).text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_SEARCH_CAR_WAIT_TXT")
            
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(4 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {
                
                self.ufxCurrentLoadedDriverList.removeAll()
                self.ufxCurrentLoadedDriverList.append(contentsOf: self.currentLoadedDriverList)
                self.ufxTableView.reloadData()
                
                if(self.ufxCurrentLoadedDriverList.count == 0){
//                    self.ufxNoProviderView.isHidden = false
                    if(self.UFX_PAGE_MODE == choiceType){
                        
                       
                    }
                    
                    self.ufxLoaderView.isHidden = true
                }
                
                self.ufxLoaderView.isHidden = true
                
            })

        }
    }
    
    func addressViewTapped(sender:UITapGestureRecognizer){
        
        if(pickUpAddViewFrame == nil){
            pickUpAddViewFrame = addressContainerView.subviews[1].frame
        }
        
        if(destAddViewFrame == nil){
            destAddViewFrame = addressContainerView.subviews[0].frame
        }
        
        if(isPickUpMode == false){
            
            if(sender.view!.tag == 0 && isPickUpMode == false){
                if(self.isDriverAssigned == true && self.destLocation != nil){
                }else{
                    let addDestinationUv = GeneralFunctions.instantiateViewController(pageName: "AddDestinationUV") as! AddDestinationUV
                    addDestinationUv.centerLocation = self.pickUpLocation
                    addDestinationUv.SCREEN_TYPE = "DESTINATION"
                    self.pushToNavController(uv: addDestinationUv)
                }
                
                return
            }
            
            addressContainerView.subviews[0].layer.zPosition = -1
            addressContainerView.subviews[1].layer.zPosition = 1
            
            addressContainerView.subviews[0].frame.size = CGSize(width: Application.screenSize.width - 60, height: destAddViewFrame.height)
            addressContainerView.subviews[0].layoutIfNeeded()
            addressContainerView.subviews[0].setNeedsLayout()
            
            addressContainerView.subviews[1].frame.size = CGSize(width: Application.screenSize.width - 60 - 25, height: pickUpAddViewFrame.height)
            addressContainerView.subviews[1].layoutIfNeeded()
            addressContainerView.subviews[1].setNeedsLayout()
            addressContainerView.layoutSubviews()

            isPickUpMode = true
            
        }else{
            
            if(sender.view!.tag == 1 && isPickUpMode == true){
                if(self.isDriverAssigned == false){
                    
                    let addDestinationUv = GeneralFunctions.instantiateViewController(pageName: "AddDestinationUV") as! AddDestinationUV
                    addDestinationUv.centerLocation = self.pickUpLocation
                    addDestinationUv.SCREEN_TYPE = "PICKUP"
                    self.pushToNavController(uv: addDestinationUv)
                }
                return
            }
            
            addressContainerView.subviews[0].frame.size = CGSize(width: Application.screenSize.width - 60 - 25, height: pickUpAddViewFrame.height)
            addressContainerView.subviews[0].layoutIfNeeded()
            addressContainerView.subviews[0].setNeedsLayout()
            
            addressContainerView.subviews[1].frame.size = CGSize(width: Application.screenSize.width - 60, height: destAddViewFrame.height)
            addressContainerView.subviews[1].layoutIfNeeded()
            addressContainerView.subviews[1].setNeedsLayout()
            
            addressContainerView.subviews[0].layer.zPosition = 1
            addressContainerView.subviews[1].layer.zPosition = -1

            addressContainerView.layoutSubviews()
            isPickUpMode = false
        }
        
    }
    
    func isCarTypesArrChanged(carTypes:NSArray) -> Bool{
        
        if(self.cabTypesArr.count != carTypes.count){
            return true
        }
        
        for i in 0..<self.cabTypesArr.count{
            let tempDict = self.cabTypesArr[i]
            let newTempDict = carTypes[i] as! NSDictionary
            
            let iVehicleTypeId = tempDict.get("iVehicleTypeId")
            let newVehicleTypeId = newTempDict.get("iVehicleTypeId")
            
            if(iVehicleTypeId != newVehicleTypeId){
                return true
            }
            
        }
        
        return false
    }
    
    func getFirstCarTypeID() -> String{
        var iVehicleTypeId = ""
//        let carTypesArr = userProfileJson!.getArrObj("VehicleTypes")
        
        if(self.currentCabGeneralType.uppercased() == Utils.cabGeneralType_UberX.uppercased()){
            return self.ufxSelectedVehicleTypeId
        }
        
        for i in 0..<cabTypesArr.count{
            let tempDict = cabTypesArr[i]
            
//            if(tempDict.get("eType") == self.userProfileJson.get("APP_TYPE")){

                iVehicleTypeId = tempDict.get("iVehicleTypeId")
                
                break
//            }
            
        }
        
        return iVehicleTypeId
    }

    func notifyCarSearching(){
        
       
        
        setETA(time: "--")
    }
    
    func notifyNoCabs(){
       
        if(self.currentCabGeneralType.uppercased() == Utils.cabGeneralType_UberX.uppercased()){
            if(self.UFX_PAGE_MODE == Utils.Cab_UberX_Type_List){
                configUFXView(choiceType: Utils.Cab_UberX_Type_List, autoLoad: true)
            }else{
                ufxSelectMapPage()
            }
        }
        setETA(time: "--")
    }
    
    func notifyCabsAvailable(){
        
        
    }
    
    func setCurrentLoadedDriverList(currentLoadedDriverList: [NSDictionary]){
        self.currentLoadedDriverList = currentLoadedDriverList
    }
    
    func getDriverLocationChannelList() -> [String]{
        
        var subscribeChannelList = [String]()
        
        for i in 0..<currentLoadedDriverList.count{
            subscribeChannelList += [Utils.PUBNUB_UPDATE_LOC_CHANNEL_PREFIX_DRIVER + "\(currentLoadedDriverList[i].get("driver_id"))"]
        }
        
        return subscribeChannelList
    }
    
    func getDriverLocationChannelList(listData:[NSDictionary]) -> [String]{
        
        var channels_update_loc = [String]()
        
        for i in 0..<listData.count{
            
            channels_update_loc += [Utils.PUBNUB_UPDATE_LOC_CHANNEL_PREFIX_DRIVER + "\(listData[i].get("driver_id"))"]
            
        }
        
        return channels_update_loc
    }
    
    func getDriverMarkerOnPubNubMsg(iDriverId:String, isRemoveFromList:Bool) -> GMSMarker?{
        if(loadAvailableCab == nil){
            return nil
        }
        var driverMarkerList = self.loadAvailableCab.getDriverMarkerList()
        
        for i in 0..<driverMarkerList.count{
            let marker = driverMarkerList[i]
            let driver_id = marker.title?.replace("DriverId", withString: "")
            if(driver_id == iDriverId){
                if(isRemoveFromList){
                    driverMarkerList.remove(at: i)
                    self.loadAvailableCab.setDriverMarkerList(driverMarkerList: driverMarkerList)
                }
                
                return marker
            }
        }
        
        return nil
    }
    
    func setETA(time:String){
        Utils.printLog(msgData: "Time:\(time)")
        if(sourcePickUpEtaLbl != nil){
            sourcePickUpEtaLbl.text = time
        }
    }
    
    func openRequestPickUpView(isFromRideLater:Bool){
        
        self.addressContainerView.isHidden = false
        
        estimateFare()
        sourcePinImgView.isHidden = false
        requestPickUpView = self.generalFunc.loadView(nibName: "RequestPickUpBottomView", uv: self, isWithOutSize: true)
        
        var height:CGFloat = 255
        if(self.currentCabGeneralType.uppercased() == Utils.cabGeneralType_UberX.uppercased()){
            height = height - 134
        }
        
        requestPickUpView.frame = CGRect(x: 0, y: self.view.frame.size.height + height, width: Application.screenSize.width, height: height)
        self.cardPaymentView.isHidden = true
//        requestPickUpView.center = CGPoint(x: self.view.bounds.midX, y: Application.screenSize.height - 310)
        self.view.addSubview(requestPickUpView)
        
        UIView.animate(withDuration: 0.8,
                       animations: {
//                        self.requestPickUpView.center = CGPoint(x: 0, y: 310)
                        self.requestPickUpView.frame.origin.y = self.view.frame.size.height - height
//                        self.myLocImgView.frame.origin.y = self.myLocImgView.frame.origin.y - height
                        self.myLocBottomMargin.constant = self.myLocBottomMargin.constant + height - self.defaultRecentLocationViewHeight
                        self.view.layoutIfNeeded()
        },  completion: { finished in
            
        })
        
//        if(Configurations.isRTLMode()){
//            var scalingTransform : CGAffineTransform!
//            scalingTransform = CGAffineTransform(scaleX: -1, y: 1);
//            cabTypeCollectionView.transform = scalingTransform
//        }
        
//         self.cabTypesArr.removeAll()
//        let cabTypesArr = userProfileJson!.getArrObj("VehicleTypes")
//        
//        for i in 0..<cabTypesArr.count {
//            let tempItem = cabTypesArr[i] as! NSDictionary
//            
//            if(tempItem.get("eType") == self.userProfileJson.get("APP_TYPE")){
//                self.cabTypesArr += [tempItem]
//            }
//        }
        
        self.cashPayLbl.text = self.generalFunc.getLanguageLabel(origValue: "Pay by cash", key: "LBL_PAY_BY_CASH_TXT")
        self.cardPayLbl.text = self.generalFunc.getLanguageLabel(origValue: "Pay by card", key: "LBL_PAY_BY_CARD_TXT")
        
        self.promoCodeLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_PRMO_TXT")
        self.noCabTypeLbl.text = self.generalFunc.getLanguageLabel(origValue: "No aervice available in your selected pickup location.", key: "LBL_NO_SERVICE_AVAILABLE_TXT")
        self.noCabTypeLbl.fitText()
        self.noCabTypeLbl.isHidden = true
        
        if(self.cabTypesArr.count < 1){
            self.noCabTypeLbl.isHidden = false
        }
        
        let promoTapGue = UITapGestureRecognizer()
        promoTapGue.addTarget(self, action: #selector(self.promoTapped))
        
        self.promoCodeView.addGestureRecognizer(promoTapGue)
        
        self.isCardValidated = false
        self.isCashPayment = true
        self.isDeliveryDataEntered = false
        
        self.receiverName = ""
        self.receiverMobile = ""
        self.pickUpIns = ""
        self.deliveryIns = ""
        self.packageTypeId = ""
        self.packageDetails = ""
        
        if(self.userProfileJson.get("APP_PAYMENT_MODE") == "Cash-Card"){
            self.payView.tag = 0
            self.payView.isUserInteractionEnabled = true
            self.payView.addGestureRecognizer(getPayTapGue())
            
            self.cashPaymentView.tag = 1
            self.payView.isUserInteractionEnabled = true
            self.cashPaymentView.addGestureRecognizer(getPayTapGue())
            
            self.cardPaymentView.tag = 2
            self.payView.isUserInteractionEnabled = true
            self.cardPaymentView.addGestureRecognizer(getPayTapGue())
        }else if(self.userProfileJson.get("APP_PAYMENT_MODE") == "Card"){
            isCashPayment = false
            self.payLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_CARD")
            self.payImgView.image = UIImage(named: "ic_card_new")
        }else{
            isCashPayment = true
            self.payLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_CASH_TXT")
            self.payImgView.image = UIImage(named: "ic_cash_new")
        }

        if(self.currentCabGeneralType == Utils.cabGeneralType_Deliver){
            self.requestNowBtn.setButtonTitle(buttonTitle: self.generalFunc.getLanguageLabel(origValue: "Next", key: "LBL_BTN_NEXT_TXT"))
        }else{
            self.requestNowBtn.setButtonTitle(buttonTitle: self.generalFunc.getLanguageLabel(origValue: "Request Now", key: "LBL_REQUEST_NOW"))
        }

        self.requestNowBtn.setButtonTitleColor(color: UIColor.UCAColor.AppThemeTxtColor)
        
        self.cabTypeCollectionView.register(UINib(nibName: "CabTypeCVCell", bundle: nil), forCellWithReuseIdentifier: "CabTypeCVCell")
        self.cabTypeCollectionView.dataSource = self
        self.cabTypeCollectionView.delegate = self
        self.cabTypeCollectionView.reloadData()
        
        self.menuImgView.image = UIImage(named: "ic_nav_bar_back")!
        GeneralFunctions.setImgTintColor(imgView: menuImgView, color: UIColor(hex: 0x000000))
        
        let rideLaterTapGue = UITapGestureRecognizer()
        
        rideLaterTapGue.addTarget(self, action: #selector(self.rideLaterTapped))
        
        rideLaterImgView.isUserInteractionEnabled = true
        rideLaterImgView.addGestureRecognizer(rideLaterTapGue)
        
        self.requestNowBtn.clickDelegate = self
        
        if(currentLoadedDriverList.count == 0){
            
            self.requestNowBtn.setButtonEnabled(isBtnEnabled: false)
            self.requestNowBtn.setButtonTitleColor(color: UIColor(hex: 0x6b6b6b))
        }
        
        if(isFromRideLater == true){
            rideLaterImgView.isHidden = true
            self.rideLaterBtn.isHidden = false
            self.rideLaterBtn.clickDelegate = self
            self.rideLaterBtn.setButtonTitle(buttonTitle: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_CONFIRM_BOOKING"))
        }
        
        if(self.userProfileJson.get("ENABLE_TOLL_COST").uppercased() != "YES" || self.currentCabGeneralType.uppercased() == Utils.cabGeneralType_UberX.uppercased()){
            self.isTollChecked = true
        }
        
        if(self.currentCabGeneralType.uppercased() == Utils.cabGeneralType_UberX.uppercased()){
            self.cabTypeCollectionView.isHidden = true
            self.cabTypeContainerViewHeight.constant = 0
            self.noCabTypeLbl.isHidden = true
        }
        
        
    }
    
    
    func estimateFare(){
        self.cabTypesFareArr.removeAll()
        if(cabTypeCollectionView != nil){
            self.cabTypeCollectionView.reloadData()
        }
        
        
        let destLoc = self.destLocation != nil ? self.destLocation : self.pickUpLocation
        
        let directionURL = "https://maps.googleapis.com/maps/api/directions/json?origin=\(self.pickUpLocation!.coordinate.latitude),\(self.pickUpLocation!.coordinate.longitude)&destination=\(destLoc!.coordinate.latitude),\(destLoc!.coordinate.longitude)&key=\(Configurations.getInfoPlistValue(key: "GOOGLE_SERVER_KEY"))&language=\(Configurations.getGoogleMapLngCode())&sensor=true"
        
        let exeWebServerUrl = ExeServerUrl(dict_data: [String:String](), currentView: self.view, isOpenLoader: false)
        
        exeWebServerUrl.executeGetProcess(completionHandler: { (response) -> Void in
            
            if(response != ""){
                let dataDict = response.getJsonDataDict()
                
                if(dataDict.get("status").uppercased() != "OK" || dataDict.getArrObj("routes").count == 0){
                    return
                }
                
                
                let routesArr = dataDict.getArrObj("routes")
                let legs_arr = (routesArr.object(at: 0) as! NSDictionary).getArrObj("legs")
                let duration = (legs_arr.object(at: 0) as! NSDictionary).getObj("duration").get("value")
                let distance = (legs_arr.object(at: 0) as! NSDictionary).getObj("distance").get("value")
                
                self.continueEstimateFare(distance: distance, time: duration)
                
            }else{
                //                self.generalFunc.setError(uv: self)
            }
        }, url: directionURL)
        
        
        
    }
    
    func continueEstimateFare(distance:String, time:String){
        let parameters = ["type":"estimateFareNew","SelectedCar": self.selectedCabTypeId, "distance": distance, "time": time, "iUserId": GeneralFunctions.getMemberd(), "PromoCode": self.appliedPromoCode]
        
        let exeWebServerUrl = ExeServerUrl(dict_data: parameters, currentView: self.view, isOpenLoader: false)
        exeWebServerUrl.executePostProcess(completionHandler: { (response) -> Void in
            
            self.cabTypesFareArr.removeAll()
            if(self.cabTypeCollectionView != nil){
                self.cabTypeCollectionView.reloadData()
            }
            
            if(response != ""){
                let dataDict = response.getJsonDataDict()
                
                if(dataDict.get("Action") == "1"){
                    
                    let cabTypesArr = dataDict.getArrObj(Utils.message_str)
                    
                    for i in 0..<cabTypesArr.count {
                        let tempItem = cabTypesArr[i] as! NSDictionary

                        if(((tempItem.get("eType") == Utils.cabGeneralType_Ride) && (self.userProfileJson.get("APP_TYPE").uppercased() == "RIDE-DELIVERY" || self.userProfileJson.get("APP_TYPE").uppercased() == "RIDE")) ||
                            ((tempItem.get("eType") == Utils.cabGeneralType_Deliver) && (self.userProfileJson.get("APP_TYPE").uppercased() == "RIDE-DELIVERY" || self.userProfileJson.get("APP_TYPE").uppercased() == "DELIVERY"))){
                            self.cabTypesFareArr += [tempItem]
                        }
                    }
                    
                    if(self.cabTypeCollectionView != nil){
                        self.cabTypeCollectionView.reloadData()
                    }
                    
                }else{
                    
                }
                
            }else{
                //                self.generalFunc.setError(uv: self)
            }
        })
    }
    
    
    func getCabTypeFare(currentVehicleTypeId:String) -> String{
    
        for i in 0..<cabTypesFareArr.count {
            
            let tempDict = cabTypesFareArr[i]
            let iVehicleTypeId = tempDict.get("iVehicleTypeId")
            
            if(currentVehicleTypeId == iVehicleTypeId){
                return tempDict.get("total_fare")
            }
        }
        
        return ""
    }
    
    
    func rideLaterTapped(){
        if((self.userProfileJson.get("APP_PAYMENT_MODE") == "Card" && isCardValidated == false) || (isCashPayment == false && isCardValidated == false)){
            checkCardConfig(isRideLater: true, isAutoContinue: true)
            return
        }

        let minDate = Calendar.current.date(byAdding: .hour, value: 1, to: Date())
        let maxDate = Calendar.current.date(byAdding: .month, value: 1, to: Date())
        
        DatePickerDialog().show(self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_BOOKING_DATE"), doneButtonTitle: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_SELECT_TXT"), cancelButtonTitle: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_CANCEL_TXT"), minimumDate: minDate,maximumDate: maxDate, datePickerMode: .dateAndTime) {
            (date) -> Void in
            
            if(date != nil){
                let dateFormatter = DateFormatter()
                dateFormatter.locale = Locale(identifier: "en_GB")
                dateFormatter.dateFormat = "YYYY-MM-dd HH:mm"
                let dateString = dateFormatter.string(from: date!)
                
                self.selectedDate = dateString
                self.selectedTimeZone = dateFormatter.timeZone.identifier
                
                self.checkSurgePrice(selectedTime: dateString, isRideLater:true)
            }
        }
    }
    
    func checkSurgePrice(selectedTime:String, isRideLater:Bool){
        if(pickUpAddGeoCodeResult == "" || (userProfileJson.get("APP_DESTINATION_MODE").uppercased() == "STRICT" && destAddGeoCodeResult == "") || (self.currentCabGeneralType != Utils.cabGeneralType_UberX && destAddGeoCodeResult == "") ){
            return
        }
        
        if(self.currentCabGeneralType == Utils.cabGeneralType_UberX){
            
            
            if(self.requestPickUpView == nil){
                self.loadAvailableCab.filterDrivers(isCheckAgain: false)
                self.ufxSelectMapPage()
                self.openRequestPickUpView(isFromRideLater: isRideLater)
            }else{
                if(isRideLater == false){
                    
                    self.requestCab(tollPrice: "", tollPriceCurrencyCode: "", isTollSkipped: "")
                    
                }else{
                    self.continueRideLaterSchedule(tollPrice: "", tollPriceCurrencyCode: "", isTollSkipped: "")
                    
                }
            }
            
            return
        }
        
        let parameters = ["type":"checkSurgePrice","SelectedCarTypeID": self.selectedCabTypeId, "SelectedTime": selectedTime]
//        , "TimeZone": selectedTimeZone
        let exeWebServerUrl = ExeServerUrl(dict_data: parameters, currentView: self.view, isOpenLoader: true)
        exeWebServerUrl.executePostProcess(completionHandler: { (response) -> Void in
            
            if(response != ""){
                let dataDict = response.getJsonDataDict()
                
                if(dataDict.get("Action") == "1"){
                    
                    if(isRideLater == false){
                        
                        self.requestCab(tollPrice: "", tollPriceCurrencyCode: "", isTollSkipped: "")
                        
//                        self.sendRequestToDrivers(driverIds: self.getAvailableDriverIds())
                    }else{
                        
                        self.continueRideLaterSchedule(tollPrice: "", tollPriceCurrencyCode: "", isTollSkipped: "")
                        
                    }
                    
                }else{
                    
                    self.openSurgeConfirmDialog(isRideLater: isRideLater, dataDict: dataDict)
                }
                
            }else{
                self.generalFunc.setError(uv: self)
            }
        })
    }
    
    func openSurgeConfirmDialog(isRideLater:Bool, dataDict:NSDictionary){
    
        surgePriceView = self.generalFunc.loadView(nibName: "SurgePriceView", uv: self, isWithOutSize: true)
        
        let width = Application.screenSize.width  > 390 ? 375 : Application.screenSize.width - 50
        
        surgePriceView.frame.size = CGSize(width: width, height: 250)
        
        
        surgePriceView.center = CGPoint(x: self.contentView.bounds.midX, y: self.contentView.bounds.midY)
        
        surgePriceBGView = UIView()
        surgePriceBGView.backgroundColor = UIColor.black
        surgePriceBGView.alpha = 0.4
        surgePriceBGView.isUserInteractionEnabled = true
        
        let bgViewTapGue = UITapGestureRecognizer()
        surgePriceBGView.frame = self.contentView.frame
        
        surgePriceBGView.center = CGPoint(x: self.contentView.bounds.midX, y: self.contentView.bounds.midY)
        
        bgViewTapGue.addTarget(self, action: #selector(self.cancelSurgeView))
        
        surgePriceBGView.addGestureRecognizer(bgViewTapGue)
        
        surgePriceView.layer.shadowOpacity = 0.5
        surgePriceView.layer.shadowOffset = CGSize(width: 0, height: 3)
        surgePriceView.layer.shadowColor = UIColor.black.cgColor
        
        
        self.view.addSubview(surgePriceBGView)
        self.view.addSubview(surgePriceView)
        
        let cancelSurgeTapGue = UITapGestureRecognizer()
        cancelSurgeTapGue.addTarget(self, action: #selector(self.cancelSurgeView))
        
        surgeLaterLbl.isUserInteractionEnabled = true
        surgeLaterLbl.addGestureRecognizer(cancelSurgeTapGue)
        
        self.surgePayAmtLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_PAYABLE_AMOUNT")
        self.surgeLaterLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_TRY_LATER")
        self.surgePriceVLbl.text = Configurations.convertNumToAppLocal(numStr: dataDict.get("SurgePrice"))
        self.surgePriceHLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: dataDict.get(Utils.message_str))
        self.surgeAcceptBtn.setButtonTitle(buttonTitle: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_ACCEPT_SURGE"))
        self.surgeAcceptBtn.clickDelegate = self
        
        self.isRideLaterFromSurge = isRideLater
    }
    
    
    func cancelSurgeView(){
        surgePriceView.removeFromSuperview()
        surgePriceBGView.removeFromSuperview()
    }
    
    
    func promoTapped(){
        
        promoCodeDialogView = self.generalFunc.loadView(nibName: "EnterPromoCodeView", uv: self, isWithOutSize: true)
        
        let width = Application.screenSize.width  > 390 ? 375 : Application.screenSize.width - 50
        
        promoCodeDialogView.frame.size = CGSize(width: width, height: 200)
        
        
        promoCodeDialogView.center = CGPoint(x: self.contentView.bounds.midX, y: self.contentView.bounds.midY)
        
        promoCodeBGDialogView = UIView()
        promoCodeBGDialogView.backgroundColor = UIColor.black
        promoCodeBGDialogView.alpha = 0.4
        promoCodeBGDialogView.isUserInteractionEnabled = true
        
        let bgViewTapGue = UITapGestureRecognizer()
        promoCodeBGDialogView.frame = self.contentView.frame
        
        promoCodeBGDialogView.center = CGPoint(x: self.contentView.bounds.midX, y: self.contentView.bounds.midY)
        
        bgViewTapGue.addTarget(self, action: #selector(self.closePromoView))
        
        promoCodeBGDialogView.addGestureRecognizer(bgViewTapGue)
        
        promoCodeDialogView.layer.shadowOpacity = 0.5
        promoCodeDialogView.layer.shadowOffset = CGSize(width: 0, height: 3)
        promoCodeDialogView.layer.shadowColor = UIColor.black.cgColor
        
        
        self.view.addSubview(promoCodeBGDialogView)
        self.view.addSubview(promoCodeDialogView)
        
        let cancelPromoTapGue = UITapGestureRecognizer()
        cancelPromoTapGue.addTarget(self, action: #selector(self.closePromoView))
        
        cancelPromoLbl.isUserInteractionEnabled = true
        cancelPromoLbl.addGestureRecognizer(cancelPromoTapGue)
        
        let confirmPromoTapGue = UITapGestureRecognizer()
        confirmPromoTapGue.addTarget(self, action: #selector(self.checkPromoCode))
        
        confirmPromoLbl.isUserInteractionEnabled = true
        confirmPromoLbl.addGestureRecognizer(confirmPromoTapGue)
        
        self.promoViewHLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_PROMO_CODE_ENTER_TITLE")
        self.cancelPromoLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_CANCEL_TXT")
        self.confirmPromoLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_BTN_OK_TXT")
        
        Utils.createRoundedView(view: promoCodeView, borderColor: UIColor.clear, borderWidth: 0, cornerRadius: 10)
        
        if(self.appliedPromoCode != ""){
            self.promoTxtField.setText(text: self.appliedPromoCode)
        }
    }
    
    func closePromoView(){
        if(promoCodeBGDialogView != nil){
            promoCodeBGDialogView.removeFromSuperview()
            promoCodeBGDialogView = nil
        }
        
        if(promoCodeDialogView != nil){
            promoCodeDialogView.removeFromSuperview()
            promoCodeDialogView = nil
        }
        
    }
    
    func checkPromoCode(){
        let required_str = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_FEILD_REQUIRD_ERROR_TXT")
        
        if(self.appliedPromoCode != "" && Utils.getText(textField: self.promoTxtField.getTextField()!) == ""){
            closePromoView()
             self.generalFunc.setError(uv: self, title: "", content: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_PROMO_REMOVED"))
            return
        }
        
        let promoEntered = Utils.checkText(textField: self.promoTxtField.getTextField()!) ? (Utils.getText(textField: self.promoTxtField.getTextField()!).contains(" ") ? Utils.setErrorFields(textField: self.promoTxtField.getTextField()!, error: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_PROMO_INVALIED")) : true) : Utils.setErrorFields(textField: self.promoTxtField.getTextField()!, error: required_str)
        
        if(promoEntered){
            applyPromoCodeView(appliedPromoCode: Utils.getText(textField: self.promoTxtField.getTextField()!))
        }
    }
    
    func applyPromoCodeView(appliedPromoCode:String){
    
        closePromoView()
        
        let parameters = ["type":"CheckPromoCode","PromoCode": appliedPromoCode, "iUserId": GeneralFunctions.getMemberd()]
        
        let exeWebServerUrl = ExeServerUrl(dict_data: parameters, currentView: self.view, isOpenLoader: true)
        exeWebServerUrl.executePostProcess(completionHandler: { (response) -> Void in
            
            if(response != ""){
                let dataDict = response.getJsonDataDict()
                self.appliedPromoCode = ""
                if(dataDict.get("Action") == "1"){
                    
                   self.appliedPromoCode = appliedPromoCode
                    self.generalFunc.setError(uv: self, title: "", content: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_PROMO_APPLIED"))
                    if(self.requestPickUpView != nil){
                        self.cabTypesFareArr.removeAll()
                        self.cabTypeCollectionView.reloadData()
                        self.estimateFare()
                    }
                    
                }else if(dataDict.get("Action") == "01"){
                    self.generalFunc.setError(uv: self, title: "", content: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_PROMO_USED"))
                    
                }else{
                    self.generalFunc.setError(uv: self, title: "", content: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_PROMO_INVALIED"))
//                    self.generalFunc.setError(uv: self, title: "", content: self.generalFunc.getLanguageLabel(origValue: "", key: dataDict.get("message")))
                }
                
            }else{
                self.generalFunc.setError(uv: self)
            }
        })
    }
    
    func getPayTapGue() -> UITapGestureRecognizer{
        let payOptionTapGue = UITapGestureRecognizer()
        payOptionTapGue.addTarget(self, action: #selector(self.paymentOptionTapped(sender:)))
        
        return payOptionTapGue
    }
    
    func paymentOptionTapped(sender:UITapGestureRecognizer){
        
        if(sender.view!.tag == 0){
            self.requestPickUpView.frame.size = CGSize(width: self.requestPickUpView.frame.width, height: requestPickUpView.frame.height + 40)
            self.requestPickUpView.frame.origin.y = self.view.frame.height - requestPickUpView.frame.height
            
            UIView.animate(withDuration: 0.8,
                           animations: {
                            //                        self.requestPickUpView.center = CGPoint(x: 0, y: 310)
                            self.paymentOptionContainerViewHeight.constant = self.paymentOptionContainerViewHeight.constant + 40
                            self.paymentOptionContainerView.frame.size = CGSize(width: self.paymentOptionContainerView.frame.width, height: self.paymentOptionContainerView.frame.height + 40)
                            
                            self.paymentContainerView.frame.size = CGSize(width: self.paymentOptionContainerView.frame.width, height: self.paymentOptionContainerView.frame.height + 40)
                            
                            self.cardPaymentView.isHidden = false
                            self.cashPaymentView.isHidden = false
                            self.payView.isHidden = true
                            
                            self.payViewHeight.constant = 0
                            self.seperaterViewXPostion.constant = Configurations.isRTLMode() ? (self.seperaterViewXPostion.constant - Application.screenSize.width) : (self.seperaterViewXPostion.constant + Application.screenSize.width)
                            
                            self.promoCodeView.isHidden = true
                            
            },  completion: { finished in
                
            })
        }else{
            self.requestPickUpView.frame.size = CGSize(width: self.requestPickUpView.frame.width, height: (requestPickUpView.frame.height - 40))
            self.requestPickUpView.frame.origin.y = self.view.frame.height - requestPickUpView.frame.height
            
            UIView.animate(withDuration: 0.8,
                           animations: {
                            //                        self.requestPickUpView.center = CGPoint(x: 0, y: 310)
                            self.paymentOptionContainerViewHeight.constant = self.paymentOptionContainerViewHeight.constant - 40
                            self.paymentOptionContainerView.frame.size = CGSize(width: self.paymentOptionContainerView.frame.width, height: self.paymentOptionContainerView.frame.height - 40)
                            
                            self.paymentContainerView.frame.size = CGSize(width: self.paymentOptionContainerView.frame.width, height: self.paymentOptionContainerView.frame.height - 40)
                            
                            self.cardPaymentView.isHidden = true
                            self.cashPaymentView.isHidden = true
                            self.payView.isHidden = false
                            
                            self.payViewHeight.constant = 40
                            self.seperaterViewXPostion.constant = Configurations.isRTLMode() ? (self.seperaterViewXPostion.constant + Application.screenSize.width) : (self.seperaterViewXPostion.constant - Application.screenSize.width)
                            
                            self.promoCodeView.isHidden = false
                            
            },  completion: { finished in
                
            })
            
            if(sender.view!.tag == 1){
                isCashPayment = true
                
                self.payLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_CASH_TXT")
                self.payImgView.image = UIImage(named: "ic_cash_new")
                
                if(self.loadAvailableCab != nil){
                    self.loadAvailableCab.changeCabs()
                }
            }else{
//                isCashPayment = false
//                
//                self.payLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_CARD")
//                self.payImgView.image = UIImage(named: "ic_card_new")
                
                checkCardConfig(isRideLater: false, isAutoContinue: false)
            }
        }
        
        
    }
    
    func checkCardConfig(isRideLater:Bool, isAutoContinue:Bool){
        if(userProfileJson.get("vStripeCusId") == ""){
            let paymentUV = GeneralFunctions.instantiateViewController(pageName: "PaymentUV") as! PaymentUV
            (self.navigationDrawerController?.rootViewController as! UINavigationController).pushViewController(paymentUV, animated: true)
        }else{
            showPaymentBox(isRideLater:isRideLater, isAutoContinue: isAutoContinue)
        }
    }
    
    func changeCard(){
        closeConfirmCardView()
        let paymentUV = GeneralFunctions.instantiateViewController(pageName: "PaymentUV") as! PaymentUV
        (self.navigationDrawerController?.rootViewController as! UINavigationController).pushViewController(paymentUV, animated: true)
    }
    
    
    func showPaymentBox(isRideLater:Bool, isAutoContinue:Bool){
    
        isRideLater_payBox = isRideLater
        isAutoContinue_payBox = isAutoContinue
        
        confirmCardDialogView = self.generalFunc.loadView(nibName: "ConfirmCardView", uv: self, isWithOutSize: true)
        
        let width = Application.screenSize.width  > 390 ? 375 : Application.screenSize.width - 50
        
        confirmCardDialogView.frame.size = CGSize(width: width, height: 200)
        
        
        confirmCardDialogView.center = CGPoint(x: self.contentView.bounds.midX, y: self.contentView.bounds.midY)
        
        confirmCardBGDialogView = UIView()
        confirmCardBGDialogView.backgroundColor = UIColor.black
        confirmCardBGDialogView.alpha = 0.4
        confirmCardBGDialogView.isUserInteractionEnabled = true
        
        let bgViewTapGue = UITapGestureRecognizer()
        confirmCardBGDialogView.frame = self.contentView.frame
        
        confirmCardBGDialogView.center = CGPoint(x: self.contentView.bounds.midX, y: self.contentView.bounds.midY)
        
        bgViewTapGue.addTarget(self, action: #selector(self.closeConfirmCardView))
        
        confirmCardBGDialogView.addGestureRecognizer(bgViewTapGue)
        
        confirmCardDialogView.layer.shadowOpacity = 0.5
        confirmCardDialogView.layer.shadowOffset = CGSize(width: 0, height: 3)
        confirmCardDialogView.layer.shadowColor = UIColor.black.cgColor
        
        
        self.view.addSubview(confirmCardBGDialogView)
        self.view.addSubview(confirmCardDialogView)
        
        let cancelConfirmCardTapGue = UITapGestureRecognizer()
        cancelConfirmCardTapGue.addTarget(self, action: #selector(self.closeConfirmCardView))
        
        cancelCardLbl.isUserInteractionEnabled = true
        cancelCardLbl.addGestureRecognizer(cancelConfirmCardTapGue)
        
        let confirmCardTapGue = UITapGestureRecognizer()
        confirmCardTapGue.addTarget(self, action: #selector(self.checkCard))
        
        confirmCardLbl.isUserInteractionEnabled = true
        confirmCardLbl.addGestureRecognizer(confirmCardTapGue)
        
        let changeCardTapGue = UITapGestureRecognizer()
        changeCardTapGue.addTarget(self, action: #selector(self.changeCard))
        
        changeCardLbl.isUserInteractionEnabled = true
        changeCardLbl.addGestureRecognizer(changeCardTapGue)
        
        self.confirmCardHLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_TITLE_PAYMENT_ALERT")
        
        Utils.createRoundedView(view: promoCodeView, borderColor: UIColor.clear, borderWidth: 0, cornerRadius: 10)
        
        self.confirmCardLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_BTN_TRIP_CANCEL_CONFIRM_TXT")
        self.cancelCardLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_CANCEL_TXT")
        self.changeCardLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_CHANGE")
        
        self.confirmCardVLbl.text = self.userProfileJson.get("vCreditCard")
    }
    
    
    func closeConfirmCardView(){
        if(confirmCardBGDialogView != nil){
            confirmCardBGDialogView.removeFromSuperview()
            confirmCardBGDialogView = nil
        }
        
        if(confirmCardDialogView != nil){
            confirmCardDialogView.removeFromSuperview()
            confirmCardDialogView = nil
        }
    }
    
    func checkCard(){
        closeConfirmCardView()
        
        var parameters = ["type":"CheckCard", "iUserId": GeneralFunctions.getMemberd()]
        if GeneralFunctions.checkCardType() == "PayStack" {
            parameters["email"] = GeneralFunctions.getEmail()
        }
        
        let exeWebServerUrl = ExeServerUrl(dict_data: parameters, currentView: self.view, isOpenLoader: true)
        exeWebServerUrl.executePostProcess(completionHandler: { (response) -> Void in
            
            if(response != ""){
                let dataDict = response.getJsonDataDict()
                
                if(dataDict.get("Action") == "1"){
                    self.isCardValidated = true
                    self.isCashPayment = false
                    
                    self.payLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_CARD")
                    self.payImgView.image = UIImage(named: "ic_card_new")
                    
                    if(self.loadAvailableCab != nil){
                        self.loadAvailableCab.changeCabs()
                    }

                    if(self.isAutoContinue_payBox == true){
                        if(self.isRideLater_payBox == true){
                            self.rideLaterTapped()
                        }else{
                            self.myBtnTapped(sender: self.requestNowBtn)
                        }
                    }
                    
                }else{
                    self.generalFunc.setError(uv: self, title: "", content: self.generalFunc.getLanguageLabel(origValue: "", key: dataDict.get("message")))
                }
                
            }else{
                self.generalFunc.setError(uv: self)
            }
        })
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let iVehicleTypeId = self.cabTypesArr[indexPath.item].get("iVehicleTypeId")
        
        if(self.selectedCabTypeId == iVehicleTypeId){
            openFareInfoView(cabTypeItem: self.cabTypesArr[indexPath.item])
        }else{
            self.selectedCabTypeId = iVehicleTypeId
            self.loadAvailableCab!.setCabTypeId(selectedCabTypeId: iVehicleTypeId)
            self.loadAvailableCab!.changeCabs()
            collectionView.reloadData()
        }
        
    }
    
    
    func openFareInfoView(cabTypeItem: NSDictionary){
        self.menuImgView.isUserInteractionEnabled = false
        let height = Application.screenSize.height > 480 ? 450 : (Application.screenSize.height - 60)
        let fareDetailView = FareDetailView(frame: CGRect(x: 0, y: self.view.frame.height - height, width: Application.screenSize.width, height: height))
        
        fareDetailView.setViewHandler { (isViewClose, view, isMoreDetailTapped) in
            fareDetailView.frame.origin.y = Application.screenSize.height + height
            self.view.layoutIfNeeded()
            self.menuImgView.isUserInteractionEnabled = true
            
            if(isMoreDetailTapped){
                let fareBreakDownUv = GeneralFunctions.instantiateViewController(pageName: "FareBreakDownUV") as! FareBreakDownUV
                fareBreakDownUv.selectedCabTypeId = self.selectedCabTypeId
                fareBreakDownUv.pickUpLocation = self.pickUpLocation
                fareBreakDownUv.destLocation = self.destLocation
                fareBreakDownUv.promoCode = self.appliedPromoCode
                fareBreakDownUv.selectedCabTypeName = cabTypeItem.get("vVehicleType")
                self.pushToNavController(uv: fareBreakDownUv)
            }
        }
        
        
        self.view.addSubview(fareDetailView)
        
        
        fareDetailView.cabTypeNameLbl.text = cabTypeItem.get("vVehicleType")
        
        fareDetailView.doneBtn.setButtonTitle(buttonTitle: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_DONE"))
        
        let vLogo = cabTypeItem.get("vLogo")
        
        var vCarLogoHoverImg = ""
        if(UIScreen.main.scale < 2){
            vCarLogoHoverImg = "1x_hover_\(vLogo)"
        }else if(UIScreen.main.scale < 3){
            vCarLogoHoverImg = "2x_hover_\(vLogo)"
        }else{
            vCarLogoHoverImg = "3x_hover_\(vLogo)"
        }
        
        let hoverImgUrl = vVehicleImgPath + "\(selectedCabTypeId)/ios/\(vCarLogoHoverImg)"
        
        fareDetailView.cabTypeImgView.sd_setImage(with: URL(string: hoverImgUrl), placeholderImage: UIImage(named: "placeHolder.png"),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
            
            GeneralFunctions.setImgTintColor(imgView: fareDetailView.cabTypeImgView, color: UIColor.UCAColor.AppThemeColor)
        })
        
        fareDetailView.capacityHLbl.text = self.generalFunc.getLanguageLabel(origValue: "Capacity", key: "LBL_CAPACITY")
        
        if(self.currentCabGeneralType.uppercased() == Utils.cabGeneralType_UberX.uppercased()){
            fareDetailView.capacityVLbl.text = "--"
        }else{
            fareDetailView.capacityVLbl.text = Configurations.convertNumToAppLocal(numStr: cabTypeItem.get("iPersonSize") + " \(self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_PEOPLE_TXT"))")
        }
        
        fareDetailView.fareHLbl.text = self.generalFunc.getLanguageLabel(origValue: "Fare", key: "LBL_FARE_TXT")
        fareDetailView.noteLbl.text = self.generalFunc.getLanguageLabel(origValue: "This fare is based on our estimation. This may vary during trip and final fare.", key: "LBL_GENERAL_NOTE_FARE_EST")
        fareDetailView.noteLbl.fitText()
        
        fareDetailView.moreDetailsLbl.text = self.generalFunc.getLanguageLabel(origValue: "More Info", key: "LBL_MORE_INFO")
        fareDetailView.fareVLbl.text = Configurations.convertNumToAppLocal(numStr: getCabTypeFare(currentVehicleTypeId: selectedCabTypeId))
    
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        
        let screenWidth = Application.screenSize.width
        let totalCellWidth = (120 * cabTypesArr.count)
        
        
        let leftInset = (collectionView.frame.width - CGFloat(totalCellWidth + 0)) / 2;
        let rightInset = leftInset
        
        if(screenWidth < CGFloat(totalCellWidth)){
            return UIEdgeInsetsMake(0, 0, 0, 0)
        }else{
            return UIEdgeInsetsMake(0, leftInset, 0, rightInset)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cabTypesArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CabTypeCVCell", for: indexPath) as! CabTypeCVCell
        
        let tempDict = cabTypesArr[indexPath.item]
        let iVehicleTypeId = tempDict.get("iVehicleTypeId")
        
        
        if(self.selectedCabTypeId == iVehicleTypeId){
            cell.cabTypeHoverImgView.isHidden = false
            cell.cabTypeImgView.isHidden = true
            cell.cabTypeNameLbl.textColor = UIColor.UCAColor.AppThemeColor
        }else{
            cell.cabTypeHoverImgView.isHidden = true
            cell.cabTypeImgView.isHidden = false
            cell.cabTypeNameLbl.textColor = UIColor(hex: 0x161718)
        }
        
//        if(Configurations.isRTLMode()){
//            var scalingTransform : CGAffineTransform!
//            scalingTransform = CGAffineTransform(scaleX: -1, y: 1);
//            cell.transform = scalingTransform
//         }
        
        cell.fareEstLbl.text = Configurations.convertNumToAppLocal(numStr: getCabTypeFare(currentVehicleTypeId: iVehicleTypeId))
        
        cell.cabTypeNameLbl.text = tempDict.get("vVehicleType")
        
        Utils.createRoundedView(view: cell.cabTypeImgView, borderColor: UIColor(hex: 0xcbcbcb), borderWidth: 1)
        Utils.createRoundedView(view: cell.cabTypeHoverImgView, borderColor: UIColor.UCAColor.AppThemeColor, borderWidth: 1)
        
        var vCarLogoImg = ""
        var vCarLogoHoverImg = ""
        if(UIScreen.main.scale < 2){
            vCarLogoImg = "1x_\(tempDict.get("vLogo"))"
            vCarLogoHoverImg = "1x_hover_\(tempDict.get("vLogo"))"
        }else if(UIScreen.main.scale < 3){
            vCarLogoImg = "2x_\(tempDict.get("vLogo"))"
            vCarLogoHoverImg = "2x_hover_\(tempDict.get("vLogo"))"
        }else{
            vCarLogoImg = "3x_\(tempDict.get("vLogo"))"
            vCarLogoHoverImg = "3x_hover_\(tempDict.get("vLogo"))"
        }
        let imgUrl = vVehicleImgPath + "\(iVehicleTypeId)/ios/\(vCarLogoImg)"

        let hoverImgUrl = vVehicleImgPath + "\(iVehicleTypeId)/ios/\(vCarLogoHoverImg)"
        
        cell.cabTypeHoverImgView.sd_setImage(with: URL(string: hoverImgUrl), placeholderImage: UIImage(named: "placeHolder.png"),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
            
            GeneralFunctions.setImgTintColor(imgView: cell.cabTypeHoverImgView, color: UIColor.UCAColor.AppThemeTxtColor)
        })
        cell.cabTypeImgView.sd_setImage(with: URL(string: imgUrl), placeholderImage: UIImage(named: "placeHolder.png"),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
            GeneralFunctions.setImgTintColor(imgView: cell.cabTypeImgView, color: UIColor(hex: 0x999fa2))
        })
        
        cell.cabTypeImgView.backgroundColor = UIColor(hex: 0xebebeb)
        cell.cabTypeHoverImgView.backgroundColor = UIColor.UCAColor.AppThemeColor
        
        GeneralFunctions.setImgTintColor(imgView: cell.cabTypeHoverImgView, color: UIColor.UCAColor.AppThemeTxtColor)
        GeneralFunctions.setImgTintColor(imgView: cell.cabTypeImgView, color: UIColor(hex: 0x999fa2))

        if(indexPath.item == 0){
            cell.leftSeperationTopView.isHidden = true
            cell.leftSeperationBottomView.isHidden = true
            
        }else{
            
            cell.leftSeperationTopView.isHidden = false
            cell.leftSeperationBottomView.isHidden = false
        }
        
        if(indexPath.item == (self.cabTypesArr.count - 1)){
            cell.rightSeperationTopView.isHidden = true
            cell.rightSeperationBottomView.isHidden = true
        }else{
            cell.rightSeperationTopView.isHidden = false
            cell.rightSeperationBottomView.isHidden = false
        }
        
        return cell
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func updateDriverLocationBeforeTrip(iDriverId:String, latitude:String, longitude:String, dataDict:NSDictionary){
        let driverMarker_temp = getDriverMarkerOnPubNubMsg(iDriverId: iDriverId, isRemoveFromList: false)
        
        if(driverMarker_temp != nil){
            let currentLocation = CLLocation(latitude: driverMarker_temp!.position.latitude, longitude: driverMarker_temp!.position.longitude)
            let location = CLLocation(latitude: GeneralFunctions.parseDouble(origValue: 0.0, data: latitude), longitude: GeneralFunctions.parseDouble(origValue: 0.0, data: longitude))
            
            let rotationAngle = currentLocation.bearingToLocationDegrees(destinationLocation: location, currentRotation: driverMarker_temp!.rotation)
            
            
//            driverMarker_temp!.rotation = 
//            driverMarker_temp!.position = location.coordinate
            
            
//            if(rotationAngle != -1.000){
                if(self.currentCabGeneralType.uppercased() == Utils.cabGeneralType_UberX.uppercased()){
                    driverMarker_temp!.position = location.coordinate
                }else{
                    Utils.updateMarker(marker: driverMarker_temp!, googleMap: self.gMapView, coordinates: location.coordinate, rotationAngle: rotationAngle, duration: 1.0)
                }
                
//            }
        }
    }
    
    func updateDriverLocation(iDriverId:String, latitude:String, longitude:String, dataDict:NSDictionary){
        
        updateAssignedDriverMarker(driverLocation: CLLocation(latitude: GeneralFunctions.parseDouble(origValue: 0.0, data: latitude), longitude: GeneralFunctions.parseDouble(origValue: 0.0, data: longitude)), dataDict: dataDict)
    }
    
    func setDriverArrivedStatus(){
        if(self.isDriverArrived == false){
            self.generalFunc.setError(uv: self, title: "", content: self.generalFunc.getLanguageLabel(origValue: "Driver has been arrived to your location.", key: "LBL_DRIVER_ARRIVE"))
        }
        
        setNavBar(isHidden: false)
        setLblToTitleView()
        self.titleLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_DRIVER_ARRIVED_TXT")
        
        self.isDriverArrived = true
    }
    
    
    
    func getAvailableDriverIds() -> String{
        var driverIds = ""
        
        var finalLoadedDriverList = [NSDictionary]()
        finalLoadedDriverList.append(contentsOf: self.currentLoadedDriverList)
        
        if(userProfileJson.get("DRIVER_REQUEST_METHOD") == "Distance"){
            finalLoadedDriverList = (finalLoadedDriverList as NSArray).sortedArray(using: [NSSortDescriptor(key: "DIST_TO_PICKUP", ascending: true)]) as! [NSDictionary]
        }
        
        for i in 0..<finalLoadedDriverList.count{
            let iDriverId = finalLoadedDriverList[i].get("driver_id")
            
            driverIds = driverIds == "" ? iDriverId : (driverIds + "," + iDriverId)
        }
        
        return driverIds
    }
    
    func myBtnTapped(sender: MyButton) {
        if(self.requestNowBtn != nil && sender == self.requestNowBtn){
            if((self.userProfileJson.get("APP_PAYMENT_MODE") == "Card" && isCardValidated == false) || (isCashPayment == false && isCardValidated == false)){
                checkCardConfig(isRideLater: false, isAutoContinue: true)
                
                return
            }
            
            
            checkSurgePrice(selectedTime: "", isRideLater: false)
            
            
        }else if(retryReqBtn != nil && sender == retryReqBtn){
            self.startDriverRequestQueue()
        }else if(surgeAcceptBtn != nil  && sender == surgeAcceptBtn){
            
            self.cancelSurgeView()
            
            if(isRideLaterFromSurge == false){
                self.requestCab(tollPrice: "", tollPriceCurrencyCode: "", isTollSkipped: "")
            }else{
                self.continueRideLaterSchedule(tollPrice: "", tollPriceCurrencyCode: "", isTollSkipped: "")
            }
        }else if(sender.btnType == "UFX_DRIVER_MORE_INFO"){
            if(providerDetailMarkerView != nil){
                providerDetailMarkerView.closeViewTapped()
            }
            self.ufxSelectedServiceProviderId = ""
            let openProviderDetailView = OpenProviderDetailView(uv: self, containerView: self.contentView)
            openProviderDetailView.setViewHandler(handler: { (isContinueBtnTapped) in
                if(isContinueBtnTapped == true){
                    self.checkSurgePrice(selectedTime: "", isRideLater: false)
                }
            })
            
            openProviderDetailView.show(dataDict: currentLoadedDriverList[sender.tag])
        }else if(self.requestPickUpView != nil && sender == self.rideLaterBtn){
            self.continueRideLaterSchedule(tollPrice: "", tollPriceCurrencyCode: "", isTollSkipped: "")
        }
    }
    
    func addDriverNotificationObserverForRideLater(){
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.driverCallBackReceivedRideLater(sender:)), name: NSNotification.Name(rawValue: Utils.driverCallBackNotificationKey), object: nil)
    }
    
    func addDriverNotificationObserver(){
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.driverCallBackReceived(sender:)), name: NSNotification.Name(rawValue: Utils.driverCallBackNotificationKey), object: nil)
    }
    
    func driverCallBackReceivedRideLater(sender: NSNotification){
        let userInfo = sender.userInfo
        let msgData = (userInfo!["body"] as! String).getJsonDataDict()
        
        let msgStr = msgData.get("Message")
        
        if(msgStr == "CabRequestAccepted"){
            if(msgData.get("iCabBookingId") != ""){
                let window = Application.window
                
                let getUserData = GetUserData(uv: self, window: window!)
                getUserData.getdata()
                
                return
            }
        }
    }
    
    func driverCallBackReceived(sender: NSNotification){
        let userInfo = sender.userInfo
        let msgData = (userInfo!["body"] as! String).getJsonDataDict()
        
        let msgStr = msgData.get("Message")
        
        Utils.closeKeyboard(uv: self)
        
        if(msgStr == "CabRequestAccepted"){
            if(self.isDriverAssigned == true){
                return
            }
            
            
            LocalNotification.dispatchlocalNotification(with: "", body: (GeneralFunctions()).getLanguageLabel(origValue: "", key: "LBL_DRIVER_ARRIVING_TXT"), at: Date().addedBy(seconds: 0), onlyInBackground: true)
            
            self.stopDriverRequestQueue()
            self.isDriverAssigned = true
            assignedDriverId = msgData.get("iDriverId")
            assignedTripId = msgData.get("iTripId")
            
            if(msgData.get("iCabBookingId") != ""){
                let window = Application.window
                
                let getUserData = GetUserData(uv: self, window: window!)
                getUserData.getdata()
                
                return
            }
            
            if(self.currentCabGeneralType.uppercased() == Utils.cabGeneralType_UberX.uppercased()){
                self.ufxDriverAcceptedReqNow = true
                loadBookingFinishView()
                return
            }
            
            setNavBar(isHidden: false)
            setLblToTitleView()
            self.titleLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_DRIVER_ARRIVING_TXT")
            
            configureAssignedDriver(isAppRestarted:false)
            
            self.sourcePickUpEtaLbl.isHidden = true
            
        }else if(msgStr == "TripStarted"){
            if(self.isDriverAssigned == false){
                let window = Application.window
                
                let getUserData = GetUserData(uv: self, window: window!)
                getUserData.getdata()
                return
            }
            
            if(self.isTripStarted == true){
                return
            }
            LocalNotification.dispatchlocalNotification(with: "", body: (GeneralFunctions()).getLanguageLabel(origValue: "", key: "LBL_START_TRIP_DIALOG_TXT"), at: Date().addedBy(seconds: 0), onlyInBackground: true)
            
            setTripStartData(deliveryConfirmCode: msgData.get("VerificationCode"))
        }else if(msgStr == "DestinationAdded"){
            setUpDestination()
        }else if(msgStr == "TripCancelledByDriver" || msgStr == "TripEnd"){
            if(self.isDriverAssigned == false || (msgStr == "TripEnd" && self.isTripStarted == false)){
                let window = Application.window
                
                let getUserData = GetUserData(uv: self, window: window!)
                getUserData.getdata()
                return
            }
            
            if(self.isTripFinished == true){
                return
            }
            
            let cancelTripMsg = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_PREFIX_TRIP_CANCEL_DRIVER") + " " + msgData.get("Reason") + " " + self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_CANCEL_TRIP_BY_DRIVER_MSG_SUFFIX")
            LocalNotification.dispatchlocalNotification(with: "", body: msgStr == "TripCancelledByDriver" ? cancelTripMsg : self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_END_TRIP_DIALOG_TXT"), at: Date().addedBy(seconds: 0), onlyInBackground: true)
            
            self.isTripFinished = true
                        
            self.generalFunc.setAlertMessage(uv: self, title: self.generalFunc.getLanguageLabel(origValue: "", key: msgStr == "TripCancelledByDriver" ? "LBL_CANCELED_TXT" : "LBL_FINISHED_TXT"), content: msgStr == "TripCancelledByDriver" ? cancelTripMsg : self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_END_TRIP_DIALOG_TXT"), positiveBtn: self.generalFunc.getLanguageLabel(origValue: "Ok", key: "LBL_BTN_OK_TXT"), nagativeBtn: "", completionHandler: { (btnClickedIndex) in
                
                let window = Application.window
                
                let getUserData = GetUserData(uv: self, window: window!)
                getUserData.getdata()
                
            })
        }
    }
    
    func configureAssignedDriver(isAppRestarted:Bool){
        
        closeCabReqView()
        removeAddReqPickUpView(isDriverAssigned: true)
        
        if(self.recentLocView != nil){
            self.recentLocView.isViewHidden = true
            self.recentLocView.isHidden = true
            if(self.recentLocView.view != nil){
                self.recentLocView.view.removeFromSuperview()
                self.recentLocView.view.isHidden = true
            }
        }
        if(self.loadAvailableCab != nil){
            self.loadAvailableCab.onPauseCalled()
            self.loadAvailableCab.setTaskKilledValue(isTaskKilled: true)
            self.loadAvailableCab = nil
        }
        self.isMyLocationEnabled = false
        self.gMapView.isMyLocationEnabled = isMyLocationEnabled
        self.gMapView.clear()
        
        
        driverDetailView = self.generalFunc.loadView(nibName: "DriverDetailView", uv: self, isWithOutSize: true)
        driverDetailView.frame = CGRect(x: 0, y: self.contentView.frame.height + 220, width: Application.screenSize.width, height: 220)

        self.view.addSubview(driverDetailView)
        
        UIView.animate(withDuration: 0.8,
                       animations: {
                        self.driverDetailView.frame.origin.y = self.contentView.frame.height - 220
                        self.view.layoutIfNeeded()
        },  completion: { finished in
           
        })
        
        
        
        if(isAppRestarted == true){
            addDriverNotificationObserver()
            
            self.isDriverAssigned = true
            
            let tripDetailJson = userProfileJson.getObj("TripDetails")
            let driverDetailJson = userProfileJson.getObj("DriverDetails")
            let driverCarDetailJson = userProfileJson.getObj("DriverCarDetails")
            
            let vTripPaymentMode = tripDetailJson.get("vTripPaymentMode")
            let tEndLat = tripDetailJson.get("tEndLat")
            let tEndLong = tripDetailJson.get("tEndLong")
            let tDaddress = tripDetailJson.get("tDaddress")
            
            
            
            let driverTripStatus = driverDetailJson.get("vTripStatus")
            
            if(driverTripStatus == "Arrived"){
                setNavBar(isHidden: false)
                setLblToTitleView()
                self.titleLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_DRIVER_ARRIVED_TXT")
                
                self.isDriverArrived = true
            }
            
            if (vTripPaymentMode == "Cash") {
                self.isCashPayment = true
            } else {
                self.isCashPayment = false
            }
            assignedDriverId = tripDetailJson.get("iDriverId")
            assignedTripId = tripDetailJson.get("iTripId")
            eTripType = tripDetailJson.get("eType")
            
            self.selectedCabTypeId = tripDetailJson.get("iVehicleTypeId")
            
            
            addressContainerView.pickUpAddressLbl.text = tripDetailJson.get("tSaddress")

            if (tEndLat != "0.0" && tEndLong != "0.0" && tDaddress != "Not Set" && tEndLat != "" && tEndLong != "" && tDaddress != "") {
                destAddress = tDaddress
                
                self.destLocation = CLLocation(latitude: GeneralFunctions.parseDouble(origValue: 0.0, data: tEndLat), longitude: GeneralFunctions.parseDouble(origValue: 0.0, data: tEndLong))
                
                addressContainerView.destAddressLbl.text = tDaddress
                
            }
            
            
            self.pickUpLocation = CLLocation(latitude: GeneralFunctions.parseDouble(origValue: 0.0, data: tripDetailJson.get("tStartLat")), longitude: GeneralFunctions.parseDouble(origValue: 0.0, data: tripDetailJson.get("tStartLong")))
            
            self.destLocation = CLLocation(latitude: GeneralFunctions.parseDouble(origValue: 0.0, data: tEndLat), longitude: GeneralFunctions.parseDouble(origValue: 0.0, data: tEndLong))
            
            assignedDriverData["PickUpLatitude"] = "\(tripDetailJson.get("tStartLat"))"
            assignedDriverData["PickUpLongitude"] = "\(tripDetailJson.get("tStartLong"))"
            assignedDriverData["PickUpAddress"] = "\(tripDetailJson.get("tSaddress"))"
            assignedDriverData["vVehicleType"] = "\(tripDetailJson.get("vVehicleType"))"
            assignedDriverData["vDeliveryConfirmCode"] = "\(tripDetailJson.get("vDeliveryConfirmCode"))"
            assignedDriverData["eType"] = "\(tripDetailJson.get("eType"))"
            assignedDriverData["TripStatus"] = "\(driverDetailJson.get("vTripStatus"))"
            assignedDriverData["DriverTripStatus"] = "\(driverDetailJson.get("vTripStatus"))"
            assignedDriverData["DriverPhone"] = "\(driverDetailJson.get("vPhone"))"
            assignedDriverData["DriverRating"] = "\(driverDetailJson.get("vAvgRating"))"
            assignedDriverData["DriverAppVersion"] = "\(driverDetailJson.get("iAppVersion"))"
            assignedDriverData["DriverLatitude"] = "\(driverDetailJson.get("vLatitude"))"
            assignedDriverData["DriverLongitude"] = "\(driverDetailJson.get("vLongitude"))"
            assignedDriverData["DriverImage"] = "\(driverDetailJson.get("vImage"))"
            assignedDriverData["DriverName"] = "\(driverDetailJson.get("vName"))"
            assignedDriverData["DriverCarPlateNum"] = "\(driverCarDetailJson.get("vLicencePlate"))"
            assignedDriverData["DriverCarColor"] = "\(driverCarDetailJson.get("vColour"))"
            assignedDriverData["DriverCarName"] = "\(driverCarDetailJson.get("make_title"))"
            assignedDriverData["DriverCarModelName"] = "\(driverCarDetailJson.get("model_title"))"
            assignedDriverData["vDeliveryConfirmCode"] = "\(tripDetailJson.get("vDeliveryConfirmCode"))"
            assignedDriverData["eTollSkipped"] = "\(tripDetailJson.get("eTollSkipped"))"
            
        }else{
            if(self.currentLoadedDriverList.count == 0){
                let window = Application.window
                
                let getUserData = GetUserData(uv: self, window: window!)
                getUserData.getdata()
                return
            }
            
            var isDriverIdMatch = false
            
            for i in 0..<currentLoadedDriverList.count{
                let driverDataMap = currentLoadedDriverList[i]
                
                let iDriverId = driverDataMap.get("driver_id")
                
                if(iDriverId == self.assignedDriverId){
                    
                    isDriverIdMatch = true
                    
                    assignedDriverData["PickUpLatitude"] = "\(self.pickUpLocation.coordinate.latitude)"
                    assignedDriverData["PickUpLongitude"] = "\(self.pickUpLocation.coordinate.longitude)"
                    assignedDriverData["PickUpAddress"] = "\(self.pickUpAddress)"
                    assignedDriverData["vVehicleType"] = "\(GeneralFunctions.getSelectedCarTypeData(selectedCarTypeId: selectedCabTypeId, dataKey: "vVehicleType", carTypesArr: cabTypesArr as NSArray))"
                    assignedDriverData["vDeliveryConfirmCode"] = ""
                    assignedDriverData["DriverTripStatus"] = ""
                    assignedDriverData["TripStatus"] = ""
                    assignedDriverData["DriverPhone"] = "\(driverDataMap.get("vPhone_driver"))"
                    assignedDriverData["DriverRating"] = "\(driverDataMap.get("average_rating"))"
                    assignedDriverData["DriverAppVersion"] = "\(driverDataMap.get("iAppVersion"))"
                    assignedDriverData["DriverLatitude"] = "\(driverDataMap.get("Latitude"))"
                    assignedDriverData["DriverLongitude"] = "\(driverDataMap.get("Longitude"))"
                    assignedDriverData["DriverImage"] = "\(driverDataMap.get("driver_img"))"
                    assignedDriverData["DriverName"] = "\(driverDataMap.get("Name"))"
                    assignedDriverData["DriverCarPlateNum"] = "\(driverDataMap.get("vLicencePlate"))"
                    assignedDriverData["DriverCarColor"] = "\(driverDataMap.get("vColour"))"
                    assignedDriverData["DriverCarName"] = "\(driverDataMap.get("make_title"))"
                    assignedDriverData["DriverCarModelName"] = "\(driverDataMap.get("model_title"))"
                    assignedDriverData["eType"] = "\(self.currentCabGeneralType)"
                    assignedDriverData["vDeliveryConfirmCode"] = ""
                    assignedDriverData["eTollSkipped"] = "\(currTollSkipped)"
                    
                    break
                }
                
            }
        }
        
        
        
        assignedDriverData["iDriverId"] = "\(self.assignedDriverId)"
        assignedDriverData["iTripId"] = "\(self.assignedTripId)"
        assignedDriverData["PassengerName"] = "\(self.userProfileJson.get("vName"))"
        assignedDriverData["PassengerImageName"] = "\(self.userProfileJson.get("vImgName"))"
        
        setDriverData()
        
        if(GeneralFunctions.getValue(key: "OPEN_MSG_SCREEN") != nil && (GeneralFunctions.getValue(key: "OPEN_MSG_SCREEN") as! String) == "true"){
            let chatUV = GeneralFunctions.instantiateViewController(pageName: "ChatUV") as! ChatUV
            
            let assignedDriverData = self.assignedDriverData as NSDictionary
            
            GeneralFunctions.removeValue(key: "OPEN_MSG_SCREEN")
            
            chatUV.receiverId = assignedDriverData.get("iDriverId")
            chatUV.receiverDisplayName = assignedDriverData.get("DriverName")
            chatUV.assignedtripId = assignedDriverData.get("iTripId")
            chatUV.pPicName = assignedDriverData.get("DriverImage")
            
            self.pushToNavController(uv:chatUV, isDirect: true)
            
        }
    }
    
    func setDriverData(){
        Utils.driverMarkersPositionList.removeAll()
        Utils.driverMarkerAnimFinished = true
        
        if(configPubNub != nil){
            configPubNub!.iTripId = self.assignedTripId
        }
        
        GeneralFunctions.saveValue(key: "isDriverAssigned", value: "true" as AnyObject)
        
        let assignedDriverData = self.assignedDriverData as NSDictionary
    
        let driverLocation = CLLocation(latitude: GeneralFunctions.parseDouble(origValue: 0.0, data: assignedDriverData.get("DriverLatitude")), longitude: GeneralFunctions.parseDouble(origValue: 0.0, data: assignedDriverData.get("DriverLongitude")))
        
        updateAssignedDriverMarker(driverLocation: driverLocation, dataDict: nil)

        if(updateDirection != nil){
            self.updateDirection.releaseTask()
        }
        
        if(assignedDriverData.get("TripStatus") == "On Going Trip"){
            setTripStartData(deliveryConfirmCode: assignedDriverData.get("vDeliveryConfirmCode"))
//            setTripStartData()
        }else{
            
            self.updateDirection = UpdateDirections(uv: self, gMap: self.gMapView, fromLocation: driverLocation, destinationLocation: self.pickUpLocation, isCurrentLocationEnabled: false)
            self.updateDirection.onDirectionUpdateDelegate = self
            self.updateDirection.scheduleDirectionUpdate(eTollSkipped: "")
            
            addWaitingMarker(fromLocation: driverLocation, toLocation: self.pickUpLocation, waitingTime: "")
        }
        
        if(self.configPubNub == nil){
            let DRIVER_LOC_FETCH_TIME_INTERVAL = GeneralFunctions.parseFloat(origValue: 5, data: self.userProfileJson.get("DRIVER_LOC_FETCH_TIME_INTERVAL"))
            updateFreqDriverLocTask = UpdateFreqTask(interval: CGFloat(DRIVER_LOC_FETCH_TIME_INTERVAL))
            updateFreqDriverLocTask.currInst = updateFreqDriverLocTask
            updateFreqDriverLocTask.setTaskRunListener(onTaskRunCalled: self)
        }
        
        
        self.driverCallLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_CALL_TXT")
        self.driverMsgLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_MESSAGE_TXT")
        self.driverShareLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_SHARE_BTN_TXT")
        self.driverCancelTripLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_CANCEL_TXT")
        
        self.driverNameLbl.text = assignedDriverData.get("DriverName")
        self.driverCarType.text = "(\(assignedDriverData.get("DriverCarColor") == "" ? assignedDriverData.get("vVehicleType") : assignedDriverData.get("DriverCarColor")))"
        self.driverCardNameLbl.text = assignedDriverData.get("DriverCarName") + "-" + assignedDriverData.get("DriverCarModelName")
        self.driverCarPlateNoLbl.text = assignedDriverData.get("DriverCarPlateNum")
        
        driverPicImgView.sd_setImage(with: URL(string: CommonUtils.driver_image_url + "\(assignedDriverData.get("iDriverId"))/\(assignedDriverData.get("DriverImage"))"), placeholderImage: UIImage(named: "ic_no_pic_user"),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
            
        })
        
        Utils.createRoundedView(view: driverPicImgView, borderColor: UIColor.clear, borderWidth: 0)
        
        self.carIcBgView.backgroundColor = UIColor.UCAColor.AppThemeColor
        GeneralFunctions.setImgTintColor(imgView: self.carIcImgView, color: UIColor.UCAColor.AppThemeTxtColor)
        
        GeneralFunctions.setImgTintColor(imgView: self.driverCallImgView, color: UIColor.UCAColor.AppThemeColor)
        GeneralFunctions.setImgTintColor(imgView: self.driverMsgImgView, color: UIColor.UCAColor.AppThemeColor)
        GeneralFunctions.setImgTintColor(imgView: self.driverCancelTripImgView, color: UIColor.UCAColor.AppThemeColor)
        GeneralFunctions.setImgTintColor(imgView: self.driverShareImgView, color: UIColor.UCAColor.AppThemeColor)
        
        let cancelTripTapGue = UITapGestureRecognizer()
        cancelTripTapGue.addTarget(self, action: #selector(self.cancelTripTapped))
        
        let callDriverTapGue = UITapGestureRecognizer()
        callDriverTapGue.addTarget(self, action: #selector(self.callDriverTapped))
        
        let msgDriverTapGue = UITapGestureRecognizer()
        msgDriverTapGue.addTarget(self, action: #selector(self.msgDriverTapped))
        
        let shareTripTapGue = UITapGestureRecognizer()
        shareTripTapGue.addTarget(self, action: #selector(self.shareTripTapped))
        
        self.driverCancelTripView.isUserInteractionEnabled = true
        self.driverCallView.isUserInteractionEnabled = true
        self.driverMsgView.isUserInteractionEnabled = true
        self.driverShareView.isUserInteractionEnabled = true
        
        self.driverCancelTripView.addGestureRecognizer(cancelTripTapGue)
        self.driverCallView.addGestureRecognizer(callDriverTapGue)
        self.driverMsgView.addGestureRecognizer(msgDriverTapGue)
        self.driverShareView.addGestureRecognizer(shareTripTapGue)
        
        self.driverRatingLbl.text = Configurations.convertNumToAppLocal(numStr: assignedDriverData.get("DriverRating"))
        self.driverRatingBar.rating = GeneralFunctions.parseFloat(origValue: 0, data: assignedDriverData.get("DriverRating"))
        
        
        subscribeToDriverLocChannel()
        
        if(self.addressContainerView != nil){
            self.addressContainerView.isDriverAssigned = self.isDriverAssigned
            self.addressContainerView.addDestImgView.isHidden = true
            self.addressContainerView.addPickUpImgView.isHidden = true
        }
    }
    
    func callDriverTapped(){
        let assignedDriverData = self.assignedDriverData as NSDictionary
        
        
        let number = "\(assignedDriverData.get("DriverPhone"))"
        UIApplication.shared.openURL(NSURL(string:"telprompt:" + number)! as URL)
    }
    
    func msgDriverTapped(){
        let chatUV = GeneralFunctions.instantiateViewController(pageName: "ChatUV") as! ChatUV
        chatUV.receiverId = assignedDriverData["iDriverId"]!
        chatUV.receiverDisplayName = assignedDriverData["DriverName"]!
        chatUV.assignedtripId = assignedDriverData["iTripId"]!
        chatUV.pPicName = assignedDriverData["DriverImage"]!
//        self.pushToNavController(uv:chatUV)
        
        self.pushToNavController(uv:chatUV, isDirect: true)
        
//        let assignedDriverData = self.assignedDriverData as NSDictionary
//        
//        let number = "\(assignedDriverData.get("DriverPhone"))"
//        UIApplication.shared.openURL(NSURL(string:"sms:" + number)! as URL)
    }
    
    func cancelTripTapped(){
        
        self.generalFunc.setAlertMessage(uv: self, title: "", content: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_TRIP_CANCEL_TXT"), positiveBtn: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_CANCEL_TRIP_NOW"), nagativeBtn: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_CONTINUE_TRIP_TXT"), completionHandler: { (btnClickedIndex) in
            
            if(btnClickedIndex == 0){
                self.continueCancelTrip()
            }
        })
        
        
    }
    
    func continueCancelTrip(status : String = "", reference: String = "", code: String = ""){
    
        var parameters = ["type":"cancelTrip", "iUserId": GeneralFunctions.getMemberd(), "iDriverId": self.assignedDriverId, "UserType": Utils.appUserType, "iTripId": self.assignedTripId]
        if(GeneralFunctions.checkCardType() == "PayStack") {
            parameters["email"] = GeneralFunctions.getEmail()
            parameters["status"] = status
            parameters["reference"] = reference
            parameters["code"] = code
        }
        
        let exeWebServerUrl = ExeServerUrl(dict_data: parameters, currentView: self.view, isOpenLoader: true)
        exeWebServerUrl.executePostProcess(completionHandler: { (response) -> Void in
            
            if(response != ""){
                let dataDict = response.getJsonDataDict()
                
                if(dataDict.get("Action") == "1"){
                    
                    self.generalFunc.setAlertMessage(uv: self, title: "", content: self.generalFunc.getLanguageLabel(origValue: "Your trip is successfully canceled.", key: "LBL_SUCCESS_TRIP_CANCELED"), positiveBtn: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_BTN_OK_TXT"), nagativeBtn: "", completionHandler: { (btnClickedIndex) in
                        
                        let window = Application.window
                        
                        let getUserData = GetUserData(uv: self, window: window!)
                        getUserData.getdata()
                        
                    })
                }else{
                    
                    if(dataDict.get(Utils.message_str) == "DO_RESTART" || dataDict.get("message") == "LBL_SERVER_COMM_ERROR" || dataDict.get("message") == "GCM_FAILED" || dataDict.get("message") == "APNS_FAILED"){
                        let window = Application.window
                        
                        let getUserData = GetUserData(uv: self, window: window!)
                        getUserData.getdata()
                        
                        return
                    }
                    else if (dataDict.get(Utils.message_str) == "send_pin") {
                        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "VerifyViewController") as! VerifyViewController
                        vc.finishDelegate = self
                        vc.reference = dataDict.get("reference")
                        vc.status = "send_pin"
                        vc.amount = dataDict.get("amount")
                        vc.modalPresentationStyle = .overCurrentContext
                        self.present(vc, animated: true, completion: nil)
                    }
                    else if (dataDict.get(Utils.message_str) == "send_otp") {
                        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "VerifyViewController") as! VerifyViewController
                        vc.finishDelegate = self
                        vc.reference = dataDict.get("reference")
                        vc.status = "send_otp"
                        vc.amount = dataDict.get("amount")
                        vc.modalPresentationStyle = .overCurrentContext
                        self.present(vc, animated: true, completion: nil)
                    }
                    else if (dataDict.get(Utils.message_str) == "send_phone") {
                        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "VerifyViewController") as! VerifyViewController
                        vc.finishDelegate = self
                        vc.reference = dataDict.get("reference")
                        vc.status = "send_phone"
                        vc.amount = dataDict.get("amount")
                        vc.modalPresentationStyle = .overCurrentContext
                        self.present(vc, animated: true, completion: nil)
                    }
                    else {
                        self.generalFunc.setError(uv: self, title: "", content: self.generalFunc.getLanguageLabel(origValue: "", key: dataDict.get("message")))
                    }
                }
                
            }else{
                    self.generalFunc.setError(uv: self)
            }
        })
        
    }
    
    func shareTripTapped(){
        
        let latitude = "\(assignedDriverLocation.coordinate.latitude)"
        let longitude = "\(assignedDriverLocation.coordinate.longitude)"
        
        let map_location_url = "http://maps.google.com/?q=" + latitude + "," + longitude;
        
        let share_txt_str = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_SEND_STATUS_CONTENT_TXT") + " " + map_location_url
        
        let objectsToShare = [share_txt_str]
        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        activityVC.excludedActivityTypes = [UIActivityType.airDrop, UIActivityType.addToReadingList]
        activityVC.popoverPresentationController?.sourceView = self.view
        self.present(activityVC, animated: true, completion: nil)
        
    }
    
    func onTaskRun(currInst: UpdateFreqTask) {
        checkDriverLocation()
    }
    
    func checkDriverLocation(){
        
        let parameters = ["type":"getDriverLocations", "iUserId": GeneralFunctions.getMemberd(), "iDriverId": self.assignedDriverId, "UserType": Utils.appUserType]
        
        let exeWebServerUrl = ExeServerUrl(dict_data: parameters, currentView: self.view, isOpenLoader: true)
        exeWebServerUrl.executePostProcess(completionHandler: { (response) -> Void in
            
            if(response != ""){
                let dataDict = response.getJsonDataDict()
                
                if(dataDict.get("Action") == "1"){
                    
                    let vLatitude = dataDict.get("vLatitude")
                    let vLongitude = dataDict.get("vLongitude")
                    let vTripStatus = dataDict.get("vTripStatus")
                    
                    if(vTripStatus == "Arrived" && self.isDriverArrived == false){
                        self.setDriverArrivedStatus()
                    }
                    if(vLatitude != "" && vLatitude != "0.0" && vLatitude != "-180.0" && vLongitude != "" && vLongitude != "0.0" && vLongitude != "-180.0"){
                        
                        self.updateAssignedDriverMarker(driverLocation: CLLocation(latitude: GeneralFunctions.parseDouble(origValue: 0.0, data: vLatitude), longitude: GeneralFunctions.parseDouble(origValue: 0.0, data: vLongitude)), dataDict: nil)
                    }
                }else{
                    
                    
//                    self.generalFunc.setError(uv: self, title: "", content: self.generalFunc.getLanguageLabel(origValue: "", key: dataDict.get("message")))
                }
                
            }else{
//                self.generalFunc.setError(uv: self)
            }
        })
        
    }
    
    
    func updateAssignedDriverMarker(driverLocation:CLLocation, dataDict:NSDictionary?){
        
        
        
        self.assignedDriverLocation = driverLocation
        
        if(self.assignedDriverMarker == nil){
            let driverMarker = GMSMarker()
            self.assignedDriverMarker = driverMarker
        }
        
        if(self.updateDirection != nil){
            if(self.isTripStarted == false && self.isDriverAssigned == true){
                self.updateDirection.changeLocation(fromLocation: driverLocation, destinationLocation: self.pickUpLocation)
            }else if(self.isTripStarted == true && self.isDriverAssigned == true){
                self.updateDirection.changeLocation(fromLocation: self.assignedDriverLocation, destinationLocation: self.destLocation)
            }
        }
        
        
        var rotationAngle:Double = -1
        if(assignedDriverRotatedLocation != nil){
            rotationAngle = assignedDriverRotatedLocation.bearingToLocationDegrees(destinationLocation: driverLocation, currentRotation: assignedDriverMarker.rotation)
//            Utils.printLog(msgData: "rotationAngle0:\(rotationAngle)")
            if(rotationAngle != -1){
                assignedDriverRotatedLocation = driverLocation
            }
        }else{
            assignedDriverRotatedLocation = driverLocation
        }
        
//        Utils.printLog(msgData: "rotationAngle1:\(rotationAngle)")
//        Utils.updateMarker(marker: assignedDriverMarker, googleMap: self.gMapView, coordinates: driverLocation.coordinate, rotationAngle: rotationAngle, duration: 1.0)
        
        
        if(dataDict != nil && self.assignedDriverMarker != nil){
            
            let previousItemOfMarker = Utils.getLastLocationDataOfMarker(marker: assignedDriverMarker)
            
            var tempData = [String:String]()
            tempData["vLatitude"] = "\(driverLocation.coordinate.latitude)"
            tempData["vLongitude"] = "\(driverLocation.coordinate.longitude)"
            tempData["iDriverId"] = "\(self.assignedDriverId)"
            tempData["RotationAngle"] = "\(rotationAngle)"
            tempData["LocTime"] = "\(dataDict!.get("LocTime"))"
            
            if(previousItemOfMarker.get("LocTime") != "" && dataDict!.get("LocTime") != ""){
                
                let locTime = Int64(previousItemOfMarker.get("LocTime"))
                let newLocTime = Int64(dataDict!.get("LocTime"))
                
                if(locTime != nil && newLocTime != nil){
                    
                    if((newLocTime! - locTime!) > 0 && Utils.driverMarkerAnimFinished == false){
                        Utils.driverMarkersPositionList.append(tempData as NSDictionary)
                    }else if((newLocTime! - locTime!) > 0){
                        Utils.updateMarkerOnTrip(marker: assignedDriverMarker, googleMap: self.gMapView, coordinates: driverLocation.coordinate, rotationAngle: rotationAngle, duration: 0.8, iDriverId: self.assignedDriverId, LocTime: dataDict!.get("LocTime"))
                    }
                
                }else if((locTime == nil || newLocTime == nil) && Utils.driverMarkerAnimFinished == false){
                    Utils.driverMarkersPositionList.append(tempData as NSDictionary)
                }else{
                     Utils.updateMarkerOnTrip(marker: assignedDriverMarker, googleMap: self.gMapView, coordinates: driverLocation.coordinate, rotationAngle: rotationAngle, duration: 0.8, iDriverId: self.assignedDriverId, LocTime: dataDict!.get("LocTime"))
                }
                
            }else if(Utils.driverMarkerAnimFinished == false){
                Utils.driverMarkersPositionList.append(tempData as NSDictionary)
            }else{
                 Utils.updateMarkerOnTrip(marker: assignedDriverMarker, googleMap: self.gMapView, coordinates: driverLocation.coordinate, rotationAngle: rotationAngle, duration: 0.8, iDriverId: self.assignedDriverId, LocTime: dataDict!.get("LocTime"))
            }
            
        }else{
             Utils.updateMarkerOnTrip(marker: assignedDriverMarker, googleMap: self.gMapView, coordinates: driverLocation.coordinate, rotationAngle: rotationAngle, duration: 0.8, iDriverId: self.assignedDriverId, LocTime: "")
        }
        
        
        assignedDriverMarker.title = self.assignedDriverId
        assignedDriverMarker.icon = UIImage(named: "ic_driver_car_pin")
        assignedDriverMarker.map = self.gMapView
        assignedDriverMarker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
        assignedDriverMarker.infoWindowAnchor = CGPoint(x: 0.5, y: 0.5)
        assignedDriverMarker.isFlat = true
        
        
    }
    

    func onDirectionUpdate(directionResultDict: NSDictionary) {
        let routesArr = directionResultDict.getArrObj("routes")
        let legs_arr = (routesArr.object(at: 0) as! NSDictionary).getArrObj("legs")
        let duration = (legs_arr.object(at: 0) as! NSDictionary).getObj("duration").get("text")
        let value = GeneralFunctions.parseDouble(origValue: 1.0, data: (legs_arr.object(at: 0) as! NSDictionary).getObj("duration").get("value"))
        
        
        if(self.isDriverAssigned == true && self.isTripStarted == true){
            if(self.destLocation != nil){
                
                addWaitingMarker(fromLocation: self.assignedDriverLocation, toLocation: self.destLocation, waitingTime: duration)
            }
        }else if(self.isDriverAssigned == true ){
            if(assignedDriverLocation != nil && self.pickUpLocation != nil){
                addWaitingMarker(fromLocation: self.assignedDriverLocation, toLocation: self.pickUpLocation, waitingTime: duration)
            }
            
            if(lastArrivingNotificationTime != nil && (Utils.currentTimeMillis() - lastArrivingNotificationTime) > 59000){
                return
            }
            if(value == 0.0 && arrivingNotificationCounter1 == false)
            {
                setArrivingNotification()
                arrivingNotificationCounter1 = true
            }
            else if(value <= 3.0 && arrivingNotificationCounter2 == false)
            {
                setArrivingNotification()
                arrivingNotificationCounter2 = true
            }
            else if(value <= 1.0 && arrivingNotificationCounter3 == false)
            {
                setArrivingNotification()
                arrivingNotificationCounter3 = true
            }


        }

    }
    func setArrivingNotification()
    {
        let localNotification = UILocalNotification()
        localNotification.fireDate =  NSDate(timeIntervalSinceNow: 2) as Date
        localNotification.alertBody = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_DRIVER_ARRIVING_TXT")

        //localNotification.timeZone = NSTimeZone.init(name: "GMT") as TimeZone?
        
        localNotification.soundName = UILocalNotificationDefaultSoundName
        UIApplication.shared.scheduleLocalNotification(localNotification)
        lastArrivingNotificationTime = Utils.currentTimeMillis()
    }
    
    func addWaitingMarker(fromLocation:CLLocation, toLocation:CLLocation, waitingTime:String){
       
        if(waitingMarker == nil){
            waitingMarker = GMSMarker()
        }
        
        var minTime = "--"
        if(waitingTime != ""){
            minTime = waitingTime
            
            self.waitingMarker.icon = Utils.textToImage(drawText: minTime.replace(" ", withString: "\n") as NSString, inImage: UIImage(named:"ic_wait_marker")!, atYPoint: 8)
            
        }else{
            var DRIVER_ARRIVED_MIN_TIME_PER_MINUTE:Double = 3
            DRIVER_ARRIVED_MIN_TIME_PER_MINUTE = GeneralFunctions.parseDouble(origValue: 3, data: userProfileJson.get("DRIVER_ARRIVED_MIN_TIME_PER_MINUTE"))
            
            var distance = fromLocation.distance(from: toLocation) / 1000
            
            if(fromLocation.coordinate.latitude == 0.0 || fromLocation.coordinate.longitude == 0.0 || toLocation.coordinate.latitude == 0.0 || toLocation.coordinate.longitude == 0.0){
                distance = 0
            }
            
            
            let lowestTime = distance * DRIVER_ARRIVED_MIN_TIME_PER_MINUTE
            
            let lowestTime_int = Int(lowestTime)
            
            if(lowestTime_int < 1){
                minTime = "--"
            }else{
                minTime = "\(lowestTime_int)"
            }
            
            
            
            self.waitingMarker.icon = Utils.textToImage(drawText: minTime + "\n" + self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_MIN_SMALL_TXT") as NSString, inImage: UIImage(named:"ic_wait_marker")!, atYPoint: 8)
        }
        
        
        self.waitingMarker.position = toLocation.coordinate
        
        
        self.waitingMarker.map = self.gMapView
        self.waitingMarker.infoWindowAnchor = CGPoint(x: 0.5, y:0.5)
    }
    
    func subscribeToDriverLocChannel(){
        var channels =  [String]()
        channels += [Utils.PUBNUB_UPDATE_LOC_CHANNEL_PREFIX_DRIVER+self.assignedDriverId]
        if(configPubNub != nil){
            self.configPubNub?.subscribeToChannels(channels: channels)
        }
    }
    
    func unSubscribeToDriverLocChannel(){
        var channels =  [String]()
        channels += [Utils.PUBNUB_UPDATE_LOC_CHANNEL_PREFIX_DRIVER+self.assignedDriverId]
        if(configPubNub != nil){
            self.configPubNub?.subscribeToChannels(channels: channels)
        }
    }
    
    func setTripStartData(deliveryConfirmCode:String){
        self.isTripStarted = true
        
        self.generalFunc.setError(uv: self, title: "", content: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_START_TRIP_DIALOG_TXT"))
        
        if(waitingMarker != nil){
            waitingMarker.map = nil
        }
        
        if(self.updateDirection != nil){
            self.updateDirection.releaseTask()
        }
        
        if(self.destLocation != nil){
            self.updateDirection = UpdateDirections(uv: self, gMap: self.gMapView, fromLocation: self.assignedDriverLocation, destinationLocation: self.destLocation, isCurrentLocationEnabled: false)
            self.updateDirection.onDirectionUpdateDelegate = self
            self.updateDirection.scheduleDirectionUpdate(eTollSkipped: (self.assignedDriverData as NSDictionary).get("eTollSkipped"))
        }
        
        if(destLocation != nil){
            addWaitingMarker(fromLocation: self.pickUpLocation, toLocation: self.destLocation, waitingTime: "")
        }
        
        self.driverCancelTripView.isHidden = true
        
        setNavBar(isHidden: false)
        setLblToTitleView()
        self.titleLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_EN_ROUTE_TXT")
        
        self.emeImgView = UIImageView(frame: CGRect(x: 15,y: self.addressContainerView.frame.maxY + 10, width: 50, height: 50))
        self.emeImgView.image = UIImage(named: "ic_emergency")
        self.emeImgView.isUserInteractionEnabled = true
        
        self.view.addSubview(self.emeImgView)
        let emeTapGue = UITapGestureRecognizer()
        emeTapGue.addTarget(self, action: #selector(self.emeImgViewTapped))
        self.emeImgView.addGestureRecognizer(emeTapGue)
        
        if(deliveryConfirmCode != ""){
            self.deliveryConfirmCodeLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_DELIVERY_CONFIRMATION_CODE_TXT") + ": " + deliveryConfirmCode
            self.deliveryConfirmCodeLblHeight.constant = 30
            
            self.deliveryConfirmCodeLbl.backgroundColor = UIColor.UCAColor.AppThemeColor
            self.deliveryConfirmCodeLbl.textColor = UIColor.UCAColor.AppThemeTxtColor
            
            self.driverDetailView.frame.origin.y = self.driverDetailView.frame.origin.y - self.deliveryConfirmCodeLblHeight.constant
            self.driverDetailView.frame.size = CGSize(width: self.driverDetailView.frame.width, height: self.driverDetailView.frame.height + self.deliveryConfirmCodeLblHeight.constant)
            
        }
//        driverDetailView
    }
    
    func emeImgViewTapped(){
        let confirmEmergencyTapUV = GeneralFunctions.instantiateViewController(pageName: "ConfirmEmergencyTapUV") as! ConfirmEmergencyTapUV
        confirmEmergencyTapUV.iTripId = (self.assignedDriverData as NSDictionary).get("iTripId")
        self.pushToNavController(uv: confirmEmergencyTapUV)
    }
    
    func setUpDestination(){

        let window = Application.window
        
        let getUserData = GetUserData(uv: self, window: window!)
        getUserData.getdata()
        
//        if(self.destLocation != nil){
//            self.updateDirection = UpdateDirections(uv: self, gMap: self.gMapView, fromLocation: driverLocation, destinationLocation: self.destLocation, isCurrentLocationEnabled: false)
//            self.updateDirection.scheduleDirectionUpdate()
//        }
    
    }
    
    func openDeliveryDetailsUV(isFromRideLater:Bool){
    
        
    }
    
    func checkTollPrice(isFromRideLater:Bool){
        
        let tollURL = "https://tce.cit.api.here.com/2/calculateroute.json?app_id=\(self.userProfileJson.get("TOLL_COST_APP_ID"))&app_code=\(self.userProfileJson.get("TOLL_COST_APP_CODE"))&waypoint0=\(self.pickUpLocation.coordinate.latitude),\(self.pickUpLocation.coordinate.longitude)&waypoint1=\(self.destLocation.coordinate.latitude),\(self.destLocation.coordinate.longitude)&mode=fastest;car"
        Utils.printLog(msgData: "tollURL:\(tollURL)")
        let exeWebServerUrl = ExeServerUrl(dict_data: [String:String](), currentView: self.view, isOpenLoader: true)
        
        exeWebServerUrl.executeGetProcess(completionHandler: { (response) -> Void in
            
            if(response != ""){
                let dataDict = response.getJsonDataDict()
                
                if(dataDict.get("onError").uppercased() == "FALSE" || dataDict.get("onError") == "0"){
                    
                    self.isTollChecked = true
                    
                    let totalCost = dataDict.getObj("costs").get("totalCost")
                    let currency = dataDict.getObj("costs").get("currency")
                    
                    if(totalCost != "0.0"){
                        
                        let openTollBox = OpenTollBox(uv: self, containerView: self.view)
                        openTollBox.setViewHandler(handler: { (isContinueBtnTapped, isTollSkipped) in
                            if(isContinueBtnTapped){
                                
                                if(isFromRideLater == true){
                                    self.continueRideLaterSchedule(tollPrice: totalCost, tollPriceCurrencyCode: currency, isTollSkipped: isTollSkipped == true ? "Yes" : "No")
                                }else{
                                    self.requestCab(tollPrice: totalCost, tollPriceCurrencyCode: currency, isTollSkipped: isTollSkipped == true ? "Yes" : "No")
                                }
                            }else{
                                self.isTollChecked = false
                            }
                        })
                        openTollBox.show(tollPrice: "\(currency) \(totalCost)")
                    }else{
                        
                        if(isFromRideLater == true){
                            self.continueRideLaterSchedule(tollPrice: "", tollPriceCurrencyCode: "", isTollSkipped: "")
                        }else{
                            self.requestCab(tollPrice: "", tollPriceCurrencyCode: "", isTollSkipped: "")
                        }
                    }
                    
                }else{
                    
                    self.isTollChecked = true
                    
                    if(isFromRideLater == true){
                        self.continueRideLaterSchedule(tollPrice: "", tollPriceCurrencyCode: "", isTollSkipped: "")
                    }else{
                        self.requestCab(tollPrice: "", tollPriceCurrencyCode: "", isTollSkipped: "")
                    }
                }
                
                
            }else{
                
                self.generalFunc.setError(uv: self)
            }
        }, url: tollURL)
    
    }
    
    func requestCab(tollPrice:String, tollPriceCurrencyCode:String, isTollSkipped: String){
        
        if(self.currentCabGeneralType.uppercased() == Utils.cabGeneralType_UberX.uppercased()){
            self.navigationController?.navigationBar.layer.zPosition = -1
        }
        
        currTollPrice = tollPrice
        currTollPriceCurrencyCode = tollPriceCurrencyCode
        currTollSkipped = isTollSkipped
        
        if(isDeliveryDataEntered == false && self.currentCabGeneralType == Utils.cabGeneralType_Deliver){
            openDeliveryDetailsUV(isFromRideLater: false)
            return
        }
        
        //Check For TollRoute
        
        if(isTollChecked == false){
            checkTollPrice(isFromRideLater: false)
            
            return
        }
        
        if(userProfileJson.get("eEmailVerified").uppercased() != "YES" || userProfileJson.get("ePhoneVerified").uppercased() != "YES" ){
            
            self.generalFunc.setAlertMessage(uv: self, title: "", content: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_ACCOUNT_VERIFY_ALERT_RIDER_TXT"), positiveBtn: self.generalFunc.getLanguageLabel(origValue: "Ok", key: "LBL_BTN_OK_TXT"), nagativeBtn: "", completionHandler: { (btnClickedIndex) in
                
                self.openAccountVerify()
                
            })
            
            return
        }
        
        currentLoadedDriverIdsArr.removeAll()
        
        addDriverNotificationObserver()
        
        requestCabView = self.generalFunc.loadView(nibName: "RequestCabView", uv: self, contentView: contentView)
        
        requestCabView.frame = CGRect(x: 0, y: 0, width: Application.screenSize.width, height: self.view.frame.height)
        requestCabView.center = CGPoint(x: self.view.bounds.midX, y: self.view.bounds.midY)
        
        if(self.currentCabGeneralType.uppercased() == Utils.cabGeneralType_UberX.uppercased()){
            requestCabView.frame.size.height = requestCabView.frame.size.height + 64
            requestCabView.frame.origin.y = requestCabView.frame.origin.y - 64
        }

        self.view.addSubview(requestCabView)
        
        let cancelCabReqTapGue = UITapGestureRecognizer()
        cancelCabReqTapGue.addTarget(self, action: #selector(self.cancelCabRequest))
        
        cancelCabReqBgView.isUserInteractionEnabled = true
        cancelCabReqBgView.addGestureRecognizer(cancelCabReqTapGue)
        
        self.requestCabTitleLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_REQUESTING_TXT")
        self.requestCabTitleLbl.fitText()
        self.requestCabTitleLbl.textColor = UIColor.UCAColor.AppThemeTxtColor
        
        self.reqNoteLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_NOTE_NO_DRIVER_REQUEST")
        self.retryReqBtn.setButtonTitle(buttonTitle: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_RETRY_TXT"))
        self.retryReqBtn.clickDelegate = self
        
        self.reqNoteLbl.fitText()
        self.reqNoteLbl.textColor = UIColor.white
        
        requestCabView.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        
        GeneralFunctions.setImgTintColor(imgView: self.cancelCabReqImgView, color: UIColor.UCAColor.AppThemeColor)
        self.cancelCabReqBgView.backgroundColor = UIColor.UCAColor.AppThemeTxtColor
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {
            
            let halo = PulsingHaloLayer()
            halo.haloLayerNumber = 4
            halo.radius = 240.0
            halo.position = CGPoint(x: self.rippleBgView.frame.width / 2, y: self.rippleBgView.frame.height / 2)
            halo.backgroundColor = UIColor.UCAColor.AppThemeColor.cgColor
            self.rippleBgView.layer.addSublayer(halo)
            halo.start()
        })
        
//        self.rippleBgView.haloLayerNumber = 3
//        self.rippleBgView.backgroundColor = UIColor.UCAColor.AppThemeColor.cgColor
//        self.rippleBgView.start()
        
        self.currentLoadedDriverIdsArr = self.getAvailableDriverIds().components(separatedBy: ",")
        
        startDriverRequestQueue()
        
        
        if(self.loadAvailableCab != nil){
            self.loadAvailableCab.onPauseCalled()
        }
        
        
    }
    
    func incCountOfRequestToDriver(){
        if(userProfileJson.get("DRIVER_REQUEST_METHOD").uppercased() != "ALL"){
            startDriverRequestQueue()
        }
    }
    
    func startDriverRequestQueue(){
    
        if(driverRequestQueueTimer != nil){
            driverRequestQueueTimer!.invalidate()
        }
        
        currDriverReqPosition = 0
        
        noDriverAvailView(isHidden: true)
        
        driverRequestQueueTimer =  Timer.scheduledTimer(timeInterval: Double(RIDER_REQUEST_ACCEPT_TIME + 5), target: self, selector: #selector(continueDriverRequestQueue), userInfo: nil, repeats: true)
        
        driverRequestQueueTimer.fire()
        
    }
    
    func stopDriverRequestQueue(){
        if(driverRequestQueueTimer != nil){
            driverRequestQueueTimer!.invalidate()
        }
    }
    
    func continueDriverRequestQueue(){
        
        if(currDriverReqPosition < currentLoadedDriverIdsArr.count){
            
            if(userProfileJson.get("DRIVER_REQUEST_METHOD").uppercased() == "ALL"){
                currDriverReqPosition = currentLoadedDriverIdsArr.count
                self.sendRequestToDrivers(driverIds: self.getAvailableDriverIds(),  tollPrice: self.currTollPrice, tollPriceCurrencyCode: self.currTollPriceCurrencyCode, isTollSkipped: self.currTollSkipped)
                
            }else{
               
                self.sendRequestToDrivers(driverIds: currentLoadedDriverIdsArr[currDriverReqPosition],  tollPrice: self.currTollPrice, tollPriceCurrencyCode: self.currTollPriceCurrencyCode, isTollSkipped: self.currTollSkipped)
                 currDriverReqPosition = currDriverReqPosition + 1
                
            }
        }else{
            stopDriverRequestQueue()
            noDriverAvailView(isHidden: false)
        }
    }
    
    func noDriverAvailView(isHidden:Bool){
        if(requestCabView != nil){
            reqNoteLbl.isHidden = isHidden
            retryReqBtn.isHidden = isHidden
            
            if(isHidden == false){
                
                currDriverReqPosition = 0
                self.requestCabHeaderBarHeight.constant = 150
            }else{
                self.requestCabHeaderBarHeight.constant = 64
            }
        }
    }
    
    func closeCabReqView(){
        
        if(requestCabView != nil){
            stopDriverRequestQueue()
            self.requestCabView.removeFromSuperview()
            requestCabView = nil
        }
    }
    
    
    func cancelCabRequest(){
        
        self.generalFunc.setAlertMessage(uv: self, title: "", content: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_CONFIRM_REQUEST_CANCEL_TXT"), positiveBtn: self.generalFunc.getLanguageLabel(origValue: "Confirm", key: "LBL_BTN_TRIP_CANCEL_CONFIRM_TXT"), nagativeBtn: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_CANCEL_TXT"), completionHandler: { (btnClickedIndex) in
            
            if(btnClickedIndex == 0){
                self.continueCancelCabReq()
            }
        })
        
    }
    
    func continueCancelCabReq(){
        let parameters = ["type":"cancelCabRequest", "iUserId": GeneralFunctions.getMemberd()]
        
        let exeWebServerUrl = ExeServerUrl(dict_data: parameters, currentView: self.view, isOpenLoader: true)
        exeWebServerUrl.executePostProcess(completionHandler: { (response) -> Void in
            
            if(response != ""){
                let dataDict = response.getJsonDataDict()
                
                if(dataDict.get("Action") == "1"){
                    let window = Application.window
                    
                    let getUserData = GetUserData(uv: self, window: window!)
                    getUserData.getdata()
                    
                }else{
                    if(dataDict.get(Utils.message_str) == "DO_RESTART" || dataDict.get("message") == "LBL_SERVER_COMM_ERROR" || dataDict.get("message") == "GCM_FAILED" || dataDict.get("message") == "APNS_FAILED"){
                        let window = Application.window
                        
                        let getUserData = GetUserData(uv: self, window: window!)
                        getUserData.getdata()
                        
                        return
                    }
                    
                    self.generalFunc.setError(uv: self, title: "", content: self.generalFunc.getLanguageLabel(origValue: "", key: dataDict.get("message")))
                }
                
            }else{
                self.generalFunc.setError(uv: self)
            }
        })
    }
    
    func continueRideLaterSchedule(tollPrice:String, tollPriceCurrencyCode:String, isTollSkipped:String){
        
        if(isDeliveryDataEntered == false && self.currentCabGeneralType == Utils.cabGeneralType_Deliver){
            openDeliveryDetailsUV(isFromRideLater: true)
            return
        }
        
        Utils.printLog(msgData: "isTollChecked:\(isTollChecked)")
        if(isTollChecked == false){
            checkTollPrice(isFromRideLater: true)
            return
        }
    
        let window = Application.window
        
        let parameters = ["type":"ScheduleARide", "iUserId": GeneralFunctions.getMemberd(),"pickUpLocAdd":self.pickUpAddress, "pickUpLatitude":"\(self.pickUpLocation!.coordinate.latitude)", "pickUpLongitude": "\(self.pickUpLocation!.coordinate.longitude)", "destLocAdd":self.destAddress,"destLatitude":"\(self.destLocation == nil ? "" : "\(self.destLocation!.coordinate.latitude)")", "destLongitude": "\(self.destLocation == nil ? "" : "\(self.destLocation!.coordinate.longitude)")", "scheduleDate": self.selectedDate, "iVehicleTypeId":selectedCabTypeId, "CashPayment":"\(isCashPayment)","eType":self.currentCabGeneralType,"PromoCode":self.appliedPromoCode, "PickUpAddGeoCodeResult": self.pickUpAddGeoCodeResult.condenseWhitespace(), "DestAddGeoCodeResult": self.destAddGeoCodeResult.condenseWhitespace(), "iPackageTypeId": self.packageTypeId, "vReceiverName": self.receiverName, "vReceiverMobile": self.receiverMobile, "tPickUpIns": self.pickUpIns, "tDeliveryIns": self.deliveryIns, "tPackageDetails": self.packageDetails, "fTollPrice": tollPrice, "vTollPriceCurrencyCode": tollPriceCurrencyCode, "eTollSkipped": isTollSkipped, "Quantity": self.ufxSelectedQty, "SelectedDriverId": self.ufxSelectedServiceProviderId, "HandicapPrefEnabled": self.isHandicapPrefEnabled == true ? "Yes" : "No", "PreferFemaleDriverEnable": self.isPreferFemaleDriverEnable == true ? "Yes" : "No"]
        
//        , "TimeZone": self.selectedTimeZone
        
        let exeWebServerUrl = ExeServerUrl(dict_data: parameters, currentView: self.view, isOpenLoader: true)
        exeWebServerUrl.executePostProcess(completionHandler: { (response) -> Void in
            
            if(response != ""){
                let dataDict = response.getJsonDataDict()
                
                if(dataDict.get("Action") != "1"){
                    if(dataDict.get(Utils.message_str) == "SESSION_OUT"){
                        GeneralFunctions.logOutUser()
                        self.generalFunc.setAlertMessage(uv: self, title: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_SESSION_TIME_OUT"), content: self.generalFunc.getLanguageLabel(origValue: "Your session is expired. Please login again.", key: "LBL_SESSION_TIME_OUT"), positiveBtn: self.generalFunc.getLanguageLabel(origValue: "Ok", key: "LBL_BTN_OK_TXT"), nagativeBtn: "", completionHandler: { (btnClickedIndex) in
                            
                            GeneralFunctions.postNotificationSignal(key: Utils.releaseAllTaskObserverKey, obj: self)
                            
                            GeneralFunctions.restartApp(window: window!)
                        })
                        
                        return
                    }
                    
                    if(dataDict.get(Utils.message_str) == "DO_RESTART" || dataDict.get("message") == "LBL_SERVER_COMM_ERROR" || dataDict.get("message") == "GCM_FAILED" || dataDict.get("message") == "APNS_FAILED"){
                        let window = Application.window
                        
                        let getUserData = GetUserData(uv: self, window: window!)
                        getUserData.getdata()
                        
                        return
                    }
                    
                    if(dataDict.get(Utils.message_str) == "NO_CARS" || dataDict.get(Utils.message_str) == "LBL_PICK_DROP_LOCATION_NOT_ALLOW" || dataDict.get(Utils.message_str) == "LBL_DROP_LOCATION_NOT_ALLOW" || dataDict.get(Utils.message_str) == "LBL_PICKUP_LOCATION_NOT_ALLOW"){
                        
                        self.generalFunc.setAlertMessage(uv: self, title: "", content: self.generalFunc.getLanguageLabel(origValue: "", key: dataDict.get(Utils.message_str) == "NO_CARS" ? "LBL_NO_CAR_AVAIL_TXT" : dataDict.get(Utils.message_str)), positiveBtn: self.generalFunc.getLanguageLabel(origValue: "Ok", key: "LBL_BTN_OK_TXT"), nagativeBtn: "", completionHandler: { (btnClickedIndex) in
                            
                            self.closeCabReqView()
                            self.removeAddReqPickUpView()
                            
                        })
                        
                        return
                    }
                    
//                    if(dataDict.get(Utils.message_str) == "NO_CARS"){
//                        
//                        self.generalFunc.setAlertMessage(uv: self, title: "", content: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_NO_CAR_AVAIL_TXT"), positiveBtn: self.generalFunc.getLanguageLabel(origValue: "Ok", key: "LBL_BTN_OK_TXT"), nagativeBtn: "", completionHandler: { (btnClickedIndex) in
//                            
//                            self.closeCabReqView()
//                            self.removeAddReqPickUpView()
//                            
//                        })
//                        
//                        return
//                    }
                    
                    if(dataDict.get(Utils.message_str) == "DO_EMAIL_PHONE_VERIFY" || dataDict.get(Utils.message_str) == "DO_PHONE_VERIFY" || dataDict.get(Utils.message_str) == "DO_EMAIL_VERIFY"){
                        self.generalFunc.setAlertMessage(uv: self, title: "", content: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_ACCOUNT_VERIFY_ALERT_RIDER_TXT"), positiveBtn: self.generalFunc.getLanguageLabel(origValue: "Ok", key: "LBL_BTN_OK_TXT"), nagativeBtn: "", completionHandler: { (btnClickedIndex) in
                            
                            let accountVerificationUv = GeneralFunctions.instantiateViewController(pageName: "AccountVerificationUV") as! AccountVerificationUV
                            accountVerificationUv.isMainScreen = true
                            accountVerificationUv.mainScreenUv = self
                            accountVerificationUv.requestType = dataDict.get(Utils.message_str)
                            self.pushToNavController(uv: accountVerificationUv)
                            
                        })
                    }
                    
                    
                }else{
                    self.loadBookingFinishView()
                }
                
            }else{
                self.generalFunc.setError(uv: self)
            }
        })
        
    }
    
    func loadBookingFinishView(){
        if(self.currentCabGeneralType == Utils.cabGeneralType_UberX){
            self.releaseAllTask()
            self.performSegue(withIdentifier: "unwindToUFXHomeScreen", sender: self)
            return
        }
        let openBookingFinishedView = OpenBookingFinishedView(uv: self, containerView: self.contentView)
        openBookingFinishedView.currentInst = openBookingFinishedView
        openBookingFinishedView.show()
        
        if(self.requestPickUpView != nil){
            removeAddReqPickUpView()
        }
        
    }
    
    
    
    func sendRequestToDrivers(driverIds:String, tollPrice: String, tollPriceCurrencyCode: String, isTollSkipped: String){
        var driverIds = driverIds
        if(self.currentCabGeneralType.uppercased() == Utils.cabGeneralType_UberX.uppercased()){
            driverIds = self.ufxSelectedServiceProviderId
            
        }
        let window = Application.window
        
        let parameters = ["type":"sendRequestToDrivers", "driverIds": driverIds, "userId": GeneralFunctions.getMemberd(), "CashPayment": "\(isCashPayment)", "SelectedCarTypeID": selectedCabTypeId, "DestLatitude": "\(self.destLocation != nil ? "\(self.destLocation!.coordinate.latitude)" : "")", "DestLongitude": "\(self.destLocation != nil ? "\(self.destLocation!.coordinate.longitude)" : "")", "DestAddress": self.destAddress, "PickUpLatitude": "\(self.pickUpLocation!.coordinate.latitude)", "PickUpLongitude": "\(self.pickUpLocation!.coordinate.longitude)", "eType": self.currentCabGeneralType, "PromoCode": self.appliedPromoCode,"PickUpAddress": self.pickUpAddress, "PickUpAddGeoCodeResult": self.pickUpAddGeoCodeResult.condenseWhitespace(), "DestAddGeoCodeResult": self.destAddGeoCodeResult.condenseWhitespace(), "iPackageTypeId": self.packageTypeId, "vReceiverName": self.receiverName, "vReceiverMobile": self.receiverMobile, "tPickUpIns": self.pickUpIns, "tDeliveryIns": self.deliveryIns, "tPackageDetails": self.packageDetails, "fTollPrice": tollPrice, "vTollPriceCurrencyCode": tollPriceCurrencyCode, "eTollSkipped": isTollSkipped, "Quantity": self.ufxSelectedQty]
        
//        , "TimeZone": "\(DateFormatter().timeZone.identifier)"
        
        let exeWebServerUrl = ExeServerUrl(dict_data: parameters, currentView: self.view, isOpenLoader: false)
        exeWebServerUrl.executePostProcess(completionHandler: { (response) -> Void in
            
//            Utils.printLog(msgData: "response:\(response)")
            
            if(response != ""){
                let dataDict = response.getJsonDataDict()
                
                if(dataDict.get("Action") != "1"){
                    if(dataDict.get(Utils.message_str) == "SESSION_OUT"){
                        GeneralFunctions.logOutUser()
                        self.generalFunc.setAlertMessage(uv: self, title: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_SESSION_TIME_OUT"), content: self.generalFunc.getLanguageLabel(origValue: "Your session is expired. Please login again.", key: "LBL_SESSION_TIME_OUT"), positiveBtn: self.generalFunc.getLanguageLabel(origValue: "Ok", key: "LBL_BTN_OK_TXT"), nagativeBtn: "", completionHandler: { (btnClickedIndex) in
                            
                            GeneralFunctions.postNotificationSignal(key: Utils.releaseAllTaskObserverKey, obj: self)
                            
                            GeneralFunctions.restartApp(window: window!)
                        })
                        
                        return
                    }
                    
                    if(dataDict.get(Utils.message_str) == "DO_RESTART" || dataDict.get("message") == "LBL_SERVER_COMM_ERROR" || dataDict.get("message") == "GCM_FAILED" || dataDict.get("message") == "APNS_FAILED"){
                        let window = Application.window
                        
                        let getUserData = GetUserData(uv: self, window: window!)
                        getUserData.getdata()
                        
                        return
                    }

                    
                    if(dataDict.get(Utils.message_str) == "NO_CARS" || dataDict.get(Utils.message_str) == "LBL_PICK_DROP_LOCATION_NOT_ALLOW" || dataDict.get(Utils.message_str) == "LBL_DROP_LOCATION_NOT_ALLOW" || dataDict.get(Utils.message_str) == "LBL_PICKUP_LOCATION_NOT_ALLOW"){
                        
                        self.generalFunc.setAlertMessage(uv: self, title: "", content: self.generalFunc.getLanguageLabel(origValue: "", key: dataDict.get(Utils.message_str) == "NO_CARS" ? "LBL_NO_CAR_AVAIL_TXT" : dataDict.get(Utils.message_str)), positiveBtn: self.generalFunc.getLanguageLabel(origValue: "Ok", key: "LBL_BTN_OK_TXT"), nagativeBtn: "", completionHandler: { (btnClickedIndex) in
                            
                            self.closeCabReqView()
                            self.removeAddReqPickUpView()
                            
                        })
                        
                        return
                    }
                    
                    if(dataDict.get(Utils.message_str) == "DO_EMAIL_PHONE_VERIFY" || dataDict.get(Utils.message_str) == "DO_PHONE_VERIFY" || dataDict.get(Utils.message_str) == "DO_EMAIL_VERIFY"){
                        self.generalFunc.setAlertMessage(uv: self, title: "", content: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_ACCOUNT_VERIFY_ALERT_RIDER_TXT"), positiveBtn: self.generalFunc.getLanguageLabel(origValue: "Ok", key: "LBL_BTN_OK_TXT"), nagativeBtn: "", completionHandler: { (btnClickedIndex) in
                            
                            self.closeCabReqView()
                            self.removeAddReqPickUpView()
                            
                            let accountVerificationUv = GeneralFunctions.instantiateViewController(pageName: "AccountVerificationUV") as! AccountVerificationUV
                            accountVerificationUv.isMainScreen = true
                            accountVerificationUv.mainScreenUv = self
                            accountVerificationUv.requestType = dataDict.get(Utils.message_str)
                            self.pushToNavController(uv: accountVerificationUv)
                            
                        })
                    }

                    
                }
//                else{
//                    self.generalFunc.setError(uv: self, title: "", content: self.generalFunc.getLanguageLabel(origValue: "", key: dataDict.get("message")))
//                }
                
            }else{
//                self.generalFunc.setError(uv: self)
                
                if(self.userProfileJson.get("DRIVER_REQUEST_METHOD").uppercased() == "ALL"){
                    self.currDriverReqPosition = 0
                }else if(self.currDriverReqPosition > 0){
                    self.currDriverReqPosition = self.currDriverReqPosition - 1
                }
            }
        })
    }
    
    func openManageProfile(isOpenEditProfile: Bool){
        let manageProfileUv = GeneralFunctions.instantiateViewController(pageName: "ManageProfileUV") as! ManageProfileUV
        manageProfileUv.isOpenEditProfile = isOpenEditProfile
//        (self.navigationDrawerController?.rootViewController as! UINavigationController).pushViewController(manageProfileUv, animated: true)
        self.pushToNavController(uv: manageProfileUv)
    }
    
    func setTripLocation(selectedAddress:String, selectedLocation:CLLocation){
        if(self.isPickUpMode){
            addressContainerView.pickUpAddressLbl.text = selectedAddress
            self.pickUpAddress = selectedAddress
            self.pickUpLocation = selectedLocation
            if(self.loadAvailableCab != nil){
                self.loadAvailableCab.setPickUpLocation(pickUpLocation: selectedLocation)
            }
             addressContainerView.goToPickLoc()
            
            sourcePinImgView.image = UIImage(named: "ic_pin_source")
            
            sourcePinImgViewWidth.constant = 80
            sourcePinImgViewHeight.constant = 80
            sourcePinImgViewOffset.constant = -40
            
            if(self.loadAvailableCab != nil && requestCabView == nil){
                self.loadAvailableCab.onResumeCalled()
            }
        }else{
            self.destLocation = selectedLocation
            self.destAddress = selectedAddress
            
            addressContainerView.destAddressLbl.text = selectedAddress
            
            addressContainerView.goToDestLoc()
            
            sourcePinImgViewWidth.constant = 50
            sourcePinImgViewHeight.constant = 50
            sourcePinImgViewOffset.constant = -25
            
            sourcePinImgView.image = UIImage(named: "ic_pin_dest_selection")
        }
        
        if(self.destLocation != nil && requestPickUpView == nil ){
            self.openRequestPickUpView(isFromRideLater: false)
        }
        
        
//        if(self.addressContainerView != nil){
//            if(sourcePinImgView.isHidden == false){
//                sourcePinImgView.image = UIImage(named: "ic_pin_dest_selection")
//                self.addressContainerView.goToDestLoc()
//            }
//        }
        
        if(requestPickUpView != nil){
            self.cabTypesFareArr.removeAll()
            self.cabTypeCollectionView.reloadData()
            estimateFare()
        }
    }
    
    func continueLocationSelected(selectedLocation:CLLocation, selectedAddress:String){
        if(self.isPickUpMode){
            addressContainerView.pickUpAddressLbl.text = selectedAddress
            self.pickUpAddress = selectedAddress
            self.pickUpLocation = selectedLocation
            if(self.loadAvailableCab != nil){
                self.loadAvailableCab.setPickUpLocation(pickUpLocation: selectedLocation)
            }
            addressContainerView.goToPickLoc()
        }else{
            self.destLocation = selectedLocation
            self.destAddress = selectedAddress
            
            addressContainerView.destAddressLbl.text = selectedAddress
            addressContainerView.goToDestLoc()
        }
        
        if(self.destLocation != nil && requestPickUpView == nil){
            self.openRequestPickUpView(isFromRideLater: false)
        }
        
        if(self.addressContainerView != nil){
            if(sourcePinImgView.isHidden == false){
                sourcePinImgView.image = UIImage(named: "ic_pin_dest_selection")
                self.addressContainerView.goToDestLoc()
            }
        }
        
        if(requestPickUpView != nil){
            self.cabTypesFareArr.removeAll()
            self.cabTypeCollectionView.reloadData()
            estimateFare()
        }
    }
    
    @IBAction func unwindToMainScreen(_ segue:UIStoryboardSegue) {
        //        unwindToSignUp
        
        if(segue.source.isKind(of: AddDestinationUV.self))
        {
            
          let addDestinationUv = segue.source as! AddDestinationUV
            let selectedLocation = addDestinationUv.selectedLocation
            let selectedAddress = addDestinationUv.selectedAddress
            
            if(addDestinationUv.isFromRecentLocView == true && self.recentLocView != nil){
                self.recentLocView.setSelectedLocations(latitude: selectedLocation!.coordinate.latitude, longitude: selectedLocation!.coordinate.longitude, address: selectedAddress, type: addDestinationUv.SCREEN_TYPE)
                //                return
            }
            
            continueLocationSelected(selectedLocation: selectedLocation!, selectedAddress: selectedAddress)
            
        }else if(segue.source.isKind(of: AccountVerificationUV.self)){
            _ = segue.source as! AccountVerificationUV
            
        }        
    }
}

extension MainScreenUV: VerifyRequestDelegate {
    func verifyRequested(_ code: String, reference: String, status: String, amount: String) {
        self.continueCancelTrip(status: status, reference: reference, code: code)
    }
}
