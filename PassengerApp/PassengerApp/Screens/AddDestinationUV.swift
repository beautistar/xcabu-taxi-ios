//
//  AddDestinationUV.swift
//  PassengerApp
//
//  Created by NEW MAC on 31/05/17.
//  Copyright © 2017 V3Cube. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation

class AddDestinationUV: UIViewController, GMSMapViewDelegate, OnLocationUpdateDelegate, AddressFoundDelegate, MyBtnClickDelegate {
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var googleMapContainerView: UIView!
    @IBOutlet weak var addLocationBtn: MyButton!
    @IBOutlet weak var locAreaView: UIView!
    @IBOutlet weak var locLbl: MyLabel!
    
    var centerLocation:CLLocation!
    
    var SCREEN_TYPE = "DESTINATION"
    
    let generalFunc = GeneralFunctions()
    
    var isFirstLocationUpdate = true
    var isPageLoaded = false
    var gMapView:GMSMapView!
    
    var isFromRecentLocView = false
    var isFromSearchPlaces = false
    
    var getLocation:GetLocation!
    
    var getAddressFrmLocation:GetAddressFromLocation!
    
    var selectedLocation:CLLocation!
    var selectedAddress = ""
    
    let placeMarker: GMSMarker = GMSMarker()
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.configureRTLView()
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.contentView.addSubview(self.generalFunc.loadView(nibName: "AddDestinationScreenDesign", uv: self, contentView: contentView))
        
        self.addBackBarBtn()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if(isPageLoaded == false){
            
            isPageLoaded = true
            
            
            let camera = GMSCameraPosition.camera(withLatitude: 0.0, longitude: 0.0, zoom: 0.0)
            gMapView = GMSMapView.map(withFrame: self.googleMapContainerView.frame, camera: camera)
            //        googleMapContainerView = gMapView
            //        gMapView = GMSMapView()
            //            gMapView.isMyLocationEnabled = true
            gMapView.delegate = self
            self.googleMapContainerView.addSubview(gMapView)
            
            setData()
        }
    }
    
    func setData(){
        if(SCREEN_TYPE == "DESTINATION"){
            self.navigationItem.title = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_SELECT_DESTINATION_HEADER_TXT")
            self.title = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_SELECT_DESTINATION_HEADER_TXT")
        }else if(SCREEN_TYPE == "PICKUP"){
            self.navigationItem.title = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_SET_PICK_UP_LOCATION_TXT")
            self.title = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_SET_PICK_UP_LOCATION_TXT")
        }else if(SCREEN_TYPE == "HOME"){
            self.navigationItem.title = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_ADD_HOME_BIG_TXT")
            self.title = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_ADD_HOME_BIG_TXT")
        }else if(SCREEN_TYPE == "WORK"){
            self.navigationItem.title = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_ADD_WORK_HEADER_TXT")
            self.title = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_ADD_WORK_HEADER_TXT")
        }else{
            self.navigationItem.title = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_ADD_LOC")
            self.title = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_ADD_LOC")
        }
        
        
        self.locLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_SEARCH_PLACE_HINT_TXT")
        
        self.addLocationBtn.setButtonTitle(buttonTitle: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_ADD_LOC"))
        self.addLocationBtn.clickDelegate = self
        //        self.addLocationBtn.unwindToActiveTrip
        
        getAddressFrmLocation = GetAddressFromLocation(uv: self, addressFoundDelegate: self)
        
        if(centerLocation == nil){
            self.getLocation = GetLocation(uv: self, isContinuous: true)
            self.getLocation.buildLocManager(locationUpdateDelegate: self)
        }else{
            getAddressFrmLocation.setLocation(latitude: centerLocation!.coordinate.latitude, longitude: centerLocation!.coordinate.longitude)
            getAddressFrmLocation.executeProcess(isOpenLoader: true, isAlertShow: true)
            
            self.animateGmapCamera(location: centerLocation!)
        }
        
        Utils.showSnakeBar(msg: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_LONG_TOUCH_CHANGE_LOC_TXT"), uv: self)
        
        let placeTapGue = UITapGestureRecognizer()
        placeTapGue.addTarget(self, action: #selector(self.launchPlaceFinder))
        
        locAreaView.isUserInteractionEnabled = true
        locAreaView.addGestureRecognizer(placeTapGue)
    }
    
    func launchPlaceFinder(){
        let launchPlaceFinder = LaunchPlaceFinder(viewControllerUV: self)
        launchPlaceFinder.currInst = launchPlaceFinder
        if(centerLocation != nil){
            launchPlaceFinder.setBiasLocation(sourceLocationPlaceLatitude: centerLocation!.coordinate.latitude, sourceLocationPlaceLongitude: centerLocation!.coordinate.longitude)
        }
        
        launchPlaceFinder.initializeFinder { (address, latitude, longitude) in
            self.locLbl.text = address
            self.selectedAddress = address
            self.selectedLocation = CLLocation(latitude: latitude, longitude: longitude)
        }
    }
    
    override func closeCurrentScreen() {
        releaseAllTask()
        super.closeCurrentScreen()
    }
    deinit {
        releaseAllTask()
    }
    
    func releaseAllTask(isDismiss:Bool = true){
        
        if(gMapView != nil){
            gMapView!.stopRendering()
            gMapView!.removeFromSuperview()
            gMapView!.clear()
            gMapView!.delegate = nil
            gMapView = nil
        }
        
        
        if(self.getLocation != nil){
            self.getLocation!.locationUpdateDelegate = nil
            self.getLocation!.releaseLocationTask()
            self.getLocation = nil
        }
        
        if(getAddressFrmLocation != nil){
            getAddressFrmLocation!.addressFoundDelegate = nil
            getAddressFrmLocation = nil
        }
        
        GeneralFunctions.removeObserver(obj: self)
        
        
        if(isDismiss){
            self.dismiss(animated: false, completion: nil)
            self.navigationController?.dismiss(animated: false, completion: nil)
        }
    }
    
    func mapView(_ mapView: GMSMapView, didLongPressAt coordinate: CLLocationCoordinate2D) {
        getAddressFrmLocation.setLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
        getAddressFrmLocation.executeProcess(isOpenLoader: true, isAlertShow: true)
        
//        self.animateGmapCamera(location: CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude))
    }
    
    func onLocationUpdate(location: CLLocation) {
        if(gMapView == nil){
            releaseAllTask()
            return
        }
        
        
        if(isFirstLocationUpdate){
            getAddressFrmLocation.setLocation(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
            getAddressFrmLocation.executeProcess(isOpenLoader: true, isAlertShow: true)
            
            self.animateGmapCamera(location: location)
        }
        
        isFirstLocationUpdate = false
        
    }
    
    func onAddressFound(address: String, location:CLLocation, isPickUpMode: Bool, dataResult:String) {
        self.locLbl.text = address
        self.selectedAddress = address
        self.selectedLocation = location
        
        
        changeMarkerPosition(location: location)
    }
    
    func changeMarkerPosition(location:CLLocation){
        placeMarker.position = location.coordinate
        
        placeMarker.icon = UIImage(named: "ic_destination_place_image")
        placeMarker.map = self.gMapView
        placeMarker.infoWindowAnchor = CGPoint(x: 0.5, y: 0.5)
        
        self.animateGmapCamera(location: location)
    }
    
    func animateGmapCamera(location:CLLocation){
        var currentZoomLevel:Float = self.gMapView.camera.zoom
        
        if(currentZoomLevel < 15.0 && isFirstLocationUpdate == true){
            currentZoomLevel = 15.0
        }
        let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude,
                                              longitude: location.coordinate.longitude, zoom: currentZoomLevel)
        self.gMapView.moveCamera(GMSCameraUpdate.setCamera(camera))
//        self.gMapView.animate(to: camera)
    }
    
    func myBtnTapped(sender: MyButton) {
        
        if(sender == self.addLocationBtn){
            if(self.selectedLocation == nil){
                Utils.showSnakeBar(msg: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_SET_LOCATION"), uv: self)
            }else{
                releaseAllTask()
                if(self.isFromSearchPlaces == true){
                    self.performSegue(withIdentifier: "unwindToSearchPlaceScreen", sender: self)
                }else if(self.isFromRecentLocView == false && (self.SCREEN_TYPE == "HOME" || self.SCREEN_TYPE == "WORK")){
                    self.performSegue(withIdentifier: "unwindToViewProfileScreen", sender: self)
                }else{
                    self.performSegue(withIdentifier: "unwindToMainScreen", sender: self)
                }
            }
        }
    }
    
}
