//
//  RatingUV.swift
//  PassengerApp
//
//  Created by NEW MAC on 01/06/17.
//  Copyright © 2017 V3Cube. All rights reserved.
//

import UIKit

class RatingUV: UIViewController, MyBtnClickDelegate, MyLabelClickDelegate {
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var totalFareHLbl: MyLabel!
    @IBOutlet weak var totalFareVLbl: MyLabel!
    @IBOutlet weak var billDateHLbl: MyLabel!
    @IBOutlet weak var billDateVLbl: MyLabel!
    @IBOutlet weak var dashedView: UIView!
    @IBOutlet weak var billDateVlblXposition: NSLayoutConstraint!
    @IBOutlet weak var billDateVlblWidth: NSLayoutConstraint!
    @IBOutlet weak var tripGeneralInfoLbl: MyLabel!
    @IBOutlet weak var discountHLbl: MyLabel!
    @IBOutlet weak var discountVLbl: MyLabel!
    @IBOutlet weak var sourceAddressLbl: MyLabel!
    @IBOutlet weak var destAddLbl: MyLabel!
    @IBOutlet weak var howWasLbl: MyLabel!
    @IBOutlet weak var ratingBar: RatingView!
    @IBOutlet weak var commentTxtView: KMPlaceholderTextView!
    @IBOutlet weak var submitBtn: MyButton!
    @IBOutlet weak var addressContainerView: UIView!
    @IBOutlet weak var addressContainerViewTopMargin: NSLayoutConstraint!
    @IBOutlet weak var addressContainerViewHeight: NSLayoutConstraint!
    @IBOutlet weak var rateContainerView: UIView!
    @IBOutlet weak var endLocPinImgView: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    // Give Tip OutLets
    @IBOutlet weak var giveTipHLbl: MyLabel!
    @IBOutlet weak var giveTipNoteLbl: MyLabel!
    @IBOutlet weak var giveTipPLbl: MyLabel!
    @IBOutlet weak var giveTipNLbl: MyLabel!
    @IBOutlet weak var enterTipTxtField: MyTextField!
    
    var currentTipMode = "OFF"
    
    let generalFunc = GeneralFunctions()
    
    var loaderView:UIView!
    
    var isPageLoad = false
    var window:UIWindow!
    
    var iTripId = ""
    
    var cntView:UIView!
    
    var PAGE_HEIGHT:CGFloat = 700
    var ENABLE_TIP_MODULE = ""
    
    var giveTipView:UIView!
    var bgTipView:UIView!
    
    
    var tripFinishView:UIView!
    var tripFinishBGView:UIView!
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.configureRTLView()
        self.navigationController?.navigationBar.layer.zPosition = -1

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        //        self.navigationController?.navigationBar.clipsToBounds = false
        self.navigationController?.navigationBar.layer.zPosition = 0
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
         window = Application.window!
        
        cntView = self.generalFunc.loadView(nibName: "RatingScreenDesign", uv: self, contentView: scrollView)
        
        self.scrollView.addSubview(cntView)
        
        scrollView.bounces = false
//        self.addBackBarBtn()
        
        //        setData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if(isPageLoad == false){

//            self.contentView.addSubview(scrollView)
            
            if((Application.screenSize.height - 64) > PAGE_HEIGHT){
                PAGE_HEIGHT = Application.screenSize.height - 64
            }
            cntView.frame.size = CGSize(width: cntView.frame.width, height: PAGE_HEIGHT)
            self.scrollView.bounces = false
            //            self.scrollView.setContentViewSize(offset: 15, currentMaxHeight: self.scrollViewCOntentViewHeight.constant)
            self.scrollView.contentSize = CGSize(width: self.scrollView.contentSize.width, height: PAGE_HEIGHT)
            
            
            scrollView.bounces = false
            scrollView.backgroundColor = UIColor.clear
            
            self.billDateVlblWidth.constant = (Application.screenSize.width * 55) / 100

            if((Application.screenSize.width / 2) < self.billDateVlblWidth.constant){
                if(Configurations.isRTLMode()){
                    self.billDateVlblXposition.constant = -(self.billDateVlblWidth.constant - (Application.screenSize.width / 2))
                }else{
                    self.billDateVlblXposition.constant = self.billDateVlblWidth.constant - (Application.screenSize.width / 2)
                }
            }else{
                self.billDateVlblXposition.constant = 0
            }
            
            
            setData()
            
            isPageLoad = true
        }
    }
    
    func setData(){
        self.navigationItem.title = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_RATING")
        self.title = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_RATING")
        
        addressContainerView.layer.shadowOpacity = 0.5
        addressContainerView.layer.shadowOffset = CGSize(width: 0, height: 6)
        addressContainerView.layer.shadowColor = UIColor(hex: 0xe6e6e6).cgColor
        
        rateContainerView.layer.shadowOpacity = 0.5
        rateContainerView.layer.shadowOffset = CGSize(width: 0, height: 6)
        rateContainerView.layer.shadowColor = UIColor(hex: 0xe6e6e6).cgColor
        
        tripGeneralInfoLbl.layer.shadowOpacity = 0.5
        tripGeneralInfoLbl.layer.shadowOffset = CGSize(width: 0, height: 6)
        tripGeneralInfoLbl.layer.shadowColor = UIColor(hex: 0xe6e6e6).cgColor
        
        self.submitBtn.setButtonTitle(buttonTitle: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_BTN_SUBMIT_TXT"))
        
        self.totalFareHLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_Total_Fare_TXT").uppercased()
        self.billDateHLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_TRIP_DATE_TXT").uppercased()
        self.discountHLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_DIS_APPLIED").uppercased()
        
        self.howWasLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_HOW_WAS_RIDE")
        
        self.commentTxtView.placeholder = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_WRITE_COMMENT_HINT_TXT")
        
        self.submitBtn.clickDelegate = self
        
        Utils.createRoundedView(view: addressContainerView, borderColor: UIColor.clear, borderWidth: 0, cornerRadius: 10)
        Utils.createRoundedView(view: rateContainerView, borderColor: UIColor.clear, borderWidth: 0, cornerRadius: 10)

        
        GeneralFunctions.setImgTintColor(imgView: self.endLocPinImgView, color: UIColor(hex: 0xFF0000))
        
        getTripData()
        
        self.headerView.backgroundColor = UIColor.UCAColor.AppThemeColor
    }
    
    func myBtnTapped(sender: MyButton) {
        if(sender == self.submitBtn){
            
            if(self.ratingBar.rating < 1.0){
                Utils.showSnakeBar(msg: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_ERROR_RATING_DIALOG_TXT"), uv: self)
                
                return
            }
            
//            submitRating()
            if(self.ENABLE_TIP_MODULE == "Yes"){
                self.loadTipView()
            }else{
                submitRating(fAmount: "", isCollectTip: "No")
            }
        }
    }
    
    func getTripData(){
        scrollView.isHidden = true
        loaderView =  self.generalFunc.addMDloader(contentView: self.view)
        loaderView.backgroundColor = UIColor.clear
        
        let parameters = ["type":"displayFare","iMemberId": GeneralFunctions.getMemberd(), "UserType": Utils.appUserType]
        
        let exeWebServerUrl = ExeServerUrl(dict_data: parameters, currentView: self.view, isOpenLoader: false)
        exeWebServerUrl.setDeviceTokenGenerate(isDeviceTokenGenerate: true)
        exeWebServerUrl.currInstance = exeWebServerUrl
        exeWebServerUrl.executePostProcess(completionHandler: { (response) -> Void in
            
            if(response != ""){
                let dataDict = response.getJsonDataDict()
                
                if(dataDict.get("Action") == "1"){
                    
                    self.totalFareVLbl.text = Configurations.convertNumToAppLocal(numStr: dataDict.getObj(Utils.message_str).get("FareSubTotal"))
                    self.billDateVLbl.text = Utils.convertDateFormateInAppLocal(date: Utils.convertDateGregorianToAppLocale(date: dataDict.getObj(Utils.message_str).get("tStartDate"), dateFormate: "yyyy-MM-dd HH:mm:ss"), toDateFormate: Utils.dateFormateWithTime)
                    
                    let fDiscount = dataDict.getObj(Utils.message_str).get("fDiscount")
                    let CurrencySymbol = dataDict.getObj(Utils.message_str).get("CurrencySymbol")
                    _ = dataDict.getObj(Utils.message_str).get("vTripPaymentMode")
                    
                    let eCancelled = dataDict.getObj(Utils.message_str).get("eCancelled")
                    let vCancelReason = dataDict.getObj(Utils.message_str).get("vCancelReason")
//                    self.vehicleTypeLbl.text = dataDict.getObj(Utils.message_str).get("carTypeName")
                    
                    if (fDiscount != "" && fDiscount != "0" && fDiscount != "0.00") {
                        self.discountVLbl.text = CurrencySymbol + Configurations.convertNumToAppLocal(numStr: fDiscount)
                    }else{
                        self.discountVLbl.text = "--"
                    }
                    
                    self.ENABLE_TIP_MODULE = dataDict.getObj(Utils.message_str).get("ENABLE_TIP_MODULE")
                    
                    Utils.printLog(msgData: "ENABLE_TIP_MODULE::\(self.ENABLE_TIP_MODULE)")
                    
                    self.sourceAddressLbl.text = dataDict.getObj(Utils.message_str).get("tSaddress")
                    self.destAddLbl.text = dataDict.getObj(Utils.message_str).get("tDaddress")
                    self.sourceAddressLbl.fitText()
                    self.destAddLbl.fitText()
                    
                    let sourceAddHeight = dataDict.getObj(Utils.message_str).get("tSaddress").height(withConstrainedWidth: Application.screenSize.width - 50, font: UIFont(name: "Roboto-Light", size: 16)!)
                    
                    let destAddHeight = dataDict.getObj(Utils.message_str).get("tDaddress").height(withConstrainedWidth: Application.screenSize.width - 50, font: UIFont(name: "Roboto-Light", size: 16)!)
                    
                    self.addressContainerViewHeight.constant = self.addressContainerViewHeight.constant + sourceAddHeight + destAddHeight + 10
                    
                    self.iTripId = dataDict.getObj(Utils.message_str).get("iTripId")
                    
//                    self.tripGeneralInfoLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_PAYMENT_TYPE_TXT") + ": "
//                    
//                    if(vTripPaymentMode == "Cash"){
//                        self.tripGeneralInfoLbl.text = self.tripGeneralInfoLbl.text! + self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_CASH_TXT")
//                        self.tripGeneralInfoLbl.text = self.tripGeneralInfoLbl.text! + "\n" + self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_COLLECT_MONEY_FRM_RIDER")
//                    }else{
//                        self.tripGeneralInfoLbl.text = self.tripGeneralInfoLbl.text! + self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_CARD_TXT")
//                        self.tripGeneralInfoLbl.text = self.tripGeneralInfoLbl.text! + "\n" + self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_DEDUCTED_RIDER_CARD")
//                    }
                    
                    if(eCancelled == "Yes"){
                        self.tripGeneralInfoLbl.text = self.generalFunc.getLanguageLabel(origValue: "Trip is cancelled by driver. Reason:", key: "LBL_PREFIX_TRIP_CANCEL_DRIVER") + " \(vCancelReason)"
                        self.tripGeneralInfoLbl.isHidden = false
                        self.tripGeneralInfoLbl.fitText()
                    }else{
                        self.tripGeneralInfoLbl.isHidden = true
                        self.addressContainerViewTopMargin.constant = self.addressContainerViewTopMargin.constant - 55
                        self.PAGE_HEIGHT = self.PAGE_HEIGHT - 40
                    }
                    
//                    self.tripGeneralInfoLbl.fitText()
                    
                    
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {
                        
                        self.dashedView.addDashedLine(color: UIColor(hex: 0xADADAD), lineWidth: 2)
                        self.PAGE_HEIGHT = (Application.screenSize.height - 64) > (self.submitBtn.frame.maxY + 25) ? (Application.screenSize.height - 64) : (self.submitBtn.frame.maxY + 25)
                        
                        self.cntView.frame.size = CGSize(width: self.cntView.frame.width, height: self.PAGE_HEIGHT)

                        self.scrollView.contentSize = CGSize(width: self.scrollView.contentSize.width, height: self.PAGE_HEIGHT)
                    })
                    
                    self.loaderView.isHidden = true
                    self.scrollView.isHidden = false
                }else{
                    self.generalFunc.setError(uv: self, title: "", content: self.generalFunc.getLanguageLabel(origValue: "", key: dataDict.get("message")))
                }
                
            }else{
                self.generalFunc.setError(uv: self)
            }
            
            
        })
    }
    
    func submitRating(fAmount:String, isCollectTip:String){
        
        if(bgTipView != nil){
            bgTipView.removeFromSuperview()
        }
        
        if(giveTipView != nil){
            giveTipView.removeFromSuperview()
        }
        
        let parameters = ["type":"submitRating","iMemberId": GeneralFunctions.getMemberd(), "UserType": Utils.appUserType, "tripID": iTripId, "rating": "\(self.ratingBar.rating)", "message": "\(commentTxtView.text!)", "fAmount": fAmount, "isCollectTip": isCollectTip]
        
        let exeWebServerUrl = ExeServerUrl(dict_data: parameters, currentView: self.view, isOpenLoader: true)
        exeWebServerUrl.setDeviceTokenGenerate(isDeviceTokenGenerate: true)
        exeWebServerUrl.currInstance = exeWebServerUrl
        exeWebServerUrl.executePostProcess(completionHandler: { (response) -> Void in
            
            if(response != ""){
                let dataDict = response.getJsonDataDict()
                
                if(dataDict.get("Action") == "1"){
                    
                    self.loadTripFinishView()
                    
                }else{
                    self.generalFunc.setError(uv: self, title: "", content: self.generalFunc.getLanguageLabel(origValue: "", key: dataDict.get("message")))
                }
                
            }else{
                self.generalFunc.setError(uv: self)
            }
        })
    }
    

    func loadTripFinishView(){
        let tripFinishView = self.generalFunc.loadView(nibName: "TripFinishView", uv: self, isWithOutSize: true)
        
        self.tripFinishView = tripFinishView
        
        let width = Application.screenSize.width  > 380 ? 370 : Application.screenSize.width - 50
        
        tripFinishView.frame.size = CGSize(width: width, height: 270)
        
        tripFinishView.center = CGPoint(x: Application.screenSize.width / 2, y: Application.screenSize.height / 2)
        
//        tripFinishView.center = CGPoint(x: self.contentView.bounds.midX, y: self.contentView.bounds.midY)
        
        let bgView = UIView()
//        bgView.frame = self.contentView.frame
        self.tripFinishBGView = bgView
        
        bgView.frame = CGRect(x:0, y:0, width:Application.screenSize.width, height: Application.screenSize.height)
        
        bgView.center = CGPoint(x: Application.screenSize.width / 2, y: Application.screenSize.height / 2)
        
        bgView.backgroundColor = UIColor.black
        bgView.alpha = 0.4
        bgView.isUserInteractionEnabled = true
        
        tripFinishView.layer.shadowOpacity = 0.5
        tripFinishView.layer.shadowOffset = CGSize(width: 0, height: 3)
        tripFinishView.layer.shadowColor = UIColor.black.cgColor
        
//        self.view.addSubview(bgView)
//        self.view.addSubview(tripFinishView)

        let currentWindow = Application.window
        
        if(currentWindow != nil){
            currentWindow?.addSubview(bgView)
            currentWindow?.addSubview(tripFinishView)
        }else{
            self.view.addSubview(bgView)
            self.view.addSubview(tripFinishView)
        }
        
        Utils.createRoundedView(view: tripFinishView, borderColor: UIColor.clear, borderWidth: 0, cornerRadius: 10)
        
        GeneralFunctions.setImgTintColor(imgView: (tripFinishView.subviews[0] as! UIImageView), color: UIColor.UCAColor.AppThemeColor)
        
        (tripFinishView.subviews[1] as! MyLabel).text = self.generalFunc.getLanguageLabel(origValue: "Successfully Finished", key: "LBL_SUCCESS_FINISHED")
        
        (tripFinishView.subviews[2] as! MyLabel).text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_TRIP_FINISHED_TXT")
        (tripFinishView.subviews[2] as! MyLabel).fitText()
        
        (tripFinishView.subviews[4] as! MyLabel).text = self.generalFunc.getLanguageLabel(origValue: "OK THANKS", key: "LBL_OK_THANKS").uppercased()
        
        
        let okTapGue = UITapGestureRecognizer()
        
        okTapGue.addTarget(self, action: #selector(self.tripFinishOkTapped))
        
        (tripFinishView.subviews[4] as! MyLabel).isUserInteractionEnabled = true
        
        (tripFinishView.subviews[4] as! MyLabel).addGestureRecognizer(okTapGue)
    }
    
    func loadTipView(){
        giveTipView = self.generalFunc.loadView(nibName: "GiveTipView", uv: self, isWithOutSize: true)
        
        let width = Application.screenSize.width  > 380 ? 370 : Application.screenSize.width - 50
        
        giveTipView.frame.size = CGSize(width: width, height: 300)
        
        
        giveTipView.center = CGPoint(x: self.contentView.bounds.midX, y: self.contentView.bounds.midY)
        
        bgTipView = UIView()
        bgTipView.frame = self.contentView.frame
        
        bgTipView.backgroundColor = UIColor.black
        bgTipView.alpha = 0.4
        bgTipView.isUserInteractionEnabled = true
        
        giveTipView.layer.shadowOpacity = 0.5
        giveTipView.layer.shadowOffset = CGSize(width: 0, height: 3)
        giveTipView.layer.shadowColor = UIColor.black.cgColor
        
        self.view.addSubview(bgTipView)
        self.view.addSubview(giveTipView)
        
        Utils.createRoundedView(view: giveTipView, borderColor: UIColor.clear, borderWidth: 0, cornerRadius: 10)
        
        GeneralFunctions.setImgTintColor(imgView: (giveTipView.subviews[0] as! UIImageView), color: UIColor.UCAColor.AppThemeColor)
        
        self.giveTipPLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_GIVE_TIP_TXT")
        self.giveTipNLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_NO_THANKS")
        self.giveTipNoteLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_TIP_TXT")
        self.giveTipHLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_TIP_TITLE_TXT")
        self.giveTipNoteLbl.fitText()
        
        self.enterTipTxtField.isHidden = true
        self.enterTipTxtField.getTextField()!.keyboardType = .decimalPad
        
        self.giveTipNLbl.setClickDelegate(clickDelegate: self)
        self.giveTipPLbl.setClickDelegate(clickDelegate: self)
        
        
        self.enterTipTxtField.getTextField()!.keyboardType = .numberPad
        
    }
    
    func myLableTapped(sender: MyLabel) {
        if(sender == self.giveTipPLbl){
            
            if(currentTipMode == "ON"){
                let tipEntered = Utils.checkText(textField: self.enterTipTxtField.getTextField()!) ? true : Utils.setErrorFields(textField: self.enterTipTxtField.getTextField()!, error: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_FEILD_REQUIRD_ERROR_TXT"))
                
                if(tipEntered){
//                    giveTip()
                    
                    submitRating(fAmount: "\(Utils.getText(textField: self.enterTipTxtField.getTextField()!))", isCollectTip: "Yes")
                }
            }else{
                
                self.enterTipTxtField.isHidden = false
                self.giveTipNoteLbl.isHidden = true
                
                self.giveTipPLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_BTN_OK_TXT")
                self.giveTipNLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_SKIP_TXT")
                self.giveTipHLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_TIP_AMOUNT_ENTER_TITLE")
                
                currentTipMode = "ON"
            }
            
        }else if(sender == self.giveTipNLbl){
//            tripFinishOkTapped()
            submitRating(fAmount: "", isCollectTip: "No")
        }
    }
    
    func giveTip(status : String = "", reference: String = "", code: String = ""){
        let amount = Utils.getText(textField: self.enterTipTxtField.getTextField()!)
        var parameters = ["type":"collectTip","iMemberId": GeneralFunctions.getMemberd(), "UserType": Utils.appUserType, "iTripId": iTripId, "fAmount": "\(Utils.getText(textField: self.enterTipTxtField.getTextField()!))"]
        if(GeneralFunctions.checkCardType() == "PayStack") {
            parameters["email"] = GeneralFunctions.getEmail()
            parameters["status"] = status
            parameters["reference"] = reference
            parameters["code"] = code
        }
        
        let exeWebServerUrl = ExeServerUrl(dict_data: parameters, currentView: self.view, isOpenLoader: true)
        exeWebServerUrl.setDeviceTokenGenerate(isDeviceTokenGenerate: true)
        exeWebServerUrl.currInstance = exeWebServerUrl
        exeWebServerUrl.executePostProcess(completionHandler: { (response) -> Void in
            
            if(response != ""){
                let dataDict = response.getJsonDataDict()
                
                if(dataDict.get("Action") == "1"){
                    
                    self.tripFinishOkTapped()
                    
                }else{
                    if(dataDict.get(Utils.message_str) == "LBL_NO_CARD_AVAIL_NOTE"){
                        //                        LBL_NO_CARD_AVAIL_NOTE
                        self.generalFunc.setAlertMessage(uv: self, title: "", content: self.generalFunc.getLanguageLabel(origValue: "", key: dataDict.get(Utils.message_str)), positiveBtn: self.generalFunc.getLanguageLabel(origValue: "Add Card", key: "LBL_ADD_CARD"), nagativeBtn: self.generalFunc.getLanguageLabel(origValue: "Cancel", key: "LBL_CANCEL_TXT"), completionHandler: { (btnClickedIndex) in
                            
                            if(btnClickedIndex == 0){
                                
                                let paymentUV = GeneralFunctions.instantiateViewController(pageName: "PaymentUV") as! PaymentUV
                                self.pushToNavController(uv: paymentUV)
                            }
                            
                        })
                        
                        return
                    }
                    else if (dataDict.get(Utils.message_str) == "send_pin") {
                        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "VerifyViewController") as! VerifyViewController
                        vc.finishDelegate = self
                        vc.reference = dataDict.get("reference")
                        vc.status = "send_pin"
                        vc.amount = amount
                        vc.modalPresentationStyle = .overCurrentContext
                        self.present(vc, animated: true, completion: nil)
                    }
                    else if (dataDict.get(Utils.message_str) == "send_otp") {
                        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "VerifyViewController") as! VerifyViewController
                        vc.finishDelegate = self
                        vc.reference = dataDict.get("reference")
                        vc.status = "send_otp"
                        vc.amount = amount
                        vc.modalPresentationStyle = .overCurrentContext
                        self.present(vc, animated: true, completion: nil)
                    }
                    else if (dataDict.get(Utils.message_str) == "send_phone") {
                        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "VerifyViewController") as! VerifyViewController
                        vc.finishDelegate = self
                        vc.reference = dataDict.get("reference")
                        vc.status = "send_phone"
                        vc.amount = amount
                        vc.modalPresentationStyle = .overCurrentContext
                        self.present(vc, animated: true, completion: nil)
                    }
                    else {
                        self.generalFunc.setError(uv: self, title: "", content: self.generalFunc.getLanguageLabel(origValue: "", key: dataDict.get("message")) + " " + dataDict.get("minValue"))
                    }
                }
                
            }else{
                self.generalFunc.setError(uv: self)
            }
        })
    }
    
    func tripFinishOkTapped(){
        if(tripFinishBGView != nil){
            tripFinishBGView.removeFromSuperview()
        }
        
        if(tripFinishView != nil){
            tripFinishView.removeFromSuperview()
        }
        
        let getUserData = GetUserData(uv: self, window: self.window!)
        getUserData.getdata()
    }
}


extension RatingUV: VerifyRequestDelegate {
    func verifyRequested(_ code: String, reference: String, status: String, amount: String) {
        self.giveTip(status: status, reference: reference, code: code)
    }
}
