//
//  ContactUsUV.swift
//  PassengerApp
//
//  Created by NEW MAC on 13/05/17.
//  Copyright © 2017 V3Cube. All rights reserved.
//

import UIKit

class ContactUsUV: UIViewController, MyBtnClickDelegate {

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var reasonTxtField: MyTextField!
    @IBOutlet weak var queryLbl: MyLabel!
    @IBOutlet weak var queryTxtView: KMPlaceholderTextView!
    @IBOutlet weak var submitBtn: MyButton!
    @IBOutlet weak var scrollView: UIScrollView!
    
    let generalFunc = GeneralFunctions()
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.configureRTLView()
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

//        self.contentView.addSubview(self.generalFunc.loadView(nibName: "ContactUsScreenDesign", uv: self, contentView: contentView))
        
        self.scrollView.addSubview(self.generalFunc.loadView(nibName: "ContactUsScreenDesign", uv: self, contentView: scrollView))

        self.addBackBarBtn()
        setData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setData(){
        self.navigationItem.title = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_CONTACT_US_HEADER_TXT")
        self.title = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_CONTACT_US_HEADER_TXT")
        
        self.reasonTxtField.setPlaceHolder(placeHolder: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_RES_TO_CONTACT"))
        self.queryLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_YOUR_QUERY")
 
        self.queryTxtView.placeholder = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_CONTACT_US_WRITE_EMAIL_TXT")
        self.submitBtn.setButtonTitle(buttonTitle: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_SEND_QUERY_BTN_TXT"))
        self.submitBtn.clickDelegate = self
        
        self.reasonTxtField.getTextField()!.isPlaceholderAnimated = true
    }

    func submitContactQuery(){
        
        let parameters = ["type":"sendContactQuery", "UserType": Utils.appUserType, "UserId": GeneralFunctions.getMemberd(),"message": queryTxtView.text!, "subject": Utils.getText(textField: reasonTxtField.getTextField()!)]
        
        let exeWebServerUrl = ExeServerUrl(dict_data: parameters, currentView: self.view, isOpenLoader: true)
        exeWebServerUrl.executePostProcess(completionHandler: { (response) -> Void in
            
//            print("Response:\(response)")
            if(response != ""){
                let dataDict = response.getJsonDataDict()
                
                if(dataDict.get("Action") == "1"){
                    
                    self.reasonTxtField.setText(text: "")
//                   self.reasonTxtField.getTextField()!.text = ""
                    self.queryTxtView.text = ""
                    
                }
                
                self.generalFunc.setError(uv: self, title: "", content: self.generalFunc.getLanguageLabel(origValue: "", key: dataDict.get("message")))

                
            }else{
                self.generalFunc.setError(uv: self)
            }
            
        })
    
    }
    func myBtnTapped(sender: MyButton) {
        if(sender == self.submitBtn){
            
            if(Utils.getText(textField: reasonTxtField.getTextField()!).count == 0 || queryTxtView.text!.count == 0){
                self.generalFunc.setError(uv: self, title: "", content: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_ENTER_DETAILS_TXT"))
            }else if(Utils.getText(textField: reasonTxtField.getTextField()!).count < 2 || queryTxtView.text!.count < 2){
                Utils.showSnakeBar(msg: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_FILL_PROPER_DETAILS"), uv: self)
            }else{
                submitContactQuery()
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
