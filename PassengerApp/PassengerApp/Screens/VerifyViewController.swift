//
//  VerifyViewController.swift
//  PassengerApp
//
//  Created by Zhuxian Quan on 3/27/18.
//  Copyright © 2018 V3Cube. All rights reserved.
//

import UIKit

protocol VerifyRequestDelegate {
    func verifyRequested(_ code: String, reference: String, status: String, amount: String  )
}

class VerifyViewController: UIViewController {

    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var codeLabel: UITextField!
    
    var finishDelegate: VerifyRequestDelegate?
    
    var code = ""
    var reference = ""
    var status = ""
    var amount = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initView() {
        alertView.layer.masksToBounds = true
        alertView.layer.cornerRadius = 8
        if status == "send_pin" {
            titleLabel.text = "Input your Pin Code"
            codeLabel.placeholder = "Pin Code"
        }
        else if status == "send_otp" {
            titleLabel.text = "Input your OTP Code"
            codeLabel.placeholder = "OTP Code"
        }
        else if status == "send_phone" {
            titleLabel.text = "Input your Phone Number"
            codeLabel.placeholder = "Phone Number"
        }
    }

    @IBAction func submitButtonTapped(_ sender: Any) {
        if self.codeLabel.text?.count == 0 {
            return
        }
        
        self.dismiss(animated: true) {
            self.finishDelegate?.verifyRequested(self.codeLabel.text!, reference: self.reference, status: self.status, amount: self.amount)
        }
        
    }
}
