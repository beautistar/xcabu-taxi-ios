//
//  RideHistroyUV.swift
//  PassengerApp
//
//  Created by NEW MAC on 13/05/17.
//  Copyright © 2017 V3Cube. All rights reserved.
//

import UIKit

class RideHistoryUV: UIViewController, UITableViewDataSource, UITableViewDelegate, MyBtnClickDelegate {

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    let generalFunc = GeneralFunctions()
    
    var HISTORY_TYPE = "PAST"
    
    var loaderView:UIView!
    
    var dataArrList = [NSDictionary]()
    var nextPage_str = 1
    var isLoadingMore = false
    var isNextPageAvail = false
    
    var APP_TYPE = ""
    
    var cntView:UIView!
    
    var extraHeightContainer = [CGFloat]()
    var userProfileJson:NSDictionary!
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.configureRTLView()
        pageTabBarItem.titleColor = UIColor(hex: 0x141414)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        pageTabBarItem.titleColor = UIColor(hex: 0x737373)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

         cntView = self.generalFunc.loadView(nibName: "RideHistoryScreenDesign", uv: self, contentView: contentView)
        
        self.contentView.addSubview(cntView)

                
        self.tableView.delegate = self
        
        self.tableView.dataSource = self
        self.tableView.tableFooterView = UIView()
        self.tableView.register(UINib(nibName: "RideHistoryListTVCell", bundle: nil), forCellReuseIdentifier: "RideHistoryTVCell")
        
        userProfileJson = (GeneralFunctions.getValue(key: Utils.USER_PROFILE_DICT_KEY) as! String).getJsonDataDict().getObj(Utils.message_str)
        APP_TYPE = userProfileJson.get("APP_TYPE")
        
        self.dataArrList.removeAll()
        getDtata()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {
            self.cntView.frame = self.view.frame
            self.cntView.frame.size = CGSize(width: Application.screenSize.width, height: self.view.frame.height)
            self.cntView.setNeedsLayout()
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getDtata(){
        if(loaderView == nil){
            loaderView =  self.generalFunc.addMDloader(contentView: self.contentView)
            loaderView.backgroundColor = UIColor.clear
        }else if(loaderView != nil && isLoadingMore == false){
            loaderView.isHidden = false
        }

        
        let parameters = ["type": HISTORY_TYPE == "PAST" ? "getRideHistory" : "checkBookings", "UserType": Utils.appUserType, "iUserId": GeneralFunctions.getMemberd(), "page": self.nextPage_str.description]
        
        let exeWebServerUrl = ExeServerUrl(dict_data: parameters, currentView: self.view, isOpenLoader: false)
        exeWebServerUrl.executePostProcess(completionHandler: { (response) -> Void in
            
            if(response != ""){
                let dataDict = response.getJsonDataDict()
                
                if(dataDict.get("Action") == "1"){
                    
                    let dataArr = dataDict.getArrObj(Utils.message_str)
                    
                    for i in 0 ..< dataArr.count{
                        let dataTemp = dataArr[i] as! NSDictionary
                        
                        self.dataArrList += [dataTemp]
                        
                        if(self.HISTORY_TYPE != "PAST"){

                            let sourceAddHeight = dataTemp.get("vSourceAddresss").height(withConstrainedWidth: Application.screenSize.width - 80, font: UIFont(name: "Roboto-Light", size: 16)!)
                            var destAddHeight = dataTemp.get("tDestAddress").height(withConstrainedWidth: Application.screenSize.width - 80, font: UIFont(name: "Roboto-Light", size: 16)!)
                            
                            if(self.userProfileJson.get("APP_DESTINATION_MODE").uppercased() == "NONE"){
                                destAddHeight = 0
                            }
                            
                            self.extraHeightContainer += [sourceAddHeight + destAddHeight - 20]
                        }else{
                            let sourceAddHeight = dataTemp.get("tSaddress").height(withConstrainedWidth: Application.screenSize.width - 80, font: UIFont(name: "Roboto-Light", size: 16)!)
                            var destAddHeight = dataTemp.get("tDaddress").height(withConstrainedWidth: Application.screenSize.width - 80, font: UIFont(name: "Roboto-Light", size: 16)!)
                         
                            if(self.userProfileJson.get("APP_DESTINATION_MODE").uppercased() == "NONE"){
                                destAddHeight = 0
                            }
                            
                            self.extraHeightContainer += [sourceAddHeight + destAddHeight - 20]
                        }
                        
                    }
                    let NextPage = dataDict.get("NextPage")
                    
                    if(NextPage != "" && NextPage != "0"){
                        self.isNextPageAvail = true
                        self.nextPage_str = Int(NextPage)!
                        
                        self.addFooterView()
                    }else{
                        self.isNextPageAvail = false
                        self.nextPage_str = 0
                        
                        self.removeFooterView()
                    }
                    
                    self.tableView.reloadData()
                    
                }else{
                    if(self.isLoadingMore == false){
                        _ = GeneralFunctions.addMsgLbl(contentView: self.view, msg: self.generalFunc.getLanguageLabel(origValue: "", key: dataDict.get("message")))
                    }else{
                        self.isNextPageAvail = false
                        self.nextPage_str = 0
                        
                        self.removeFooterView()
                    }
                    
                }
                
                //                self.generalFunc.setError(uv: self, title: "", content: self.generalFunc.getLanguageLabel(origValue: "", key: dataDict.get("message")))
                
                
            }else{
                if(self.isLoadingMore == false){
                    self.generalFunc.setError(uv: self)

                }
            }
            
            self.isLoadingMore = false
            self.loaderView.isHidden = true
            
        })
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        
        return self.dataArrList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RideHistoryTVCell", for: indexPath) as! RideHistoryTVCell
        
        let item = self.dataArrList[indexPath.item]
        var vBookingNo = ""
        if(self.HISTORY_TYPE == "PAST"){
            cell.rideDateLbl.text = Utils.convertDateFormateInAppLocal(date: Utils.convertDateGregorianToAppLocale(date: item.get("tTripRequestDateOrig"), dateFormate: "yyyy-MM-dd HH:mm:ss"), toDateFormate: Utils.dateFormateWithTime)
            cell.pickUpLocVLbl.text = item.get("tSaddress")
            cell.destVLbl.text = item.get("tDaddress") == "" ? "----" : item.get("tDaddress")
            cell.cancelBtn.isHidden = true
            cell.statusView.isHidden = false
            vBookingNo = Configurations.convertNumToAppLocal(numStr: item.get("vRideNo"))
        }else{
//            cell.rideDateLbl.text = item.get("dBooking_date")
            cell.rideDateLbl.text = Utils.convertDateFormateInAppLocal(date: Utils.convertDateGregorianToAppLocale(date: item.get("dBooking_dateOrig"), dateFormate: "yyyy-MM-dd HH:mm:ss"), toDateFormate: Utils.dateFormateWithTime)
            cell.pickUpLocVLbl.text = item.get("vSourceAddresss")
            cell.destVLbl.text = item.get("tDestAddress") == "" ? "----" : item.get("tDestAddress")
            cell.cancelBtn.isHidden = false
            cell.statusView.isHidden = true
//            vBookingNo = item.get("vBookingNo")
            vBookingNo = Configurations.convertNumToAppLocal(numStr: item.get("vBookingNo"))
            
            if(item.get("eStatus") == "Pending"){
                cell.cancelBtn.isHidden = false
                cell.statusView.isHidden = true
            }else{
                cell.cancelBtn.isHidden = true
                cell.statusView.isHidden = false
            }
        }
        
        cell.destVLbl.fitText()
        cell.pickUpLocVLbl.fitText()
        
        cell.statusHLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_Status") + ":"
        if(item.get("eCancelled") == "Yes"){
            cell.statusVLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_CANCELED_TXT")
        }else{
            if(item.get("iActive") == "Canceled"){
                cell.statusVLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_CANCELED_TXT")
            }else if(item.get("iActive") == "Finished"){
                cell.statusVLbl.text = self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_FINISHED_TXT")
            }else {
                cell.statusVLbl.text = item.get("iActive") == "" ? item.get("eStatus") : item.get("iActive")
            }
        }
        
        cell.cancelBtn.setButtonTitle(buttonTitle: self.generalFunc.getLanguageLabel(origValue: "", key: item.get("eType") == Utils.cabGeneralType_Deliver ? "LBL_CANCEL_DELIVERY" : "LBL_CANCEL_BOOKING"))
        cell.cancelBtn.tag = indexPath.item
        cell.cancelBtn.clickDelegate = self
        
        cell.bookingNoLbl.text = self.generalFunc.getLanguageLabel(origValue: item.get("eType") == Utils.cabGeneralType_Deliver ? "Delivery" : "Booking", key: item.get("eType") == Utils.cabGeneralType_Deliver ? "LBL_DELIVERY" : "LBL_BOOKING") + "# " + vBookingNo
        
        cell.pickUpLocHLbl.text = self.generalFunc.getLanguageLabel(origValue: "Pick up location", key: "LBL_PICK_UP_LOCATION")
        cell.destHLbl.text = self.generalFunc.getLanguageLabel(origValue: "Destination location", key: "LBL_DEST_LOCATION")
//        cell.rideTypeLbl.text = item.get("eType")
//        cell.rideTypeLbl.text = item.get("eType") == Utils.cabGeneralType_Deliver ? self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_DELIVERY") : self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_BOOKING")
        cell.rideTypeLbl.text = item.get("eType") == Utils.cabGeneralType_Deliver ? self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_DELIVERY") : (item.get("eType") == Utils.cabGeneralType_Ride ? self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_RIDE") : self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_BOOKING"))
        
        if(self.APP_TYPE.uppercased() == "RIDE-DELIVERY"){
            cell.rideDateLbl.isHidden = false
        }else{
            cell.rideTypeLbl.text = cell.rideDateLbl.text
            cell.rideDateLbl.isHidden = true
        }

        if(self.userProfileJson.get("APP_DESTINATION_MODE").uppercased() == "NONE"){
            cell.destVLbl.text = "---"
        }
        
        cell.dataView.layer.shadowOpacity = 0.5
        cell.dataView.layer.shadowOffset = CGSize(width: 0, height: 3)
        cell.dataView.layer.shadowColor = UIColor(hex: 0xe6e6e6).cgColor
        
            
//            cell.dashedView.addDashedBorder(strokeColor: UIColor(hex: 0xADADAD), lineWidth: 2)
        cell.dashedView.backgroundColor = UIColor.clear
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {
            
            cell.dashedView.addDashedLine(color: UIColor(hex: 0xADADAD), lineWidth: 2)
        })
        
        
        GeneralFunctions.setImgTintColor(imgView: cell.locPinImgView, color: UIColor(hex: 0xd73030))
        
        cell.selectionStyle = .none
        cell.backgroundColor = UIColor.clear
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if(self.HISTORY_TYPE == "PAST"){
            let rideDetailUV = GeneralFunctions.instantiateViewController(pageName: "RideDetailUV") as! RideDetailUV
            rideDetailUV.tripDetailDict = self.dataArrList[indexPath.item]
            Utils.printLog(msgData: "TripDetailDictData::\(rideDetailUV.tripDetailDict)")
            self.pushToNavController(uv: rideDetailUV)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        
        if(indexPath.item < self.extraHeightContainer.count){
            return self.extraHeightContainer[indexPath.item] + 295
        }
        
        return 295
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let currentOffset = scrollView.contentOffset.y;
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
        
        
        if (maximumOffset - currentOffset <= 15) {
            
            if(isNextPageAvail==true && isLoadingMore==false){
                
                isLoadingMore=true
                
                getDtata()
            }
        }
    }
    
    func addFooterView(){
        let loaderView =  self.generalFunc.addMDloader(contentView: self.tableView, isAddToParent: false)
        loaderView.backgroundColor = UIColor.clear
        loaderView.frame = CGRect(x:0, y:0, width: Application.screenSize.width, height: 80)
        self.tableView.tableFooterView  = loaderView
        self.tableView.tableFooterView?.isHidden = false
    }
    
    func removeFooterView(){
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
        self.tableView.tableFooterView?.isHidden = true
    }
    
    func myBtnTapped(sender: MyButton) {
        cancelBooking(position: sender.tag)
    }
    
    func cancelBooking(position:Int){
        
        let bgView = UIView()
        bgView.backgroundColor = UIColor.black
        bgView.alpha = 0.4
        bgView.frame = self.contentView.frame
        
        bgView.center = CGPoint(x: self.view.bounds.midX, y: self.view.bounds.midY)
        
        let cancelBookingView = CancelBookingView(frame: CGRect(x: self.view.frame.width / 2, y: self.view.frame.height / 2, width: (Application.screenSize.width > 390 ? 375 : (Application.screenSize.width - 50)), height: 200))
        cancelBookingView.center = CGPoint(x: self.view.bounds.midX, y: self.view.bounds.midY)
        cancelBookingView.setViewHandler { (isViewClose, view, isPositiveBtnClicked, reason) in
            
            cancelBookingView.frame.origin.y = Application.screenSize.height + 1000
            
            bgView.removeFromSuperview()
            
            self.view.layoutIfNeeded()
            
            if(isPositiveBtnClicked){
                self.continueCancelBooking(iCabBookingId: self.dataArrList[position].get("iCabBookingId"), reason: reason)
            }
        }
        
        Utils.createRoundedView(view: cancelBookingView, borderColor: UIColor.clear, borderWidth: 0, cornerRadius: 10)
        
        cancelBookingView.layer.shadowOpacity = 0.5
        cancelBookingView.layer.shadowOffset = CGSize(width: 0, height: 3)
        cancelBookingView.layer.shadowColor = UIColor.black.cgColor
        
        self.view.addSubview(bgView)
        self.view.addSubview(cancelBookingView)
        
    }
    
    func continueCancelBooking(iCabBookingId: String, reason:String){
        let parameters = ["type":"cancelBooking", "iUserId": GeneralFunctions.getMemberd(), "UserType": Utils.appUserType, "iCabBookingId": iCabBookingId, "Reason": reason]
        
        let exeWebServerUrl = ExeServerUrl(dict_data: parameters, currentView: self.view, isOpenLoader: true)
        exeWebServerUrl.executePostProcess(completionHandler: { (response) -> Void in
            
            if(response != ""){
                let dataDict = response.getJsonDataDict()
                
                if(dataDict.get("Action") == "1"){
                    
                    self.generalFunc.setAlertMessage(uv: self, title: "", content: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_BOOKING_CANCELED"), positiveBtn: self.generalFunc.getLanguageLabel(origValue: "", key: "LBL_BTN_OK_TXT"), nagativeBtn: "", completionHandler: { (btnClickedIndex) in
                        
                        self.isLoadingMore = false
                        self.dataArrList.removeAll()
                        self.tableView.reloadData()
                        self.nextPage_str = 1
                        self.getDtata()
                    })
                    
                    
                    
                }else{
                    if(dataDict.get(Utils.message_str) == "DO_RESTART"){
                        let window = Application.window
                        
                        let getUserData = GetUserData(uv: self, window: window!)
                        getUserData.getdata()
                        
                        return
                    }
                    
                    self.generalFunc.setError(uv: self, title: "", content: self.generalFunc.getLanguageLabel(origValue: "", key: dataDict.get("message")))
                }
                
            }else{
                self.generalFunc.setError(uv: self)
            }
        })
    }

    @IBAction func unwindToRideHistoryScreen(_ segue:UIStoryboardSegue) {
        
        if(segue.source.isKind(of: RideDetailUV.self))
        {
            if(self.HISTORY_TYPE == "PAST"){
                let iTripId = (segue.source as! RideDetailUV).tripDetailDict.get("iTripId")
                var dataList = [NSDictionary]()
                dataList.append(contentsOf: dataArrList)
                
                self.dataArrList.removeAll()
                
                for i in 0..<dataList.count{
                    
                    let item = dataList[i]
                    let tripId = item.get("iTripId")
                    
                    if(iTripId == tripId){
                        
                        item.setValue("Yes", forKey: "is_rating")
                        
                        
                    }
                    
                    self.dataArrList += [item]
                }
                
                self.tableView.reloadData()
            }
           
        }
        
    }
}
