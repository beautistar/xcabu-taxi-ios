//
//  ChatUV.swift
//  PassengerApp
//
//  Created by NEW MAC on 01/08/17.
//  Copyright © 2017 V3Cube. All rights reserved.
//

import UIKit
import AVFoundation
import FirebaseStorage

class ChatUV: JSQMessagesViewController {
    
    
    let generalFunc = GeneralFunctions()
    var receiverId = ""
    var assignedtripId = ""
    var messageId = ""
    var receiverDisplayName = ""
    var pPicName = ""
    
    var messages = [JSQMessage]()
    
    var senderImg:JSQMessagesAvatarImage!
    var receiverImg:JSQMessagesAvatarImage!
    
    var senderImgView = UIImageView()
    var receiverImgView = UIImageView()
    
    
    var userProfileJson : NSDictionary!
    
    // BUBBLE PROPERTY
    lazy var outgoingBubble: JSQMessagesBubbleImage = {
        return JSQMessagesBubbleImageFactory()!.outgoingMessagesBubbleImage(with: UIColor(hex: 0x1087FF))
    }()
    
    lazy var incomingBubble: JSQMessagesBubbleImage = {
        return JSQMessagesBubbleImageFactory()!.incomingMessagesBubbleImage(with: UIColor(hex: 0xB6B6B6))
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.configureRTLView()
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.addBackBarBtn()
        
        inputToolbar.preferredDefaultHeight = 50
        inputToolbar.maximumHeight = 100
        
        GeneralFunctions.saveValue(key: "cahtViewVisible", value:true as Bool as AnyObject)
        
        userProfileJson = (GeneralFunctions.getValue(key: Utils.USER_PROFILE_DICT_KEY) as! String).getJsonDataDict().getObj(Utils.message_str)
        
        let defaultImgView = UIImageView(frame: CGRect(x: 0, y:0, width: 60, height: 60))
        defaultImgView.image = UIImage(named: "ic_no_pic_user")
        
        self.senderImg = JSQMessagesAvatarImageFactory.avatarImage(withPlaceholder: defaultImgView.image, diameter: 60)!
        self.receiverImg = JSQMessagesAvatarImageFactory.avatarImage(withPlaceholder: defaultImgView.image, diameter: 60)!
        
        senderId = GeneralFunctions.getMemberd() + "_" + assignedtripId + "_\(Utils.appUserType)"
        senderDisplayName = userProfileJson.get("vName")
        
        
        messageId = receiverId + "_" + assignedtripId + "_Driver"
        
        title = "\(receiverDisplayName)"
        
        inputToolbar.contentView.leftBarButtonItem = nil
        
        inputToolbar.contentView.textView.placeHolder = self.generalFunc.getLanguageLabel(origValue: "Enter new message", key: "LBL_ENTER_MESSAGE")
        
        inputToolbar.contentView.textView.accessibilityLabel = self.generalFunc.getLanguageLabel(origValue: "Enter new message", key: "LBL_ENTER_MESSAGE")
        
        senderImgView.frame.size = CGSize(width: 60, height:60)
        receiverImgView.frame.size = CGSize(width: 60, height:60)
        
        let rightButton = UIButton(type: .custom)
        rightButton.setImage(UIImage(named: "ic_send_msg_arrow"), for: .normal)
        inputToolbar.contentView.rightBarButtonItem = rightButton
        inputToolbar.contentView.rightBarButtonItemWidth = 50
        
        inputToolbar.contentView.textView.backgroundColor = UIColor.clear
        inputToolbar.contentView.textView.borderWidth = 0
        
        
        collectionView.collectionViewLayout.incomingAvatarViewSize = CGSize(width: 60, height:60)
        collectionView.collectionViewLayout.outgoingAvatarViewSize = CGSize(width: 60, height:60)
        
        let query = Constants.refs.databaseChats.queryOrderedByPriority()
        
        _ = query.observe(.childAdded, with: { [weak self] snapshot in
            
            if  let data        = snapshot.value as? [String: String],
                let id          = data["messageId"],
                let name        = data["messageUser"],
                let text        = data["messageText"],
                !text.isEmpty
            {
                if let message = JSQMessage(senderId: id, displayName: name, text: text)
                {
                    if(id == self?.senderId || id == self?.messageId)
                    {
                        //                        AudioServicesPlaySystemSound(1315);
                        self?.messages.append(message)
                        
                        self?.finishReceivingMessage()
                    }
                    
                }
            }
        })
        
        
        // Delete chat Code
        //        if (id == self?.senderId)
        //        {
        //            let ref = Constants.refs.databaseChats
        //            print("error222 \(ref)")
        //            ref.queryOrdered(byChild: "sender_id").queryEqual(toValue: id).observe(.childAdded, with: { (snapshot) in
        //
        //                snapshot.ref.removeValue(completionBlock: { (error, reference) in
        //                    if error != nil {
        //                        print("There has been an error:\(error)")
        //                    }
        //                })
        //
        //            })
        //        }
        
        
        senderImgView.sd_setImage(with: URL(string: CommonUtils.user_image_url + GeneralFunctions.getMemberd() + "/" + userProfileJson.get("vImgName")), placeholderImage: UIImage(named: "ic_no_pic_user"),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
            
            self.senderImg = JSQMessagesAvatarImageFactory.avatarImage(withPlaceholder: self.senderImgView.image, diameter: 60)!
            
            self.collectionView.reloadData()
            
            self.finishReceivingMessage()
        })
        
        receiverImgView.sd_setImage(with: URL(string: CommonUtils.driver_image_url + receiverId + "/" + pPicName), placeholderImage: UIImage(named: "ic_no_pic_user"),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
            
            self.receiverImg = JSQMessagesAvatarImageFactory.avatarImage(withPlaceholder: self.receiverImgView.image, diameter: 60)!
            
            self.collectionView.reloadData()
            
            self.finishSendingMessage()
        })
        
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        GeneralFunctions.saveValue(key: "cahtViewVisible", value:false as Bool as AnyObject)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // JSQ METHODS
    
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData!
    {
        return messages[indexPath.item]
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return messages.count
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource!
    {
        return messages[indexPath.item].senderId == senderId ? outgoingBubble : incomingBubble
    }
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAt indexPath: IndexPath!) -> JSQMessageAvatarImageDataSource!
    {
        let message = messages[indexPath.row]
        
        if(message.senderId == senderId){
            return senderImg
        }else{
            return receiverImg
        }
        //        return message.senderId == senderId ? avatars[senderId] : avatars[receiverId]
    }
    
    // Methods for display User name upon Message bubble
    //    override func collectionView(_ collectionView: JSQMessagesCollectionView!, attributedTextForMessageBubbleTopLabelAt indexPath: IndexPath!) -> NSAttributedString!
    //    {
    //        return messages[indexPath.item].senderId == senderId ? nil : NSAttributedString(string: messages[indexPath.item].senderDisplayName)
    //    }
    
    // override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForMessageBubbleTopLabelAt indexPath: IndexPath!) -> CGFloat
    // {
    // return messages[indexPath.item].senderId == senderId ? 0 : 15
    //}
    
    // SEND MESSAGE
    override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: Date!)
    {
        let ref = Constants.refs.databaseChats.childByAutoId()
        
        let message = ["messageId": senderId, "messageUser": senderDisplayName, "messageText": text]
        
        storeMessageToclientServer(textMessage: text!)
        
        ref.setValue(message)
        
        finishSendingMessage()
        
        
    }
    func storeMessageToclientServer(textMessage:String)
    {
        
        let parameters = ["type":"SendTripMessageNotification","UserType": Utils.appUserType, "iFromMemberId": GeneralFunctions.getMemberd(), "iToMemberId": receiverId, "iTripId": assignedtripId, "tMessage": textMessage]
        
        let exeWebServerUrl = ExeServerUrl(dict_data: parameters, currentView: self.view, isOpenLoader: false)
        exeWebServerUrl.setDeviceTokenGenerate(isDeviceTokenGenerate: false)
        exeWebServerUrl.currInstance = exeWebServerUrl
        exeWebServerUrl.executePostProcess(completionHandler: { (response) -> Void in
            
            if(response != ""){
                Utils.printLog(msgData: "Sucess::::::::::::::::")
                
            }else{
                
            }
        })
        
    }
}
